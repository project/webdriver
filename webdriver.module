<?php

const WEBDRIVER_LIBRARY_DEFAULT_PATH = 'sites/all/libraries';

/**
 * Include admin file for settings.
 */
module_load_include('inc', 'webdriver', 'webdriver.admin');

/**
 * Implements hook_menu().
 */
function webdriver_menu() {
  $items = array();

  $items['admin/config/development/testing/webdriver'] = array(
    'title' => 'Archives',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('webdriver_archive_form'),
    'access arguments' => array('administer unit tests'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'webdriver.pages.inc',
    'weight' => 10,
  );
  $items['admin/config/development/testing/webdriver/archive'] = array(
    'title' => 'Archives',
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  $items['admin/config/development/testing/webdriver/results/%'] = array(
    'title' => 'Test suite result',
    'page callback' => 'webdriver_results_page',
    'page arguments' => array(6),
    'access arguments' => array('administer unit tests'),
    'file' => 'webdriver.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_menu_alter().
 */
function webdriver_menu_alter(&$items) {
  $items['admin/config/development/testing/settings']['weight'] = 20;
}

/**
 * Implements hook_requirements().
 */
function webdriver_requirements($phase) {
  $requirements = array();

  if ($phase === 'runtime') {

    // Check if required Webdriver client library is available.
    $library_path = variable_get('webdriver_library_path', WEBDRIVER_LIBRARY_DEFAULT_PATH) . '/php-webdriver';
    $requirements['webdriver_library'] = array(
      'title' => 'Selenium WebDriver library path',
      'value' => $library_path,
      'severity' => is_dir($library_path) ? REQUIREMENT_OK : REQUIREMENT_ERROR,
    );

    // Ping servers configured to check status.
    $summary = array();
    $severity = REQUIREMENT_OK;
    $servers = variable_get('webdriver_servers', array());
    $servers_infos = webdriver_servers_info();
    foreach ($servers as $server_type => $config) {
      $server = new $servers_infos[$server_type]['class']();
      $infos = $server->getStatus($config['host']);

      $output = "Server type : $server_type";
      if (!empty($infos['version'])) {
        $output .= ' - Version : ' . $infos['version'];
      }
      elseif (!empty($infos)) {
        $output .= ' - Version : ' . t('Unknown');
      }
      else {
        $output .= ' - ' . t('Server is not responding.');
        $severity = REQUIREMENT_ERROR;
      }
      $summary[] = $output;
    }

    $requirements['webdriver_server'] = array(
      'title' => 'WebDriver server',
      'value' => $summary ? implode(' | ', $summary) : t('No server defined'),
      'severity' => $summary ? $severity : REQUIREMENT_WARNING,
    );
  }

  return $requirements;
}

/**
 * Implements hook_schema_alter().
 */
function webdriver_schema_alter(&$schema) {

  $schema['simpletest']['fields']['browser'] = array(
    'type' => 'varchar',
    'length' => 32,
    'not null' => TRUE,
    'default' => '',
  );

  $schema['simpletest']['fields']['capabilities'] = array(
    'type' => 'blob',
    'serialize' => TRUE,
  );

  $schema['simpletest']['fields']['server_type'] = array(
    'type' => 'varchar',
    'length' => 32,
    'not null' => TRUE,
    'default' => '',
  );

  $schema['simpletest_test_id']['fields']['timestamp'] = array(
    'type' => 'int',
    'not null' => TRUE,
    'default' => 0,
  );
}

/**
 * Implements hook_test_group_started().
 *
 * @see simpletest_run_tests().
 */
function webdriver_test_group_started() {
  // Ensure to remove verbose directory if not same as default.
  $directory = variable_get('webdriver_verbose_directory', FALSE);
  if ($directory) {
    file_unmanaged_delete_recursive($directory);
  }
}

/**
 * Implements hook_theme().
 */
function webdriver_theme() {
  return array(
    'webdriver_browsers' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Returns server type infos availables.
 *
 * @param string $type
 *   Filter on a specific server type.
 *
 * @return array
 *   An array keyed by server type and containing an array with keys :
 *   - class
 *   If $type is specified, return only the server type info if it exists.
 *
 */
function webdriver_servers_info($type = '') {
  $info =& drupal_static(__FUNCTION__, array());
  if (!$info) {
    $info = module_invoke_all('webdriver_servers_info');
    drupal_alter('webdriver_servers_info', $info);
  }
  return $type ? (isset($info[$type]) ? $info[$type] : array()) : $info;
}

/**
 * Implements hook_webdriver_servers_info().
 */
function webdriver_webdriver_servers_info() {
  return array(
    'selenium' => array(
      'class' => 'DrupalWebDriverServerSelenium',
    ),
    'selendroid' => array(
      'class' => 'DrupalWebDriverServerSelendroid',
    ),
    'appium' => array(
      'class' => 'DrupalWebDriverServerAppium',
    ),
  );
}

/**
 * Clean up testing environnement with different alternatives instead of using
 * simpletest_clean_environment() function.
 *
 * What's could be clear :
 * - testing files
 * - testing DB prefixed tables
 * - DB asserts
 * - verbose directories (html + images)
 *
 * @param string $type
 *   Which type of clear command to execute.
 *   Should be either "all" or "env".
 * @param array $test_ids
 *   An array of test IDS to clear. Used only with type "all".
 * @param int $timestamp
 *   Optionnally cleared test suites older/equal than this date. Used only with
 *   type "all" and override $test_ids.
 */
function webdriver_clean_environment($type = 'all', array $test_ids = array(), $timestamp = 0) {

  // Always clear all testing environments in case of errors during tests.
  simpletest_clean_database();
  simpletest_clean_temporary_directories();

  // Cleanup verbose messages and DB assert.
  if ($type === 'all') {

    // Default to all tests suites if none are provided.
    $test_ids = $test_ids ? $test_ids : array(NULL);

    // Override test ids by older test suites.
    if ($timestamp) {
      $test_ids = db_select('simpletest_test_id', 's')
        ->fields('s', array('test_id'))
        ->condition('s.timestamp', $timestamp, '<=')
        ->execute()
        ->fetchCol();
    }

    $dir_verbose_root = variable_get('webdriver_verbose_directory', 'public://simpletest/verbose');
    $count = 0;
    foreach ($test_ids as $test_id) {

      $directory = $test_id ? "$dir_verbose_root/$test_id" : $dir_verbose_root;
      file_unmanaged_delete_recursive($directory);

      $count += webdriver_clean_results_table($test_id);
    }
    drupal_set_message('Verbose directory removed.');
    drupal_set_message(format_plural($count, 'Removed @count test result.', 'Removed @count test results.'));
  }
}

/**
 * Method helper to force DB assert to be cleared, whatever setting is set or
 * not.
 */
function webdriver_clean_results_table($test_id = NULL) {
  global $conf;

  $clean_setting = $conf['simpletest_clear_results'];
  $conf['simpletest_clear_results'] = TRUE;
  $count = simpletest_clean_results_table($test_id);
  $conf['simpletest_clear_results'] = $clean_setting;

  return $count;
}

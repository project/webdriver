<?php

/**
 * Generate the archive page results.
 */
function webdriver_archive_form($form, &$form_state) {
  $show_all = !variable_get('webdriver_clear_verbose_results_on_success', FALSE);

  // Re-use simpletest CSS.
  $form['#attached']['css'][] = drupal_get_path('module', 'simpletest') . '/simpletest.css';

  $header = array(
    'timestamp' => array(
      'data' => t('Running date'),
      'field' => 'sti.timestamp',
      'sort' => 'desc',
    ),
    'test_id' => t('Test suite ID'),
    'pass' => t('Pass'),
    'fail' => t('Fail'),
    'exception' => t('Exception'),
    'operations' => t('Operations'),
  );
  $options = array();

  // Get tests suites info.
  $test_suites = db_select('simpletest_test_id', 'sti')
    ->extend('PagerDefault')
    ->extend('TableSort')
    ->fields('sti', array('test_id', 'timestamp'))
    ->orderByHeader($header)
    ->limit(30)
    ->execute()
    ->fetchAllKeyed();

  if ($test_suites) {

    // Get tests infos.
    $query = db_select('simpletest', 's')
      ->fields('s', array('test_id', 'status'));
    $query->addExpression('COUNT(*)', 'count');
    $query->condition('s.test_id', array_keys($test_suites), 'IN');
    $query->condition('s.status', array('pass', 'fail', 'exception'), 'IN');
    $query->groupBy('s.test_id')
      ->groupBy('s.status');

    $result = $query->execute()->fetchAll();

    // Gather count by test id and status.
    $asserts = array();
    if ($result) {
      foreach ($result as $data) {
        $asserts["$data->test_id-$data->status"] = $data->count;
      }
    }

    // Build tables rows by iterating on test suites.
    foreach ($test_suites as $test_id => $timestamp) {
      $pass = isset($asserts["$test_id-pass"]) ? $asserts["$test_id-pass"] : 0;
      $fail = isset($asserts["$test_id-fail"]) ? $asserts["$test_id-fail"] : 0;
      $exception = isset($asserts["$test_id-exception"]) ? $asserts["$test_id-exception"] : 0;

      $options[$test_id] = array(
        'timestamp' => $timestamp > 0 ? format_date($timestamp) : t('Unknown date'),
        'test_id' => $test_id,
        'pass' => $pass,
        'fail' => $fail,
        'exception' => $exception,
        'operations' => $show_all || $fail || $exception ? l(t('See'), "admin/config/development/testing/webdriver/results/$test_id", array('attributes' => array('target' => '_blank'))) : '',
        '#attributes' => array(
          'class' => array('simpletest-' . ($fail ? 'fail' : ($exception ? 'exception' : 'pass'))),
        ),
      );
    }
  }

  $form['operations'] = array(
    '#type' => 'fieldset',
    '#title' => t('Operations'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['operations']['date'] = array(
    '#type' => 'textfield',
    '#title' => t('Older than'),
    '#description' => t('If no test suites are selected, you could enter a <a href="!href">PHP relative date</a>. For e.g : "-4 days", "yesterday", "2014-11-09", etc.', array('!href' => 'http://php.net/manual/en/datetime.formats.relative.php')),
  );

  $form['operations']['actions'] = array('#type' => 'actions');
  $form['operations']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Clear results'),
  );

  $form['test_suites'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No test results available'),
  );

  $form['pager'] = array('#theme' => 'pager');
  return $form;
}

/**
 * Form validate handler.
 *
 * @see webdriver_archive_form().
 */
function webdriver_archive_form_validate(&$form, &$form_state) {
  if (!empty($form_state['values']['date'])) {
    if (!strtotime($form_state['values']['date'])) {
      form_set_error('date', t('Invalid PHP relative date provided.'));
    }
  }
  elseif (empty(array_filter($form_state['values']['test_suites']))) {
    form_set_error('test_suites', t('You should select at least one test suite to clear.'));
  }
}

/**
 * Form submit handler.
 *
 * @see webdriver_archive_form().
 */
function webdriver_archive_form_submit(&$form, &$form_state) {
  if (!empty($form_state['values']['date'])) {
    $timestamp = strtotime($form_state['values']['date']);
    webdriver_clean_environment('all', array(), $timestamp);
  }
  else {
    $test_ids = array_filter($form_state['values']['test_suites']);
    webdriver_clean_environment('all', $test_ids);
  }
}

/**
 * Page result callback.
 */
function webdriver_results_page($test_id) {
  $build = array();
  $show_all = !variable_get('webdriver_clear_verbose_results_on_success', FALSE);

  // Get only failed test classes by browsers/capabilities.
  $filters = array();
  if (!$show_all) {

    $filters = db_select('simpletest', 's')
      ->fields('s', array('test_class', 'browser', 'capabilities', 'server_type'))
      ->condition('s.test_id', $test_id)
      ->condition('s.status', array('fail', 'exception'), 'IN')
      ->groupBy('s.test_class')
      ->groupBy('s.browser')
      ->groupBy('s.capabilities')
      ->groupBy('s.server_type')
      ->execute()
      ->fetchAll();

    if (!$filters) {
      drupal_set_message(t('No failed test results founded for test ID : %test_id.', array('%test_id' => $test_id)), 'error');
      drupal_goto('admin/config/development/testing/webdriver');
    }
  }

  // Get test suite assertions.
  $query = db_select('simpletest', 's')
    ->fields('s')
    ->condition('s.test_id', $test_id);

  $db_or = db_or();
  foreach ($filters as $filter) {
    $db_and = db_and();
    $db_and
      ->condition('s.test_class', $filter->test_class)
      ->condition('s.browser', $filter->browser)
      ->condition('s.server_type', $filter->server_type);
    if (isset($filter->capabilities)) {
      $db_and->condition('s.capabilities', $filter->capabilities);
    }
    else {
      $db_and->isNull('s.capabilities');
    }
    $db_or->condition($db_and);
  }

  if (count($db_or->conditions()) > 1) {
    $query->condition($db_or);
  }

  $query->orderBy('s.test_class', 'ASC')
    ->orderBy('s.browser', 'ASC')
    ->orderBy('s.capabilities', 'ASC')
    ->orderBy('s.server_type', 'ASC')
    ->orderBy('s.message_id', 'ASC');

  $result = $query->execute()->fetchAll();
  if (!$result) {
    drupal_set_message(t('No test results founded for test id : %test_id.', array('%test_id' => $test_id)), 'error');
    drupal_goto('admin/config/development/testing/webdriver');
  }
  module_load_include('inc', 'simpletest', 'simpletest.pages');

  // Because we don't use a form, form_process_fieldset() won't be called.
  $build['#attached']['library'][] = array('system', 'drupal.collapse');

  // Re-use simpletest CSS.
  $build['#attached']['css'][] = drupal_get_path('module', 'simpletest') . '/simpletest.css';
  // Autoload PSR-0 classes for infos.
  simpletest_classloader_register();

  // Gather results by test classes and browsers/capabilities.
  $data = array();
  foreach ($result as $assertion) {
    $browser_key = $assertion->browser . '||' . $assertion->server_type . '||' . (isset($assertion->capabilities) ? $assertion->capabilities : serialize(array()));
    $data[$assertion->test_class][$browser_key][] = $assertion;
  }

  // Then build table results.
  $header = array(t('Message'), t('Group'), t('Filename'), t('Line'), t('Function'), array('colspan' => 2, 'data' => t('Status')));
  foreach ($data as $test_class => $browser_assertions) {

    $info = call_user_func(array($test_class, 'getInfo'));
    $build[$test_class] = array(
      '#type' => 'fieldset',
      '#title' => $info['name'],
      '#description' => $info['description'],
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#attributes' => array('class' => array('collapsible', 'collapsed')),
    );

    foreach ($browser_assertions as $browser_key => $assertions) {
      list($browser, $server_type, $capabilities) = explode('||', $browser_key);
      $capabilities = unserialize($capabilities);

      $title = $browser ? $browser : t('Outside browsers');
      foreach ($capabilities as $name => $value) {
        $title .= is_string($value) ? ", $name - $value" : '';
      }
      $title .= $server_type ? " - $server_type" : '';

      $build[$test_class][$browser_key] = array(
        '#type' => 'fieldset',
        '#title' => $title,
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#attributes' => array('class' => array('collapsible', 'collapsed')),
      );

      $rows = array();
      $fail = FALSE;
      foreach ($assertions as $assertion) {
        $row = array();
        $row[] = $assertion->message;
        $row[] = $assertion->message_group;
        $row[] = drupal_basename($assertion->file);
        $row[] = $assertion->line;
        $row[] = $assertion->function;
        $row[] = simpletest_result_status_image($assertion->status);
        $rows[] = array(
          'data' => $row,
          'class' => array(
            $assertion->message_group === 'Debug' ? 'simpletest-debug' : 'simpletest-' . $assertion->status,
          ),
        );
        $fail = $fail || in_array($assertion->status, array('fail', 'exception'), TRUE);
      }

      $build[$test_class][$browser_key]['table'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      );

      if ($fail) {
        // Open browser fieldset if there is errors.
        if ($build[$test_class][$browser_key]['#collapsed']) {
          $build[$test_class][$browser_key]['#collapsed'] = FALSE;
          $build[$test_class][$browser_key]['#attributes']['class'] = array_diff($build[$test_class][$browser_key]['#attributes']['class'], array('collapsed'));
        }
        // Also open test class (parent fieldset) if not already.
        if ($build[$test_class]['#collapsed']) {
          $build[$test_class]['#collapsed'] = FALSE;
          $build[$test_class]['#attributes']['class'] = array_diff($build[$test_class]['#attributes']['class'], array('collapsed'));
        }
      }
    }
  }

  return $build;
}

<?php

/**
 * @file
 * Hooks provided by the Webdriver module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Returns an array of available server types.
 *
 * @return array
 *   An array keyed by server type and containing keys :
 *   - class : the server class implementing the DrupalWebDriverServer interface.
 */
function hook_webdriver_servers_info() {
  return array(
    'appium' => array(
      'class' => 'DrupalWebDriverServerAppium',
    ),
  );
}

/**
 * Allow module to alter default server info.
 *
 * @param array $info
 *   An array returned by hook_webdriver_servers_info().
 *
 * @see hook_webdriver_servers_info().
 */
function hook_webdriver_servers_info_alter(&$info) {
  $info['selenium']['class'] = 'MyClassWebDriverServerSelenium';
}

/**
 * @} End of "addtogroup hooks".
 */

Selenium server
-------------------------------

Download it at http://docs.seleniumhq.org/download

Version tested : 2.42.X, 2.43.x, 2.44.x

As a quick start, you should only execute the selenium server jar :
  java -jar selenium-server-standalone-2.44.0.jar
on the server which host browsers to test.

You may then use Selenium Grid to run test on multiple server :

Start the hub first :
  java -jar selenium-server-standalone-2.44.0.jar -role hub -host <IP>

Start the nodes on other servers :
  java -jar selenium-server-standalone-2.44.0.jar -role node -hub http://<IP-HUB>:<HUB-PORT>/grid/register> -host <IP>

For example, we have a hub server IP address which is 192.168.56.1 with default
port 4444 used and a node server which is 192.168.56.12, it would be  :
  java -jar selenium-server-standalone-2.44.0.jar -role hub -host 192.168.56.1
  java -jar selenium-server-standalone-2.44.0.jar -role node -hub http://192.168.56.1:4444/grid/register> -host 192.168.56.12

Note that -host option is used in case of exotic network (like VM with multiple
network interface).

You may also provide a JSON file config for hub and node or directly through
command line option with for e.g :
  java -jar selenium-server-standalone-2.44.0.jar -role node -hub http://192.168.56.1:4444/grid/register> -host 192.168.56.10 -browser "browserName=internet explorer,version=8"
  java -jar selenium-server-standalone-2.44.0.jar -role node -hub http://192.168.56.1:4444/grid/register> -host 192.168.56.11 -browser "browserName=internet explorer,version=9"
  java -jar selenium-server-standalone-2.44.0.jar -role node -hub http://192.168.56.1:4444/grid/register> -host 192.168.56.12 -browser browserName=htmlunit -browser browserName=phantomjs
  java -jar selenium-server-standalone-2.44.0.jar -role node -hub http://192.168.56.1:4444/grid/register> -host 192.168.56.13 -browser browserName=firefox,platform=LINUX -browser browserName=chrome
Indeed, by default, a node starts Firefox, Chrome and Internet Explorer.
So if you intented to test other browsers, you must provide a node config.
Also keep in mind that it is recommanded to don't have more than 6 browsers on a
same server.

But of course, using a JSON config for hub and node is a better approach.
Start the hub on one host:
  java -jar selenium-server-standalone-2.44.0.jar -role hub -hubConfig <myconfig>.json
Run nodes on other host machines :
  java -jar selenium-server-standalone-2.44.0.jar -role node -nodeConfig <myconfig>.json
  java -jar selenium-server-standalone-2.44.0.jar -role node -nodeConfig <myconfig>.json
Some examples of JSON config could be founded in the links below.

You can then check the grid config and nodes registered at :
http://<HUB_HOST>:<HUB_PORT>/grid/console

For more details about selenium grid :
see http://docs.seleniumhq.org/docs/07_selenium_grid.jsp
see https://code.google.com/p/selenium/wiki/Grid2

For more details about the protocol, see :
https://code.google.com/p/selenium/wiki/JsonWireProtocol
and the spec :
http://www.w3.org/TR/2013/WD-webdriver-20130117

For more details about desired capabilities :
https://code.google.com/p/selenium/wiki/DesiredCapabilities

Webdriver issues
----------------

- you can not get HTTP headers. For HTTP status codes as a work-around, the
  class helper inject asynchronous javascript to execute an HTTP request for the
  current url. In that way, this is still the remote browser which returns HTTP
  status code.
  For drivers which don't support well javascript injection (Opera), it use a
  curl user-agent with cookies to get HTTP headers.
  However, both are not great because request is executed twice. So you may
  override the method in charge of doing it to fit your needs. For example, you
  could return the corresponding HTTP status code for the current page markup.

  For PHP errors retrieved with headers "X-Drupal-Assertion-", a Drupal core
  patch is required to log error into a file instead.
  See section "Drupal" below.

  https://code.google.com/p/selenium/issues/detail?id=141

Firefox
-------

Works out-of-box ! The driver is an extension which is installed by the
standalone server.

However, there is one known issue on Linux platform only.
By default on Linux, native events is not enabled due to windows manager broken
(i.e : switching from window may not work with some windows manager). But it is
enabled by default by this module because it provide better integration with
advanced user interactions (like drag-n-drop). So depending on your test and
windows manager, you may be able to disable it.

See https://code.google.com/p/selenium/wiki/AdvancedUserInteractions
See https://code.google.com/p/selenium/wiki/NativeEventsOnLinux

Successfully tested with 32,33,34 and selenium :
- 2.42.2
- 2.43.0/1
- 2.44.0

Note that as soon as new FF updates are released, Selenium project may needs
to be updated too because it may include some fix/review, like native events.

Chrome
------

You need to download chromedriver and put it in your PATH (for e.g : /usr/bin,
with symlink).
ChromeDriver is also a Webdriver server, so it could be used directly without
the selenium server. However, you can't take advantages of the selenium server
features like grid and so on.

Command lines options availables :
http://peter.sh/experiments/chromium-command-line-switches/

Successfully tested with 38 and selenium :
- 2.42.2
- 2.43.0/1
- 2.44.0

See https://code.google.com/p/selenium/wiki/ChromeDriver

Opera
-----

Because there is two major versions of Opera which didn't use the same
renderring engine, there is two existing drivers :
- Presto (<= 12.x) : https://github.com/operasoftware/operaprestodriver
- Blink (WebKit, >= 15.x) : https://github.com/operasoftware/operachromiumdriver

Presto :

For Presto, even if some tricks could be done to support older versions, only
12.x version would be supported.

However, some features are not supported yet :

- execute script command throw UnknownServerException even if it works fine.
  However, we could only execute it. You can not retrieve result.
  For example, see : https://github.com/operasoftware/operadriver/issues/101
- window maximize is not supported
- tag name returned are uppercase but php-webdriver library deals with.
  See https://github.com/operasoftware/operadriver/issues/102
- you cannot upload file
  See https://github.com/operasoftware/operadriver/issues/84
- cookie could not be set if hostname is an IP ! As a result, you cannot be
  logged in...
- no WebDriver alert API implemented
  https://github.com/operasoftware/operadriver/issues/31

Successfully tested on Opera 12.16 on Linux platform with selenium :
- 2.42.2
- 2.43.0/1

Failed with 2.44.0 : https://github.com/operasoftware/operaprestodriver/issues/106

Blink :
Not tested yet.

See:
https://github.com/operasoftware/operaprestodriver
https://github.com/operasoftware/operachromiumdriver
https://code.google.com/p/selenium/wiki/OperaDriver

Phantom JS
----------

Download phantomJS >= 1.9.7 and put binary in our PATH (e.g symbolik link in
/usr/bin/phantomjs).

Only works with selenium server >= 2.43.0 (which embed ghostdriver) due to
issue :
- https://github.com/detro/ghostdriver/issues/354

Known issues :
- no WebDriver alert API implemented
  https://github.com/detro/ghostdriver/issues/20

Successfully tested with 1.9.7/8 on selenium server :
- 2.42.2
- 2.43.0/1

Failed with 2.44.0, see https://github.com/detro/ghostdriver/pull/399

See https://github.com/detro/ghostdriver

HTML unit
---------

Works out-of-box, however, there is some features which are not supported well:
- as it have no GUI, you cannot have screenshot image
- onblur event (on textarea or input type text) doesn't seems to work well and
  may needs some tricks (click on body for example to loose focus)
- does not support moving mouse with coordinates (no UI)
- getting source page is formatted by HTMLunit. So it is not returned like the
  web server do and thus, cause a lot of errors on text assertions.
- no WebDriver alert API implemented
- Keep in mind that even if javascript is enabled, you may have some unexpected
  behavior because like other browsers, it have its own implementation.
  Note that you can set the javascript engine by setting its version to :
  - firefox
  - internet explorer-7
  In addition, you can concatene it with '-VERSION_NUMBER' of the engine.
  For example, 'firefox-32'.
  See https://code.google.com/p/selenium/wiki/HtmlUnitDriver

Successfully tested on selenium server :
- 2.42.2
- 2.43.0/1
- 2.44.0

See https://code.google.com/p/selenium/wiki/HtmlUnitDriver

Internet explorer
-----------------

Download IEDriverServer binary >= 2.44.0.1 for your platform (32/64) at
http://selenium-release.storage.googleapis.com/index.html
and put it in your PATH (e.g : C:\Windows\System32).
IE driver server bellow are not supported (cookie no cache trouble).
Like Chrome driver, its implements its own WebDriver API and thus could be used
without the selenium server. However, this is not supported by this module.

Some requirements are needed in IE to work properly. Like protected zones,
registry key to add/alter, etc. See link bellow for more details.

Know issues :
- you can write cookie on a specific path but cannot retrieve it
- JS prompt on IE8 must be allowed (pop-up not blocked)
- you can not fetch HTTP only cookie not write by the driver

Be careful, all user data would be deleted !

IE8, IE9, IE10 and IE11 has been tested successfully with selenium server
- 2.42.2
- 2.43.0/2.43.1
- 2.44.0

and IE driver server :
- 2.42.0
- 2.43.0 (only IE9)
- 2.44.0.1

See https://code.google.com/p/selenium/wiki/InternetExplorerDriver

Safari
------

Selenium server embed Safari driver and install it as a Safari extension so
there is nothing to do.

Know issues with driver :
- sendkey TAB is not working. You can't simulate a loose focus to trigger JS
  event in that way.
  See : https://code.google.com/p/selenium/issues/detail?id=5403
- cookies could not be deleted
  See : https://code.google.com/p/selenium/issues/detail?id=5212
- pageLoadTimeout is not implemented yet
  See : https://code.google.com/p/selenium/issues/detail?id=6015
- file could not be uploaded
  See : https://code.google.com/p/selenium/issues/detail?id=4220
- don't support mouse moving
- cannot open new tab when clicking on a link with target blank if security user
  preferences block pop-up
- don't implement alert API
  https://code.google.com/p/selenium/issues/detail?id=5618
- history not supported (you can go back or forward but could not interact with
  the page
- you can not fetch HTTP only cookie not write by the driver

Be careful, all user data would be deleted !

Tested with Safari 7.0 on a mac OS X mavericks 10.9 with selenium server :
- 2.42.2
- 2.43.0/1
- 2.44.0

See : https://code.google.com/p/selenium/wiki/SafariDriver

Selendroid server
-------------------------------

The Android driver of selenium project have been abandoned in favor of the
Selendroid project : http://selendroid.io.

Set up an android development and check requirements :
http://selendroid.io/setup.html#systemRequirements

Then, download the standalone server >= 0.12.0 at
http://search.maven.org/remotecontent?filepath=io/selendroid/selendroid-test-app/0.12.0/selendroid-test-app-0.12.0.apk
and you could start it without a grid for emulator or real devices :
java -jar selendroid-standalone-0.12.0-with-dependencies.jar

Here is some tips for non android developer :
- install the JDK since java SDK is not really required for web.
- install android SDK. Be careful, some 32 libs must be installed if you are on
  64 bits.
- Set PATH where android SDK is installed.
  Linux example in ~/.bash_profile :
    export ANDROID_HOME=~/android-sdk-linux
    export PATH=${PATH}:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools
    # Point to the JDK, not JRE, with alternatives
    export JAVA_HOME=/usr/java/default
    PATH=$JAVA_HOME/bin:$PATH

- By default, not all SDK platform and system images are installed.
  You can do it with command line or with UI :
  -> ./<PATH_TO_ANDROID_SDK>/tools/android &
  -> then select proper packages to install

- you should then create a device and an AVD (android virtual device)
  -> ./<PATH_TO_ANDROID_SDK>/tools/android avd &
  -> in the device section, clone one of them and be sure to check :
     - enabled keyboard
     - enabled Dpad (if you want to play with)
     - set buttons to hardware
     - set a minimum of 1024B RAM
  -> after device is created, you could create an AVD from it.
     - target = API previously downloaded, for example 19
     - CPU/ABI = system image, prefer intel atom x86
     - check hardware keyboard is present
     - skin : no matter, dynamic hardware is enough
     - VM heap : minimum of 32MB
     - use host GPU if possible (no black screen)
  Emulator advices here : http://selendroid.io/setup.html#androidDevices

Any emulator created would be detected when Selendroid server would be started.

You should start AVD manually when possible.
Indeed, if not previously started, when Selendroid is executed, it will try to
start the AVD automatically if it match the desired capabilities. And thus,
would shutdown it too. So AVD would be started and closed for every test methods !
As it is time consuming, if AVD was started previously, Selendroid server don't
have to start it and so won't close it. Which would save you a lot of time for
every test methods executed.

In order to have real devices detected, in the phone/tablet device, do :
- unblock the development option
  -> 7 taps in settings > about phone > build number
  -> then go to settings > developer options > and enable USB debug

- plug-in your device in USB port with PTP protocol. Otherwise, you will get
  some permissions issues.

You should now be able to detect the device with ADB :
cd ~/<ANDROID_SDK>/platform-tools/
./adb devices

If the standalone server is already running, it should detect it and register it.

For both devices or emulator, sometimes, weird bug occurs (new AVD added for e.g)
and adb server needs to be restarted :
- shutdown Selendroid server
- ./<ANDROID_SDK_PATH>/platform-tools/adb kill-server
- ./<ANDROID_SDK_PATH>/platform-tools/adb start-server
- java -jar selendroid-standalone-0.12.0-with-dependencies.jar

GRID

To use Selendroid with Grid, you should :
- download Selenium server standalone
- download Selendroid grid plugin
and place all of them into the same directory (to copy/paste commands below) of
the Selendroid server standalone.

Then begin to start the hub :
  java -Dfile.encoding=UTF-8 -cp "selendroid-grid-plugin-<VERSION>.jar:selenium-server-standalone-<VERSION>.jar" org.openqa.grid.selenium.GridLauncher -role hub -host <HUB_HOST> -port <HUB_PORT> -capabilityMcher io.selendroid.grid.SelendroidCapabilityMatcher
So for instance :
  java -Dfile.encoding=UTF-8 -cp "selendroid-grid-plugin-0.12.0.jar:selenium-server-standalone-2.44.0.jar" org.openqa.grid.selenium.GridLauncher -role hub -host 127.0.0.1 -port 4444 -capabilityMcher io.selendroid.grid.SelendroidCapabilityMatcher
Note : command is for Unix and Mac only. See http://selendroid.io/scale.html
for Windows.

Then, start a node :
  java -jar selendroid-standalone-<VERSION>-with-dependencies.jar -host <NODE_HOST> -port <NODE_PORT> -deviceScreenshot
For example :
  java -jar selendroid-standalone-0.12.0-with-dependencies.jar -host localhost -port 5555 -deviceScreenshot

Then, you need to share nodes config. Create JSON files for each nodes (called
for example "selendroid-nodes-config.json") with a config which looks like :
{
  "capabilities": [
    {
      "browserName": "android",
      "androidTarget": "ANDROID18",
      "maxInstances": 1
    }
  ],
  "configuration": {
      "maxSession": 1,
      "register": true,
      "hubHost": "localhost",
      "hubPort": 4444,
      "remoteHost": "http://localhost:5555",
      "proxy": "io.selendroid.grid.SelendroidSessionProxy"
  }
}

Once config files are created, their content must be send to the hub server. You
can do it with HTTP POST requests :
   curl -H "Content-Type: application/json" -X POST --data @<JSON_FILE>.json http://<HUB_HOST>:<HUB_PORT>/grid/register
So for example :
  curl -H "Content-Type: application/json" -X POST --data @selendroid-nodes-config.json http://localhost:4444/grid/register
You should get a 200 code with a response body "ok".
Register all nodes config as needed.

Now, Grid is ready to be used and you can send all requests to the hub server.

Advices :
- to boost screenshot on emulator, you should launch Selendroid server with
  option -deviceScreenshot. Otherwise, ddmlib would be used and is very slow.
  If you cannot set it for any reason, you may override internal flag in the
  class of your test to disable screenshot support.

See http://selendroid.io/scale.html

Android with selendroid
-----------------------

In Drupal settings page:
- set the host of the server
- set browser capabilities (emulator, platform version, screen size)

Know issues :
- uploading file is not yet supported
  https://github.com/selendroid/selendroid/issues/413
- select option click throw exception with AJAX on device (and probably with
  emulator)
  Existing work-around (just ignore exception because all is fine).
  https://github.com/selendroid/selendroid/issues/635
- cookies cannot be set with path (always set to /).
  https://github.com/selendroid/selendroid/issues/626#issuecomment-57540809
- cookies cannot be deleted by name
  https://github.com/selendroid/selendroid/issues/363
- wait for page to load with POST form didn't work (existing work-around)
  https://github.com/selendroid/selendroid/issues/596
- advanced user actions is not completly implemented (gesture)
  https://github.com/selendroid/selendroid/issues/352
- windows switch is not implemented (you have only one window with webview)
  https://github.com/selendroid/selendroid/issues/119
  https://github.com/selendroid/selendroid/issues/230

Sucessfully tested on emulator and device with platform API 18 and selendroid :
- 0.12.0

Appium
------

Appium implements the JSON wire protocol with extra command based on a draft of
the mobile spec. It provide a PHP binding client which could not be used by this
module as it is an extension of an another library (an another PHP binding of
Webdriver), which itself extends the PHPunit framework.
So php-webdriver is still use and don't provide support for any extra commands.
However, this module add some extra commands like contexts.

Appium is a node JS application.

You should at least use the Appium > 1.3.4.
Indeed, this commit is required :
https://github.com/appium/appium/commit/a3a903893bc5b2b490a2c6b20623432941db991f

Before installing appium, you should first have an Android environment.
See below in Selendroid section how to.
See http://appium.io/slate/en/master/?ruby#requirements

There is a lot of ways to get it (npm, sources, etc).
Keep in mind that installing appium with npm must not be done with root user.
Here is an example of how to install it with NPM without root privileges on UNIX.
Once again, this is just an example :
- instead of using your package manager, download last node JS (embed NPM) at
  http://nodejs.org/download/
- extract the content of the node JS archive in the directory of your choice,
  make symbolik links to bin/node and bin/npm in /usr/bin/{} for example.
- set the prefix path where npm would store node modules. In your terminal :
    npm config set prefix <PATH_TO_STORE_BIN>
  For example :
    npm config set prefix /opt/npm/bin
  (directory exists with proper permissions)
  Then edit your ~/.bash_profile (the idea is to include this npm bin dir in
  your PATH) and add the following line :
    export PATH="$PATH:<PATH_TO_STORE_BIN>"
  So to illustrate the example above :
    export PATH="$PATH:/opt/npm/bin"
- once PATH is updated for your session, you should be able to install appium
  globally with the current user :
    npm install -g appium
- then you should be able to start it :
    appium

Otherwise, if you already have node and npm installed, get it running from
source is pretty simple :

- git clone https://github.com/appium/appium.git
- cd appium
- ./reset.sh --android
- npm install -g mocha
- npm install -g grunt-cli
- node bin/appium-doctor.js --dev
- node .

See https://github.com/appium/appium/blob/master/docs/en/contributing-to-appium/appium-from-source.md

Grid

Appium provide an integration with the Selenium Grid. In that way, you could
register nodes from Selenium and Appium all together !

Create a JSON config node file similar to Selenium.

However, note that to register your nodes, the easy way is to :
- in "configuration" object, set property "registerCycle": 5000
It would try to register it to the grid regulary (instead of doing it manually)

Be careful, for some commons browsers like "chrome", you may have some conflicts
between selenium nodes and appium nodes because browser name is the same. So you
should set specific capabilities on both Drupal config and node config to have
correct matches. For example, you can set a chrome on linux in Drupal config +
node config. And set a chrome on android platform in nodeconfig of Appium.

Then, start an Appium node with the *absolute* path to the config file :
  appium --nodeconfig <absolute_path>/nodeconfig.json

To check if node is register, take a look at :
http://<HUB_HOST>:<HUB_PORT>/grid/console

See https://github.com/appium/appium/blob/master/docs/en/advanced-concepts/grid.md

Appium drivers
--------------------------

For the moment, only Android platform are tested and supported.
iOS and FirefoxOS are untested and thus not integrate yet.

Emulator can't have Chrome installed because it could be only installed through
Play Store, which is not available on emulator. So the work-around is to install
Chromium (Chrome shell).
- Downloading the latest release at :
  http://commondatastorage.googleapis.com/chromium-browser-continuous/index.html?prefix=Android/
- launch the emulator
- install the both app with adb :
    adb install chrome-android/apks/ContentShell.apk
    adb install chrome-android/apks/ChromeShell.apk
Be careful, most of build seens to work only on ARM arch, which are very very
slow.
See : http://paul.kinlan.me/installing-chrome-for-android-on-an-emulator/

If you plan to use the ChromeDriver without proxy, you may be hit with
the following exception :
UnknownServerException: unknown error: an X display is required for keycode
conversions, consider using Xvfb.

You will need to install xvfb and run it with some settings like :
  sudo Xvfb :10 -ac -screen 0 4000x2000x8
See http://stackoverflow.com/questions/24126462/unknown-error-an-x-display-is-required-for-keycode-conversions

To run multiple instance of Android on a same machine (without grid), you need
to launch one appium server per device, with the targeted UDID (-U, which is the
device ID returns by adb list) and of course an another port (-p).
https://github.com/appium/appium/issues/462

Chrome beta is for purpose only and would be untested in favor of the stable
version.

Known issues :

Android

Chrome :
- click event could have wrong coordinates if keyboard popup is displayed
  https://code.google.com/p/chromedriver/issues/detail?id=982
- could not upload file
  https://github.com/appium/appium/issues/4210
  https://code.google.com/p/chromedriver/issues/detail?id=989
- orientation is not supported by chromedriver. Use Appium native instead.
  https://github.com/appium/appium/issues/3020
  https://code.google.com/p/chromedriver/issues/detail?id=921

Chromium :
- click event could have wrong coordinates if keyboard popup is displayed
  https://code.google.com/p/chromedriver/issues/detail?id=982
- could not upload file
  https://github.com/appium/appium/issues/4210
  https://code.google.com/p/chromedriver/issues/detail?id=989
- orientation is not supported by chromedriver. Use Appium native instead.
  https://github.com/appium/appium/issues/3020
  https://code.google.com/p/chromedriver/issues/detail?id=921

Browser (built-in/old stock) :
- orientation is not supported by chromedriver. Use Appium native instead.
  https://github.com/appium/appium/issues/3020
  https://code.google.com/p/chromedriver/issues/detail?id=921
- alert could not be accept/dismiss.
  https://github.com/appium/appium/issues/3081
  https://code.google.com/p/chromedriver/issues/detail?id=853

Successfully tested with appium v1.3.4 (REV 4c640df) :
- chrome : v39 on Nexus 10 (4.4) & Nexus 7 (5)
- chromium (chrome shell test): emulator ARM API 19, real device too
- old stock browser : emulator x86 API 19, real device too

See https://github.com/appium/appium/blob/master/docs/en
See https://github.com/appium/appium/blob/master/docs/en/writing-running-appium/caps.md
See https://github.com/appium/appium/blob/master/docs/en/writing-running-appium/mobile-web.md

Drupal
------

- Download php-webdriver library from https://github.com/facebook/php-webdriver
  and extract it to have a path which looks like :
  /sites/all/libraries/php-webdriver

- Enable the webdriver module.

Patches :
  Drupal core needs to be patched.
  You could apply only one patch which merge further ones :
  https://www.drupal.org/files/issues/2377319-core_patches-1.patch

  Otherwise, check in details :

  - Apply the following patch :
    https://www.drupal.org/files/issues/test_ua_valid_timeout-2330305-1.patch
    It fix the user-agent detect timeout which is too slow. Probably that this
    patch would be removed in favor of patch below. Both could not be combined.
  OR
    to also support simpletest sandbox for browsers which cannot support
    overriding HTTP header user-agent (all except Firefox, Chrome and PhantomJS)
    you should apply this patch instead :
    https://www.drupal.org/files/issues/test_ua_cookie-2335437-2.patch
    It also fix timeout detect but also add an another way to provide the
    testing user-agent hash with cookie.

  - Apply this patch to fix a bug core which prevent testing site fatal errors
    to be logged :
    https://www.drupal.org/files/issues/2367815-error-log-1.patch

  - Apply this patch to write testing error into a log instead of using HTTP
    headers which are not supported by WebDriver :
    https://www.drupal.org/files/issues/2368345-test_error_file-2.patch

- edit your settings.php to append :
  $conf['test_ua_valid_timeout'] = 500;
  Otherwise, tests may fail as it could exceed the default 5 second.
  $conf['test_log_errors_in_file'] = TRUE;
  Otherwise, you cannot get testing site errors.

- go to admin/config/development/testing/settings and :
  - set the path to the library "php-webdriver"
  - set yours servers and browsers config
  You can check that your server is running at admin/reports/status.

Each browsers config enabled should execute the tests.
Desired capabilities are sometimes optional (not always, windows platform is
required for Internet explorer browser).
By selecting further browsers, if tests are executed with simpletest UI, it
would execute the same test classes for every browsers selected.
With Selenium grid, you could execute tests on further machine which host these
browsers.

- Copy
  WEBDRIVER_MODULE/scripts/run-tests-webdriver.sh
  to
  DRUPAL_ROOT/scripts/run-tests-webdriver.sh
  with proper user rights.

  This script is a fork of the default core script/run-tests.sh. It could launch
  tests against further browsers enabled in parallel, or even further instances.
  For more detail about how to use it, run :
  sudo -u www-data php ./scripts/run-tests-webdriver.sh --help

- Then, you can start some basic tests provided by this module by going to
  admin/config/development/testing and execute test classes under group
  "WebDriver" to check if all is working fine.
  You can take a look in the test class (webdriver.test) to understand how to
  use the API, both from webdriver module or the php-webdriver library.

- Finally, if you are already familiar with writing simpletest test classes, you
  can write your own test classes by inheriting the DrupalWebDriverTestCase or
  DrupalWebDriverLiveTestCase class instead of DrupalWebTestCase.

About UI results management
--------------------------------------

GUI of simpletest is not designed to be extented. As a consequence, this module
won't provide support for it. Actually, it could be used, but with some
drawbacks :
* no fieldset result per browser
* no re-run methods for specific browsers

Instead, using the script "run-tests-webdriver.sh" is the privileged way to run
your tests and would have a full support.

About simpletest GUI, you should note that :
- you must have the variable 'simpletest_clear_results' set to FALSE if you want
  to keep your assertions in DB. Otherwise, results would be cleared on display.
  Even if you click on the "Clean environment" form button at
  admin/config/development/testing, DB assertions won't be cleared (it just
  remove DB tables prefix, temporary public files).
  The script take an another approach by default :
  - when launched, it clear previous test results
  - when finished, it keep new test results (DB + verbose).
  It means that the script get ride of the 'simpletest_clear_results' variable.
  But you could change this behavior by providing arguments to keep old results
  or clear new results like the default core script do.

  However, this variable won't prevent verbose directory (html files and
  screenshots) to be cleared when running next test suite with the simpletest
  GUI.
  On the contrary, the script provide a 'clean' command which have two levels :
  - clean environment (public files and DB prefix tables)
  - clean all, including previous and verbose + DB asserts

An additionnal settings webdriver_clear_verbose_results_on_success allow you to
clear verbose message directories (html and image screenshots) for successfull
methods only. Fail verbose messages are still keep.
Indeed, most of the time, you only care about failed verbose message. In that
way, you could save some free disk spaces with huge test suites.
However, because successfull assertions are still keep and thus may contains
links to deleted verbose files, this option is not set by default.

Testing live/external site
--------------------------

A class helper provide you a way to execute your tests againsts a live/external
site. For live site, this class should be used with caution because *everything*
would really happen !
Because it tests your live site, this module cannot offer testing support for
this class helper. It only quickly test an external site.

<?php
/**
 * @file
 * This script could run parallel WebDriver browsers tests from command line.
 */

if (strpos(dirname(__FILE__), '/webdriver/scripts') !== FALSE) {
  echo "This script must be moved into DRUPAL_ROOT/scripts/ directory.\n";
  exit;
}

define('WEBDRIVER_SCRIPT_COLOR_PASS', 32); // Green.
define('WEBDRIVER_SCRIPT_COLOR_FAIL', 31); // Red.
define('WEBDRIVER_SCRIPT_COLOR_EXCEPTION', 33); // Brown.

// Set defaults and get overrides.
list($args, $count) = webdriver_script_parse_args();
// Validate args once they have been global.
webdriver_script_validate_args();

if ($args['help'] || $count == 0) {
  webdriver_script_help();
  exit;
}

// Child process. Drupal bootstrap is not needed.
if ($args['execute-test']) {
  // Masquerade as Apache for running tests.
  webdriver_script_init("Apache");
  // Execute only one browser per process and then exit.
  webdriver_script_run_one_test($args['test-id'], $args['execute-test'], array_shift($args['browsers']), $args['methods']);
}
else {
  // Run administrative functions as CLI.
  webdriver_script_init(NULL);
}

// Bootstrap to perform initial validation or other operations.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
if (!module_exists('webdriver')) {
  webdriver_script_print_error("The webdriver module must be enabled before this script can run.");
  exit;
}

// Now that Drupal is bootstraped, timezone is set.
$args['clean-older'] = $args['clean-older'] ? strtotime($args['clean-older']) : 0;

// Clear command.
if ($args['clean']) {
  webdriver_script_clean('all', $args['clean-older']);
  // Detect test classes that have been added, renamed or deleted.
  registry_rebuild();
  cache_clear_all('simpletest', 'cache');
  exit;
}

// Validate browsers config.
if (!empty($args['browsers'])) {
  webdriver_script_browsers_validate($args['browsers']);
}
// Otherwise, set all capabilities from config.
else {
  webdriver_script_browsers_set_default();
}
// Default to the number of browsers if not provided.
$args['concurrency'] = $args['concurrency'] > 0 ? $args['concurrency'] : count($args['browsers']);

// Load SimpleTest files.
$groups = simpletest_test_get_all();
$all_tests = array();
foreach ($groups as $group => $tests) {
  $all_tests = array_merge($all_tests, array_keys($tests));
}
$test_list = array();

if ($args['list']) {
  // Display all available tests.
  echo "\nAvailable test groups & classes\n";
  echo   "-------------------------------\n\n";
  foreach ($groups as $group => $tests) {
    echo $group . "\n";
    foreach ($tests as $class => $info) {
      echo " - " . $info['name'] . ' (' . $class . ')' . "\n";
    }
  }
  exit;
}

$test_list = webdriver_script_get_test_list();
if ($args['methods']) {
  webdriver_script_methods_validate($test_list[0], $args['methods']);
}

// Try to allocate unlimited time to run the tests.
drupal_set_time_limit(0);

// By default, clear all previous test results before launching a new one.
if (!$args['keep-old']) {
  webdriver_script_clean('all');
}
// Optionnally, clear only older test results.
elseif ($args['clean-older']) {
  webdriver_script_clean('all', $args['clean-older']);
}

webdriver_script_reporter_init();

// Setup database for test results.
$test_id = db_insert('simpletest_test_id')
  ->useDefaults(array('test_id'))
  ->fields(array(
    'timestamp' => $_SERVER['REQUEST_TIME'],
  ))
  ->execute();

// Execute tests.
webdriver_script_execute_batch($test_id, $test_list);

// Retrieve the last database prefix used for testing and the last test class
// that was run from. Use the information to read the log file in case any
// fatal errors caused the test to crash.
list($last_prefix, $last_test_class) = simpletest_last_test_get($test_id);
simpletest_log_read($test_id, $last_prefix, $last_test_class);

// Stop the timer.
webdriver_script_reporter_timer_stop();

// Display results.
webdriver_script_reporter_display_results();

if ($args['xml']) {
  webdriver_script_reporter_write_xml_results();
}

// By default we force DB asserts to be keep.
if ($args['clean-result']) {
  webdriver_script_force_clean_results_table($test_id);
}

// Test complete, exit.
exit;

/**
 * Print help text.
 */
function webdriver_script_help() {
  global $args;

  echo <<<EOF

Run Drupal tests from the shell.

Usage:        {$args['script']} [OPTIONS] <tests>
Example:      {$args['script']} Profile

All arguments are long options.

  --help          Print this page.

  --list          Display all available test groups.

  --clean         Clean previous tests and then exits (no tests are run).
                  The following options are available :
                  - env : remove db tables prefix and files (public/private/temp)
                  - all : same as "env" but also remove verbose directory and
                        force DB assert to be deleted.

  --keep-old      Whether to keep all from previous test suites before launching
                  the new test suite. Default to FALSE.

  --clean-older   Used with "--clean" or "--keep-old" option, you could
                  optionnally cleared only test suite results which are older or
                  equals than the date provided.
                  The date format is any valid PHP relative date format, e.g :
                  - "2014-10-30 10:15:12PM"
                  - "2014-10-30 22:15:12"
                  - "2014-10-30"
                  - "-5 days"
                  - "previous month"
                  - "yesterday midnight"
                  Tips : don't forgot to wrap arguments by quotes !

  --clean-result  Whether to clear current result tests (DB assert) after being
                  output in the console. Default to FALSE. If TRUE, you could
                  view test results in UI only once. Usefull if you export
                  result in XML file and want to clear immediatly DB asserts.

  --url           Immediately precedes a URL to set the host and path. You will
                  need this parameter if Drupal is in a subdirectory on your
                  localhost and you have not set \$base_url in settings.php.
                  Tests can be run under SSL by including https:// in the URL.

  --php           The absolute path to the PHP executable. Usually not needed.

  --all           Run all available tests.

  --class         Run tests identified by specific class names, instead of group
                  names.

  --file          Run tests identified by specific file names, instead of group
                  names. Specify the path and the extension
                  (i.e. 'modules/user/user.test').

  --methods       A comma separated list of methods to execute for a given test
                  test class to executed.

  --browser       A comma separated list of browser's capabilities. Each option
                  have a value affected by '='. Capabilities example :
                  Globals :
                  - browserName : required
                  - server : A server type to use in case of conflict browser
                             name. If omitted, it would be executed againsts
                             each matching server types.
                  Selenium :
                  - version
                  - platform
                  Selendroid :
                  - platformVersion
                  - emulator
                  - screenSize
                  Appium :
                  - platformName : required
                  - deviceName : required
                  - platformVersion
                  The form is browserName=firefox,version=32,platform=linux.
                  This argument could be repeated as many browsers should
                  be executed in parallel with Grid.
                  If omitted, all browsers available from config would be
                  executed.

  --concurrency   The maximum number of children processes created. With Grid,
                  you could execute tests in parallel.
                  If omitted, concurrency is default to the number of browsers
                  availables.
                  Note that if browser create new fresh profile, you could
                  launch test classes in parallel for this same browser driver.

  --xml           <path>

                  If provided, test results will be written as xml files to this
                  path. Each test classes would generate a new XML file.

  --color         Output text format results with color highlighting.

  --verbose       Output detailed assertion messages in addition to summary.

  <test1>[,<test2>[,<test3> ...]]

                  One or more tests to be run. By default, these are interpreted
                  as the names of test groups as shown at
                  ?q=admin/config/development/testing.
                  These group names typically correspond to module names like
                  "User" or "Profile" or "System", but there is also a group
                  "XML-RPC".
                  If --class is specified then these are interpreted as the
                  names of specific test classes whose test methods will be run.
                  Tests must be separated by commas. Ignored if --all is
                  specified.

To run this script you will normally invoke it from the root directory of your
Drupal installation as the webserver user (differs per configuration), or root:

sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com WebDriver

Clean all test suites then exit :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']} --clean all

Clean all test suites older than 4 days, then exit :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --clean all
  --clean-older "-4 days"

Execute all test of group "WebDriver" for Firefox, Chrome and IE9 :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  --browser browserName=firefox
  --browser browserName=chrome,platform=windows
  --browser browserName=internet_explorer,platform=windows,version=9
  WebDriver

Execute test class "WebDriverLinkTestCase" for Firefox, Chrome and IE9 with
color highlighting :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  --class
  --color
  --browser browserName=firefox
  --browser browserName=chrome,platform=windows
  --browser browserName=internet_explorer,platform=windows,version=9
  WebDriverLinkTestCase

Execute all test of group "WebDriver" with all browsers defined in the config :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  WebDriver

Execute all test of group "WebDriver" with Firefox browser, without any extra
capabilities :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  --browser browserName=firefox
  WebDriver

Execute all test of group "WebDriver" and keep previous results :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  --browser browserName=firefox
  --keep-old
  WebDriver

Same as previous but keep only test results of previous week :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  --browser browserName=firefox
  --keep-old
  --clean-older "-7 days"
  WebDriver

Execute all test of group "WebDriver", output results into xml files of the
given directory and then clear results :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  --server selenium
  --browser browserName=firefox
  --xml /var/www/myproject/test_xml
  --clean-result
  WebDriver

Execute test classes "WebDriverTitleTestCase" and "WebDriverLinkTestCase" with
verbose output in sequential mode with all server types config :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  --verbose
  --concurrency 1
  --class
  WebDriverTitleTestCase,WebDriverLinkTestCase

Execute all test of group "WebDriver" for an android emulator in platform
version 18 screen size 1200x1920 ONLY with selendroid :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  --browser server=selendroid,browserName=selendroid,platformVersion=18,emulator=1,screenSize=1200x1920
  WebDriver

Execute all test of group "WebDriver" for an android emulator with Appium and
Chromium browser :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  --color
  --browser browserName=appium-chromium,platformName=Android,deviceName="Android Emulator"
  WebDriver

Execute only some methods of a given test class for every capabilities set in
config :
sudo -u [wwwrun|www-data|etc] php ./scripts/{$args['script']}
  --url http://example.com
  --color
  --class
  --methods testCookieAdd,testCookieDelete
  WebDriverCookiesTestCase
\n
EOF;
}

/**
 * Parse execution argument and ensure that all are valid.
 *
 * @return The list of arguments.
 */
function webdriver_script_parse_args() {
  // Set default values.
  $args = array(
    'script' => '',
    'help' => FALSE,
    'list' => FALSE,
    'clean' => '',
    'clean-older' => '',
    'clean-result' => FALSE,
    'keep-old' => FALSE,
    'url' => '',
    'php' => '',
    'all' => FALSE,
    'file' => FALSE,
    'class' => FALSE,
    'methods' => array(),
    'browser' => array(),
    'concurrency' => 0,
    'color' => FALSE,
    'verbose' => FALSE,
    'xml' => '',
    'test_names' => array(),
    // Used internally.
    'test-id' => 0,
    'execute-test' => '',
    'browsers' => array(),
  );

  // Override with set values.
  $args['script'] = basename(array_shift($_SERVER['argv']));

  $i = $count = 0;
  while ($arg = array_shift($_SERVER['argv'])) {
    if (preg_match('/--(\S+)/', $arg, $matches)) {

      // Argument found.
      if (array_key_exists($matches[1], $args)) {
        // Argument found in list.
        $previous_arg = $matches[1];
        if (is_bool($args[$previous_arg])) {
          $args[$matches[1]] = TRUE;
        }
        elseif ($matches[1] === 'browser') {
          // Build internal arguments.
          $capabilities = explode(',', array_shift($_SERVER['argv']));
          foreach ($capabilities as $capability) {
            list($property, $value) = explode('=', $capability);
            $value = $property === 'platform' ? strtoupper($value) : $value;
            $args['browsers'][$i][$property] = $value;
          }
          $i++;
        }
        elseif (is_array($args[$matches[1]])) {
          $args[$matches[1]] = explode(',', array_shift($_SERVER['argv']));
        }
        else {
          $args[$matches[1]] = array_shift($_SERVER['argv']);
        }
        // Clear extraneous values.
        $args['test_names'] = array();
        $count++;
      }
      else {
        // Argument not found in list.
        webdriver_script_print_error("Unknown argument '$arg'.");
        exit;
      }
    }
    else {
      // Values found without an argument should be test names.
      $args['test_names'] += explode(',', $arg);
      $count++;
    }
  }

  return array($args, $count);
}

/**
 * Validate arguments from CLI. Thus, we could have color argument for e.g.
 */
function webdriver_script_validate_args() {
  global $args;

  // Check required properties.
  if ($args['clean'] && !in_array($args['clean'], array('env', 'all'), TRUE)) {
    webdriver_script_print_error('--clean value is not correct. Should be "env" or "all".');
    exit;
  }

  // Date timezone is not set yet (no Drupal bootstrap).
  if ($args['clean-older'] && !@strtotime($args['clean-older'])) {
    webdriver_script_print_error('--clean-older is not a valid date.');
    exit;
  }

  if ($args['concurrency'] && (int)$args['concurrency'] <= 0) {
    webdriver_script_print_error('"--concurrency" must be a positive integer.');
    exit;
  }

  if ($args['methods']) {
    if (!$args['class'] && !$args['execute-test']) {
      webdriver_script_print_error('--methods must be used with --class.');
      exit;
    }
    elseif (count($args['test_names']) > 1) {
      webdriver_script_print_error('--methods must be used with only one test class.');
      exit;
    }
  }
}

/**
 * Initialize script variables and perform general setup requirements.
 */
function webdriver_script_init($server_software) {
  global $args, $php;

  $host = 'localhost';
  $path = '';
  // Determine location of php command automatically, unless a command line argument is supplied.
  if (!empty($args['php'])) {
    $php = $args['php'];
  }
  elseif ($php_env = getenv('_')) {
    // '_' is an environment variable set by the shell. It contains the command that was executed.
    $php = $php_env;
  }
  elseif ($sudo = getenv('SUDO_COMMAND')) {
    // 'SUDO_COMMAND' is an environment variable set by the sudo program.
    // Extract only the PHP interpreter, not the rest of the command.
    list($php, ) = explode(' ', $sudo, 2);
  }
  else {
    webdriver_script_print_error('Unable to automatically determine the path to the PHP interpreter. Supply the --php command line argument.');
    webdriver_script_help();
    exit();
  }

  // Get URL from arguments.
  if (!empty($args['url'])) {
    $parsed_url = parse_url($args['url']);
    $host = $parsed_url['host'] . (isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '');
    $path = isset($parsed_url['path']) ? $parsed_url['path'] : '';

    // If the passed URL schema is 'https' then setup the $_SERVER variables
    // properly so that testing will run under HTTPS.
    if ($parsed_url['scheme'] == 'https') {
      $_SERVER['HTTPS'] = 'on';
    }
  }

  $_SERVER['HTTP_HOST'] = $host;
  $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
  $_SERVER['SERVER_ADDR'] = '127.0.0.1';
  $_SERVER['SERVER_SOFTWARE'] = $server_software;
  $_SERVER['SERVER_NAME'] = 'localhost';
  $_SERVER['REQUEST_URI'] = $path .'/';
  $_SERVER['REQUEST_METHOD'] = 'GET';
  $_SERVER['SCRIPT_NAME'] = $path .'/index.php';
  $_SERVER['PHP_SELF'] = $path .'/index.php';
  $_SERVER['HTTP_USER_AGENT'] = 'Drupal command line';

  if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
    // Ensure that any and all environment variables are changed to https://.
    foreach ($_SERVER as $key => $value) {
      $_SERVER[$key] = str_replace('http://', 'https://', $_SERVER[$key]);
    }
  }

  chdir(realpath(dirname(__FILE__) . '/..'));
  define('DRUPAL_ROOT', getcwd());
  require_once DRUPAL_ROOT . '/includes/bootstrap.inc';
}

/**
 * Function helper to validate browsers capabilities provided from command line
 * when Drupal is fully bootstrapped.
 *
 * @param array $browsers
 *   An array of array containing capabilities.
 */
function webdriver_script_browsers_validate(array $browsers) {
  $servers_infos = webdriver_servers_info();

  foreach ($browsers as $capabilities) {

    if (!isset($capabilities['browserName'])) {
      webdriver_script_print_error("--browser must have at least browserName property.");
      exit;
    }

    // Validate browsers againsts each server config which support it if not
    // declared explicitly.
    $servers_config = isset($capabilities['server']) && isset($servers_infos[$capabilities['server']])
      ? array($servers_infos[$capabilities['server']])
      : $servers_infos;

    foreach ($servers_infos as $server_infos) {
      $server = new $server_infos['class']();
      if (in_array($capabilities['browserName'], $server->getBrowsers(), TRUE)) {

        $msgs = $server->capabilitiesValidate($capabilities);
        if ($msgs) {
          foreach ($msgs as $msg) {
            webdriver_script_print_error($msg);
          }
          exit;
        }
      }
    }
  }
}

/**
 * Function helper to override global $browsers with config. Could not be done
 * while fetching command line arguments because arguments may be send by this
 * script for children processes.
 */
function webdriver_script_browsers_set_default() {
  global $args;
  $browsers = array();

  $servers_config = variable_get('webdriver_servers', array());
  foreach ($servers_config as $server_config) {
    // Remove disable browsers.
    $server_config['browsers'] = array_filter($server_config['browsers'], function($capabilities) {
      return isset($capabilities['enabled']) ? $capabilities['enabled'] : TRUE;
    });
    // Merge browsers from server types.
    $browsers = array_merge($browsers, $server_config['browsers']);
  }

  if (!$browsers) {
    webdriver_script_print_error('No enabled browsers founded from server config.');
    exit;
  }
  foreach ($browsers as $key => $browser) {
    unset($browsers[$key]['enabled']);
  }

  $args['browsers'] = $browsers;
}

/**
 * Clean up testing environnement with different alternatives instead of using
 * simpletest_clean_environment() function.
 *
 * @param string $type
 *   Should be either 'all' or 'env'.
 * @param int $timestamp
 *   Optionally a timestamp to clear previous test suites older than.
 */
function webdriver_script_clean($type = 'all', $timestamp = 0) {

  webdriver_clean_environment($type, array(), $timestamp);

  echo "\nDrupal test clean-up\n";
  echo "--------------------\n";
  echo "Type : $type\n";
  echo sprintf("Older than : %s\n\n", $timestamp ? format_date($timestamp) : "all");

  $messages = drupal_get_messages('status');
  foreach ($messages['status'] as $text) {
    echo " - " . $text . "\n";
  }
}

/**
 * Execute a batch of tests.
 */
function webdriver_script_execute_batch($test_id, $test_classes) {
  global $args;
  $browsers = $args['browsers'];
  $concurrency = $args['concurrency'];

  // Gather test cases by test classes and capabilities.
  $test_cases = array();
  foreach ($test_classes as $test_class) {
    foreach ($browsers as $capabilities) {
      $test_cases[] = array($test_class, $capabilities);
    }
  }

  // Execute all test cases.
  do {

    // Create a limited number of processes to prevent collisions.
    $children = array();
    do {
      list($test_class, $capabilities) = array_shift($test_cases);

      // Prepare command for fork and create it.
      $command = webdriver_script_command($test_id, $test_class, $capabilities);
      $process = proc_open($command, array(), $pipes, NULL, NULL, array('bypass_shell' => TRUE));
      if (!is_resource($process)) {
        echo "Unable to fork test process. Aborting.\n";
        exit;
      }

      // Register our new child process for browser.
      $children[] = array(
        'process' => $process,
        'class' => $test_class,
        'capabilities' => $capabilities,
        'pipes' => $pipes,
      );
    }
    while (count($children) < $concurrency && $test_cases);

    // Loop until all children processes haven't finish to go to the next test
    // class/capabilities.
    do {
      // Wait for children every 200ms.
      usleep(200000);

      foreach ($children as $cid => $child) {
        $status = proc_get_status($child['process']);
        if (empty($status['running'])) {
          // The child exited, unregister it.
          proc_close($child['process']);
          if ($status['exitcode']) {
            echo 'ERROR with a non-zero error code (' . $status['exitcode'] . ") for :\n";
            echo " - test class : $test_class\n";
            echo " - capabilities : " . var_export($capabilities, TRUE) . "\n";
          }
          unset($children[$cid]);
        }
      }
    }
    while (count($children) > 0);
  }
  while ($test_cases);
}

/**
 * Bootstrap Drupal and run a single test.
 *
 * @param array $capabilities
 *   An array of capabilities.
 * @param array $methods
 *   An array of test class methods to execute only.
 */
function webdriver_script_run_one_test($test_id, $test_class, array $capabilities, array $methods = array()) {
  try {
    // Bootstrap Drupal.
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

    simpletest_classloader_register();

    // Get server type and remove it before launching the test.
    $server_type = isset($capabilities['server']) ? $capabilities['server'] : '';
    unset($capabilities['server']);

    $test = new $test_class($test_id);
    $test->run($methods, $server_type, array($capabilities));
    $info = $test->getInfo();

    $had_fails = (isset($test->results['#fail']) && $test->results['#fail'] > 0);
    $had_exceptions = (isset($test->results['#exception']) && $test->results['#exception'] > 0);
    $status = ($had_fails || $had_exceptions ? 'fail' : 'pass');

    $output = array();
    $output[] = $capabilities['browserName'];
    unset($capabilities['browserName']);
    if ($server_type) {
      $output[] = $server_type;
    }
    foreach ($capabilities as $capability => $value) {
      $output[] = "$capability:$value";
    }
    $output = implode(', ', $output);
    // We don't really care about debug messages.
    $test->results['#debug'] = FALSE;
    $output .= ' - ' . $info['name'] . ' - ' . _simpletest_format_summary_line($test->results) . "\n";
    webdriver_script_print($output, webdriver_script_color_code($status));

    // Finished, kill this runner.
    exit(0);
  }
  catch (Exception $e) {
    echo (string) $e;
    exit(1);
  }
}

/**
 * Return a command used to run a test in a separate process.
 *
 * @param $test_id
 *  The current test ID.
 * @param $test_class
 *  The name of the test class to run.
 * @param $capabilities
 *   An array of capabilities to execute tests.
 */
function webdriver_script_command($test_id, $test_class, array $capabilities) {
  global $args, $php;

  $command = escapeshellarg($php) . ' ' . escapeshellarg('./scripts/' . $args['script']) . ' --url ' . escapeshellarg($args['url']);
  if ($args['color']) {
    $command .= ' --color';
  }
  $browser_args = array();
  foreach ($capabilities as $capability => $value) {
    $browser_args[] = "$capability=$value";
  }
  $command .= " --browser " . escapeshellarg(implode(',', $browser_args));
  if ($args['methods']) {
    $command .= " --methods " . escapeshellarg(implode(',', $args['methods']));
  }
  $command .= " --php " . escapeshellarg($php) . " --test-id $test_id --execute-test " . escapeshellarg($test_class);

  return $command;
}

/**
 * Get list of tests based on arguments. If --all specified then
 * returns all available tests, otherwise reads list of tests.
 *
 * Will print error and exit if no valid tests were found.
 *
 * @return List of tests.
 */
function webdriver_script_get_test_list() {
  global $args, $all_tests, $groups;

  $test_list = array();
  if ($args['all']) {
    $test_list = $all_tests;
  }
  else {
    if ($args['class']) {
      // Check for valid class names.
      $test_list = array();
      foreach ($args['test_names'] as $test_class) {
        if (class_exists($test_class)) {
          $test_list[] = $test_class;
        }
        else {
          $groups = simpletest_test_get_all();
          $all_classes = array();
          foreach ($groups as $group) {
            $all_classes = array_merge($all_classes, array_keys($group));
          }
          webdriver_script_print_error('Test class not found: ' . $test_class);
          webdriver_script_print_alternatives($test_class, $all_classes, 6);
          exit(1);
        }
      }
    }
    elseif ($args['file']) {
      $files = array();
      foreach ($args['test_names'] as $file) {
        $files[drupal_realpath($file)] = 1;
      }

      // Check for valid class names.
      foreach ($all_tests as $class_name) {
        $refclass = new ReflectionClass($class_name);
        $file = $refclass->getFileName();
        if (isset($files[$file])) {
          $test_list[] = $class_name;
        }
      }
    }
    else {
      // Check for valid group names and get all valid classes in group.
      foreach ($args['test_names'] as $group_name) {
        if (isset($groups[$group_name])) {
          $test_list = array_merge($test_list, array_keys($groups[$group_name]));
        }
        else {
          webdriver_script_print_error('Test group not found: ' . $group_name);
          webdriver_script_print_alternatives($group_name, array_keys($groups));
          exit(1);
        }
      }
    }
  }

  if (empty($test_list)) {
    webdriver_script_print_error('No valid tests were specified.');
    exit;
  }
  return $test_list;
}

/**
 * Method helper to check if provided methods exist in the given test class.
 *
 * @param string $class
 *   The test class to execute.
 * @param array $methods
 *   An array of methods which should exists in the given class.
 */
function webdriver_script_methods_validate($class, array $methods) {
  $class_methods = get_class_methods($class);
  if (count($methods) !== count(array_intersect($methods, $class_methods))) {
    webdriver_script_print_error("Some methods provided are not founded.");
    exit;
  }
}

/**
 * Initialize the reporter.
 */
function webdriver_script_reporter_init() {
  global $args, $all_tests, $test_list, $results_map;

  $results_map = array(
    'pass' => 'Pass',
    'fail' => 'Fail',
    'exception' => 'Exception'
  );

  echo "\n";
  echo "Drupal test run\n";
  echo "---------------\n";
  echo "\n";

  // Tell the user about what tests are to be run.
  if ($args['all']) {
    echo "All tests will run.\n\n";
  }
  else {
    echo "Tests to be run:\n";
    foreach ($test_list as $class_name) {
      $info = call_user_func(array($class_name, 'getInfo'));
      echo " - " . $info['name'] . ' (' . $class_name . ')' . "\n";
    }
    echo "\n";
  }

  if ($args['methods']) {
    echo "with methods :\n";
    foreach ($args['methods'] as $method) {
      echo " - $method\n";
    }
    echo "\n";
  }

  if ($args['browsers']) {
    echo "Tests run on browsers:\n";
    foreach ($args['browsers'] as $capabilities) {

      $summary = array();
      $summary[] = $capabilities['browserName'];
      unset($capabilities['browserName']);
      foreach ($capabilities as $capability => $value) {
        $summary[] = "$capability : $value";
      }
      $summary = implode(', ', $summary);
      echo " - $summary\n";
    }
    echo "\n";
  }

  echo 'Concurrency: ' . $args['concurrency'] . "\n\n";

  echo "Test run started:\n";
  echo " " . format_date($_SERVER['REQUEST_TIME'], 'long') . "\n";
  timer_start('run-tests');
  echo "\n";

  echo "Test summary\n";
  echo "------------\n";
  echo "\n";
}

/**
 * Display jUnit XML test results.
 */
function webdriver_script_reporter_write_xml_results() {
  global $args, $test_id, $results_map;

  $results = db_query("SELECT * FROM {simpletest} WHERE test_id = :test_id ORDER BY test_class, browser, capabilities, server_type, message_id", array(':test_id' => $test_id));

  $test_class = '';
  $xml_files = array();

  foreach ($results as $result) {
    if (isset($results_map[$result->status])) {

      // We've moved onto a new class, so write the last classes results to a file:
      if ($result->test_class != $test_class) {
        if (isset($xml_files[$test_class])) {
          file_put_contents($args['xml'] . '/' . $test_class . '.xml', $xml_files[$test_class]['doc']->saveXML());
          unset($xml_files[$test_class]);
        }
        $test_class = $result->test_class;
        if (!isset($xml_files[$test_class])) {
          $doc = new DomDocument('1.0');
          $root = $doc->createElement('testsuite');
          $root = $doc->appendChild($root);
          $xml_files[$test_class] = array('doc' => $doc, 'suite' => $root);
        }
      }

      // For convenience:
      $dom_document = &$xml_files[$test_class]['doc'];

      // We may moved into an another browser for this same/new test class.
      // Including none in case of error before setting capabilities.
      $browser_key = $result->browser . '||' . $result->server_type;
      $browser_key .= isset($result->capabilities) ? '||' . $result->capabilities : '';
      if (!isset($xml_files[$test_class][$browser_key])) {
        $capabilities = isset($result->capabilities) ? unserialize($result->capabilities) : array();

        $browser_case = $dom_document->createElement('browser');
        $browser_case->setAttribute('browserName', $result->browser ? $result->browser : 'Outside browser');
        $browser_case->setAttribute('serverType', $result->server_type);
        foreach ($capabilities as $capability => $value) {
          $browser_case->setAttribute($capability, $value);
        }
        $xml_files[$test_class]['suite']->appendChild($browser_case);
        $xml_files[$test_class][$browser_key] = $browser_case;
      }

      // Create the XML element for this test case:
      $case = $dom_document->createElement('testcase');
      $case->setAttribute('classname', $test_class);
      list($class, $name) = explode('->', $result->function, 2);
      $case->setAttribute('name', $name);

      // Passes get no further attention, but failures and exceptions get to add more detail:
      if ($result->status == 'fail') {
        $fail = $dom_document->createElement('failure');
        $fail->setAttribute('type', 'failure');
        $fail->setAttribute('line', $result->line);
        $fail->setAttribute('message', $result->message_group);
        $text = $dom_document->createTextNode($result->message);
        $fail->appendChild($text);
        $case->appendChild($fail);
      }
      elseif ($result->status == 'exception') {
        // In the case of an exception the $result->function may not be a class
        // method so we record the full function name:
        $case->setAttribute('name', $result->function);

        $fail = $dom_document->createElement('error');
        $fail->setAttribute('type', 'exception');
        $fail->setAttribute('line', $result->line);
        $fail->setAttribute('message', $result->message_group);
        $full_message = $result->message . "\n\nline: " . $result->line . "\nfile: " . $result->file;
        $text = $dom_document->createTextNode($full_message);
        $fail->appendChild($text);
        $case->appendChild($fail);
      }

      // Append the test case XML to the browser test suite.
      $xml_files[$test_class][$browser_key]->appendChild($case);
    }
  }
  // The last test case hasn't been saved to a file yet, so do that now:
  if (isset($xml_files[$test_class])) {
    file_put_contents($args['xml'] . '/' . $test_class . '.xml', $xml_files[$test_class]['doc']->saveXML());
    unset($xml_files[$test_class]);
  }
}

/**
 * Stop the test timer.
 */
function webdriver_script_reporter_timer_stop() {
  echo "\n";
  $end = timer_stop('run-tests');
  echo "Test run duration: " . format_interval($end['time'] / 1000);
  echo "\n\n";
}

/**
 * Display test results.
 */
function webdriver_script_reporter_display_results() {
  global $args, $test_id, $results_map;

  if ($args['verbose']) {
    // Report results.
    echo "Detailed test results\n";
    echo "---------------------\n";

    $results = db_query("SELECT * FROM {simpletest} WHERE test_id = :test_id ORDER BY test_class, browser, capabilities, server_type, message_id", array(':test_id' => $test_id));
    $test_class = '';
    $browser_key = NULL;
    foreach ($results as $result) {
      if (isset($results_map[$result->status])) {

        // Display test class every time results are for new test class.
        if ($result->test_class !== $test_class) {
          echo "\n\n#### $result->test_class ####";
          $test_class = $result->test_class;
          $browser_key = NULL;
        }

        // Display browser name and capabilities every time results are for
        // new browser/capabilities (inluding none).
        $key = $result->browser . '||' . $result->server_type;
        $key .= isset($result->capabilities) ? '||' . $result->capabilities : '';
        if ($key !== $browser_key) {
          $browser_key = $key;
          $capabilities = isset($result->capabilities) ? unserialize($result->capabilities) : array();

          $summary = array();
          $summary[] = $result->browser ? $result->browser : 'Outside browser';
          $summary[] = $result->server_type;
          foreach ($capabilities as $capability => $value) {
            $summary[] = "$capability: $value";
          }
          echo "\n\n---- " . implode(', ', $summary) . " ----\n";
          // Print table header.
          echo "Status   Filename             Line Function                                     \n";
          echo "--------------------------------------------------------------------------------\n";
        }

        webdriver_script_format_result($result);
      }
    }
  }

  // Only if DB assertions and verbose directory are keep.
  if (!$args['clean-result']) {
    $base_url = !empty($args['url']) ? $args['url'] : $GLOBALS['base_url'];
    echo "\n==> test details : $base_url/admin/config/development/testing/webdriver/results/$test_id \n\n";
  }
}

/**
 * Format the result so that it fits within the default 80 character
 * terminal size.
 *
 * @param $result The result object to format.
 */
function webdriver_script_format_result($result) {
  global $results_map, $color;

  $summary = sprintf("%-8.8s %-20.20s %4.4s %-45.45s\n",
    $results_map[$result->status],
    basename($result->file),
    $result->line,
    $result->function
  );

  webdriver_script_print($summary, webdriver_script_color_code($result->status));

  $lines = explode("\n", wordwrap(trim(strip_tags($result->message)), 80));
  foreach ($lines as $line) {
    echo "$line\n";
  }
}

/**
 * Print error message prefixed with "  ERROR: " and displayed in fail color
 * if color output is enabled.
 *
 * @param $message The message to print.
 */
function webdriver_script_print_error($message) {
  webdriver_script_print("  ERROR: $message\n", WEBDRIVER_SCRIPT_COLOR_FAIL);
}

/**
 * Print a message to the console, if color is enabled then the specified
 * color code will be used.
 *
 * @param $message The message to print.
 * @param $color_code The color code to use for coloring.
 */
function webdriver_script_print($message, $color_code) {
  global $args;
  if ($args['color']) {
    echo "\033[" . $color_code . "m" . $message . "\033[0m";
  }
  else {
    echo $message;
  }
}

/**
 * Get the color code associated with the specified status.
 *
 * @param $status The status string to get code for.
 * @return Color code.
 */
function webdriver_script_color_code($status) {
  switch ($status) {
    case 'pass':
      return WEBDRIVER_SCRIPT_COLOR_PASS;
    case 'fail':
      return WEBDRIVER_SCRIPT_COLOR_FAIL;
    case 'exception':
      return WEBDRIVER_SCRIPT_COLOR_EXCEPTION;
  }
  return 0; // Default formatting.
}

/**
 * Prints alternative test names.
 *
 * Searches the provided array of string values for close matches based on the
 * Levenshtein algorithm.
 *
 * @see http://php.net/manual/en/function.levenshtein.php
 *
 * @param string $string
 *   A string to test.
 * @param array $array
 *   A list of strings to search.
 * @param int $degree
 *   The matching strictness. Higher values return fewer matches. A value of
 *   4 means that the function will return strings from $array if the candidate
 *   string in $array would be identical to $string by changing 1/4 or fewer of
 *   its characters.
 */
function webdriver_script_print_alternatives($string, $array, $degree = 4) {
  $alternatives = array();
  foreach ($array as $item) {
    $lev = levenshtein($string, $item);
    if ($lev <= strlen($item) / $degree || FALSE !== strpos($string, $item)) {
      $alternatives[] = $item;
    }
  }
  if (!empty($alternatives)) {
    webdriver_script_print("  Did you mean?\n", WEBDRIVER_SCRIPT_COLOR_FAIL);
    foreach ($alternatives as $alternative) {
      webdriver_script_print("  - $alternative\n", WEBDRIVER_SCRIPT_COLOR_FAIL);
    }
  }
}

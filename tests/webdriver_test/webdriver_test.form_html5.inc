<?php

function webdriver_test_form_html5($form, &$form_state) {

  $form['color'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'color',
      'name' => 'color',
      'value' => variable_get('webdriver_test_form_html5_color', '#ffffff'),
    ),
    '#prefix' => '<div><div><strong>Color</strong></div>',
    '#suffix' => '</div>',
  );

  $form['date'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'date',
      'name' => 'date',
      // @see http://www.w3.org/TR/html-markup/input.date.html#input.date
      'value' => variable_get('webdriver_test_form_html5_date', '1996-12-19'),
    ),
    '#prefix' => '<div><div><strong>Date</strong></div>',
    '#suffix' => '</div>',
  );

  $form['datetime'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'datetime',
      'name' => 'datetime',
      // @see http://www.w3.org/TR/html-markup/input.datetime.html#input.datetime
      'value' => variable_get('webdriver_test_form_html5_datetime', '1996-12-19T16:39:57-08:00'),
    ),
    '#prefix' => '<div><div><strong>Datetime</strong></div>',
    '#suffix' => '</div>',
  );

  $form['datetime_local'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'datetime-local',
      'name' => 'datetime_local',
      // @see http://www.w3.org/TR/html-markup/input.datetime-local.html#input.datetime-local
      'value' => variable_get('webdriver_test_form_html5_datetime_local', '1985-04-12T23:20:50.52'),
    ),
    '#prefix' => '<div><div><strong>Datetime local</strong></div>',
    '#suffix' => '</div>',
  );

  $form['email'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'email',
      'name' => 'email',
      'value' => variable_get('webdriver_test_form_html5_email', 'foo-bar.baz@example.com'),
    ),
    '#prefix' => '<div><div><strong>Email</strong></div>',
    '#suffix' => '</div>',
  );

  $form['month'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'month',
      'name' => 'month',
      // @see http://www.w3.org/TR/html-markup/input.month.html
      'value' => variable_get('webdriver_test_form_html5_month', '1987-06'),
    ),
    '#prefix' => '<div><div><strong>Month</strong></div>',
    '#suffix' => '</div>',
  );

  $form['number'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'number',
      'name' => 'number',
      'min' => 1,
      'max' => 5,
      'value' => variable_get('webdriver_test_form_html5_number', 3),
    ),
    '#prefix' => '<div><div><strong>Number</strong></div>',
    '#suffix' => '</div>',
  );

  $form['range'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'range',
      'name' => 'range',
      'min' => 1,
      'max' => 10,
      'value' => variable_get('webdriver_test_form_html5_range', 7),
    ),
    '#prefix' => '<div><div><strong>Range</strong></div>',
    '#suffix' => '</div>',
  );

  $form['search'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'search',
      'name' => 'search',
      'value' => variable_get('webdriver_test_form_html5_search', 'Type your search'),
    ),
    '#prefix' => '<div><div><strong>Search</strong></div>',
    '#suffix' => '</div>',
  );

  $form['tel'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'tel',
      'name' => 'tel',
      // Sorry but can't resist :p !
      'value' => variable_get('webdriver_test_form_html5_tel', '08 36 65 65 65'),
    ),
    '#prefix' => '<div><div><strong>Tel</strong></div>',
    '#suffix' => '</div>',
  );

  $form['time'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'time',
      'name' => 'time',
      // @see http://www.w3.org/TR/html-markup/input.time.html#input.time
      'value' => variable_get('webdriver_test_form_html5_time', '17:39:57'),
    ),
    '#prefix' => '<div><div><strong>Time</strong></div>',
    '#suffix' => '</div>',
  );

  $form['url'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'url',
      'name' => 'url',
      'value' => variable_get('webdriver_test_form_html5_url', 'https://www.google.com'),
    ),
    '#prefix' => '<div><div><strong>URL</strong></div>',
    '#suffix' => '</div>',
  );

  $form['week'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'week',
      'name' => 'week',
      // @see http://www.w3.org/TR/html-markup/input.week.html#input.week
      'value' => variable_get('webdriver_test_form_html5_week', '2012-W40'),
    ),
    '#prefix' => '<div><div><strong>Week</strong></div>',
    '#suffix' => '</div>',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

function webdriver_test_form_html5_submit(&$form, &$form_state) {
  $form_state['values'] += $_POST;
  form_state_values_clean($form_state);

  foreach ($form_state['values'] as $key => $value) {
    variable_set('webdriver_test_form_html5_' . $key, $value);
  }
  drupal_set_message('Form submitted successfully.');
}

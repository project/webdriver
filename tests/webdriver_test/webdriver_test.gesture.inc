<?php

function webdriver_test_page_gesture() {
  $build = array();

  $base_path = drupal_get_path('module', 'webdriver_test');
  $build['#attached']['js'][] = "$base_path/js/lib/jquery.mobile-events.min.js";
  $build['#attached']['js'][] = "$base_path/js/webdriver_test.js";
  $build['#attached']['js'][] = array(
    'data' => array(
      'webdriverTouch' => array(
        'scroll' => variable_get('webdriver_test_gesture_scroll', FALSE),
      ),
    ),
    'type' => 'setting',
  );

  $build['wrapper'] = array(
    '#prefix' => '<div id="webdriver-touch">',
    '#suffix' => '</div>',
  );

  $build['wrapper']['messages'] = array(
    '#markup' => '<div id="messages"></div>',
  );

  $build['wrapper']['button_tap'] = array(
    '#type' => 'html_tag',
    '#tag' => 'button',
    '#attributes' => array(
      'id' => 'button-tap',
      'class' => array('button-tap'),
    ),
    '#value' => 'Single tap',
  );

  $build['wrapper']['button_double'] = array(
    '#type' => 'html_tag',
    '#tag' => 'button',
    '#attributes' => array(
      'id' => 'button-doubletap',
    ),
    '#value' => 'Double tap',
  );

  $build['wrapper']['button_longpress'] = array(
    '#type' => 'html_tag',
    '#tag' => 'button',
    '#attributes' => array(
      'id' => 'button-longpress',
    ),
    '#value' => 'Long press',
  );

  $build['wrapper']['slider'] = array(
    '#markup' =>
      '<form id="form-slider">'
      .  '<label for="slider-0">Input slider:</label>'
      .  '<input type="range" name="slider" id="slider-0" value="0" min="0" max="100" />'
      .'</form>',
  );

  return $build;
}

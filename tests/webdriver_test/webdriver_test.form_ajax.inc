<?php

/**
 * Ajax test form.
 */
function webdriver_test_form_ajax($form, &$form_state) {

  // Force it to use it as wrapper.
  $wrapper_id = 'webdriver-test-form-ajax-wrapper';
  $form['#prefix'] = '<div id="' . $wrapper_id . '">';
  $form['#suffix'] = '</div>';

  // Update persistent input values.
  if (!empty($form_state['values'])) {
    $form_state['storage'] = !empty($form_state['storage']) ? array_merge($form_state['storage'], $form_state['values']) : $form_state['values'];
  }

  // First step.
  $form['wrapper']['select'] = array(
    '#type' => 'select',
    '#title' => 'Step one',
    '#options' => array(
      'foo' => 'Foo',
      'bar' => 'Bar',
    ),
    '#empty_value' => '',
    '#default_value' => !empty($form_state['storage']['select']) ? $form_state['storage']['select'] : '',
    '#required' => TRUE,
    '#ajax' => array(
      'wrapper' => $wrapper_id,
      'callback' => 'webdriver_test_form_ajax_callback',
    ),
  );

  // Step two.
  if (!empty($form_state['storage']['select'])) {
    $form['wrapper']['radios'] = array(
      '#type' => 'radios',
      '#title' => 'Step two',
      '#options' => array(
        'foo' => 'Foo',
        'bar' => 'Bar',
        'baz' => 'Baz',
      ),
      '#default_value' => !empty($form_state['storage']['radios']) ? $form_state['storage']['radios'] : array(),
      '#ajax' => array(
        'wrapper' => $wrapper_id,
        'callback' => 'webdriver_test_form_ajax_callback',
      ),
    );
  }

  // Step three.
  if (!empty($form_state['storage']['radios'])) {
    $form['wrapper']['checkboxes'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Step three',
      '#options' => array(
        'foo' => 'Foo',
        'bar' => 'Bar',
        'baz' => 'Baz',
      ),
      '#default_value' => !empty($form_state['storage']['checkboxes']) ? $form_state['storage']['checkboxes'] : array(),
      '#required' => TRUE,
      '#ajax' => array(
        'wrapper' => $wrapper_id,
        'callback' => 'webdriver_test_form_ajax_callback',
      ),
    );
  }

  // Step four
  if (!empty($form_state['storage']['checkboxes'])) {
    $form['wrapper']['text'] = array(
      '#type' => 'textfield',
      '#title' => 'Step four',
      '#default_value' => !empty($form_state['storage']['text']) ? $form_state['storage']['text'] : '',
      '#ajax' => array(
        'wrapper' => $wrapper_id,
        'callback' => 'webdriver_test_form_ajax_callback',
      ),
    );
  }

  // Step five
  if (!empty($form_state['storage']['text'])) {
    $form['wrapper']['textarea'] = array(
      '#type' => 'textarea',
      '#title' => 'Step five',
      '#default_value' => !empty($form_state['storage']['textarea']) ? $form_state['storage']['textarea'] : '',
      '#ajax' => array(
        'wrapper' => $wrapper_id,
        'callback' => 'webdriver_test_form_ajax_callback',
      ),
    );
  }

  //Step six
  if (!empty($form_state['storage']['textarea'])) {
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next step'),
      '#ajax' => array(
        'wrapper' => $wrapper_id,
        'callback' => 'webdriver_test_form_ajax_callback',
      ),
    );
  }

  // Last step !
  if (!empty($form_state['storage']['next'])) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
  }

  return $form;
}

/**
 * Form AJAX callback.
 */
function webdriver_test_form_ajax_callback($form, &$form_state) {
  return $form;
}

/**
 * Submit form handler.
 */
function webdriver_test_form_ajax_submit($form, &$form_state) {

  if ($form_state['triggering_element']['#value'] === t('Next step')) {
    $form_state['storage'] = array_merge($form_state['storage'], $form_state['values']);
    $form_state['rebuild'] = TRUE;
  }
  else {
    foreach ($form_state['storage'] as $name => $value) {
      variable_set('webdriver_test_form_ajax_value_' . $name, $value);
    }
    drupal_set_message('Form was submitted successfully.');
  }
}

(function ($) {

Drupal.behaviors.webdriver_link = {
  attach: function (context, settings) {

    if (settings.webdriver && settings.webdriver.link) {

      $('#webdriver-link-trigger').click(function() {
        $(this).replaceWith(settings.webdriver.link);
      });
    }
  }
};

Drupal.behaviors.webdriver_text = {
  attach: function (context, settings) {

    $('#webdriver-text-trigger').click(function() {
      $('#webdriver-text-wrapper').html('Text set by <strong>JS</strong>.');
      $('#webdriver-text-not-unique-wrapper').html('Text not unique returns by <strong>JS</strong>. Text not unique returns by <strong>JS</strong>.');
    });
  }
};

Drupal.behaviors.webdriver_alert = {
  attach: function (context, settings) {

    $('#webdriver-alert').click(function() {
      alert('Close me');
    });

    $('#webdriver-confirm').click(function() {
      var result = confirm('Accept me');
      if (result) {
        $('#webdriver-text').html('<p>Confirm accepted</p>');
      }
      else {
        $('#webdriver-text').html('<p>Confirm dismiss</p>');
      }
    });

    $('#webdriver-prompt').click(function() {
      var text = prompt('Type something');
      if (text !== null && text !== "") {
        $('#webdriver-text').html('<p>Text provided is : ' + text + '</p>');
      }
    });
  }
};

Drupal.behaviors.webdriver_cookie = {
  attach: function (context, settings) {

    $('body.page-webdriver-cookie').once(function() {
      var cookie = $.cookie('webdriver_test_http_only');
      if (cookie) {
        $('#block-system-main .content').append('<p>HTTP only cookie is not HTTP only !</p>');
      }
    });
  }
}

Drupal.behaviors.webdriver_orientation = {
  attach: function (context, settings) {

    $('#webdriver-orientation').once('webdriver-orientation', function() {
      var $self = $(this);

      window.addEventListener("orientationchange", function() {
        // 0 = portrait view
        // -90 = landscape rotated right
        // 90 = landscape rotated left
        if (window.orientation === 0) {
          $self.html('<p>Orientation : portrait.</p>');
        }
        else {
          $self.html('<p>Orientation : landscape.</p>');
        }
      });

      // Trigger event to check first orientation at load page.
      var event = new CustomEvent('orientationchange');
      window.dispatchEvent(event);
    });
  }
};

Drupal.behaviors.webdriver_touch = {
  attach: function (context, settings) {

    $('#webdriver-touch', context).once(function() {
      var $container = $(this);
      var $msg_wrapper = $('#messages', $container);

      $('#button-tap', $container).singletap(function() {
        $msg_wrapper.html('Tap event');
      });

      $('#button-doubletap', $container).doubletap(function() {
        $msg_wrapper.html('Double tap event');
      });

      $('#button-longpress', $container).taphold(function() {
        $msg_wrapper.html('Tap hold event');
      });

      // Not working well with webdriver on specific element so use whole
      // document instead...
      if (settings.webdriverTouch.scroll) {
        $('body').scrollend(function() {
          $msg_wrapper.html('Scroll');
        });
      }
      else {
        $('body').swipe(function() {
          $msg_wrapper.html('Swipe');
        });
      }
    });
  }
};

}(jQuery));

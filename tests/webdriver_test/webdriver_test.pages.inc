<?php

function webdriver_test_page_link($path = FALSE) {
  $build = array();
  if ($path === 'test') {
    drupal_set_message('Test link clicked.');
  }
  elseif ($path === 'js') {
    drupal_set_message('Test JS link clicked.');
  }
  else {
    $build['#attached']['js'][] = drupal_get_path('module', 'webdriver_test') . '/js/webdriver_test.js';
    $build['#attached']['js'][] = array(
      'data' => array(
        'webdriver' => array(
          'link' => l('Webdriver JS link', 'webdriver/link/js'),
        ),
      ),
      'type' => 'setting',
    );

    $build['markup']['#markup'] = l('Webdriver test link', 'webdriver/link/test');
    $build['trigger'] = array(
      '#markup' => '<div><button type="button" id="webdriver-link-trigger">Click Me!</button></div>',
    );
  }
  return $build;
}

function webdriver_test_page_text() {
  $build = array();
  $build['#attached']['js'][] = drupal_get_path('module', 'webdriver_test') . '/js/webdriver_test.js';

  $build['text']['#markup'] = '<p id="webdriver-text-wrapper">Raw text returns by <strong>server</strong>.</p>';
  $build['text_notunique']['#markup'] = '<p id="webdriver-text-not-unique-wrapper">Text not unique returns by <strong>server</strong>. Text not unique returns by <strong>server</strong>.</p>';

  $build['trigger'] = array(
    '#markup' => '<div><button type="button" id="webdriver-text-trigger">Click Me!</button></div>',
  );

  return $build;
}

function webdriver_test_page_cookie() {
  $build = array();
  $build['#attached']['library'][] = array('system', 'jquery.cookie');
  $build['#attached']['js'][] = drupal_get_path('module', 'webdriver_test') . '/js/webdriver_test.js';

  foreach ($_COOKIE as $name => $value) {
    if (strpos($name, 'webdriver_test_') === 0) {
      $build[] = array(
        '#markup' => "<p>Cookie $name is present</p>",
      );
    }
  }

  return $build;
}

function webdriver_test_page_windows($new_window = FALSE) {
  $build = array();

  $build['markup'] = array(
    '#markup' => $new_window ? '<p id="webdriver-new-window">On new window</p>' : '<p id="webdriver-first-window">On first window</p>',
  );

  if (!$new_window) {
    $build['link'] = array(
      '#markup' => l('Target blank', 'webdriver/windows/1', array('attributes' => array('id' => 'webdriver-window-link', 'target' => '_blank'))),
    );
  }

  return $build;
}

function webdriver_test_page_alert() {
  $build = array();
  $build['#attached']['js'][] = drupal_get_path('module', 'webdriver_test') . '/js/webdriver_test.js';

  $build['alert'] = array(
    '#markup' => '<button id="webdriver-alert">Alert</button>',
  );

  $build['confirm'] = array(
    '#markup' => '<button id="webdriver-confirm">Confirm</button>',
  );

  $build['prompt'] = array(
    '#markup' => '<button id="webdriver-prompt">Prompt</button>',
  );

  $build['text'] = array(
    '#markup' => '<div id="webdriver-text"></div>',
  );

  return $build;
}

function webdriver_test_page_iframe($iframe = FALSE) {
  $build = array();

  if (!$iframe) {
    $build['iframe'] = array(
      '#markup' => '<div><p>Page container of the iframe</p></div><iframe id="webdriver-iframe" src="' . url('webdriver/iframe/1') . '">Iframe not supported?</iframe>',
    );
  }
  else {
    $build['content'] = array(
      '#markup' => '<p>Content of the iframe</p>',
    );
  }

  return $build;
}

function webdriver_test_page_orientation() {
  $build = array();
  $build['#attached']['js'][] = drupal_get_path('module', 'webdriver_test') . '/js/webdriver_test.js';

  $build['markup'] = array(
    '#markup' => '<div id="webdriver-orientation"></div>',
  );

  return $build;
}

function webdriver_test_page_fatal_error() {
  file_put_contents('public://error.log', "An unknown fatal error write by webdriver_test module.\n");
  a_function_which_not_exists();
}

function webdriver_test_page_error() {
  // PHP Warning
  3 / 0;
  // PDO exception.
  db_insert('node')->fields(array('nid' => 'foo'))->execute();
}

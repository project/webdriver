<?php

/**
 * Testing form.
 */
function webdriver_test_form($form, &$form_state) {
  // Force it to be sure.
  $form['#id'] = 'webdriver-test-form';

  $form['markup'] = array(
    '#markup' => '<div id="not-a-form-element">Not a form element</div>',
  );

  $form['text'] = array(
    '#type' => 'textfield',
    '#id' => 'text',
    '#name' => 'text',
    '#title' => 'input type text',
    '#default_value' => variable_get('webdriver_test_form_value_text', 'foo'),
  );
  $form['text_hidden'] = array(
    '#type' => 'textfield',
    '#title_display' => 'none',
    '#name' => 'text',
    '#title' => 'input type text',
    '#attributes' => array('style' => 'display:none;'),
    '#default_value' => variable_get('webdriver_test_form_value_text_hidden', 'bar'),
  );

  $form['autocomplete'] = array(
    '#type' => 'textfield',
    '#title' => 'input type text autocomplete',
    '#autocomplete_path' => 'webdriver/autocomplete',
    '#default_value' => variable_get('webdriver_test_form_value_autocomplete', 'test'),
  );

  $form['pass'] = array(
    '#type' => 'password',
    '#title' => 'input type password',
  );

  $form['hidden'] = array(
    '#type' => 'hidden',
    '#value' => 'foo',
  );

  $form['textarea'] = array(
    '#type' => 'textarea',
    '#title' => 'textarea',
    '#default_value' => variable_get('webdriver_test_form_value_textarea', 'foo'),
  );

  $form['group'] = array(
    '#type' => 'vertical_tabs',
  );

  $form['group_select'] = array(
    '#type' => 'fieldset',
    '#title' => 'Selects',
    '#collapsible' => TRUE,
    '#group' => 'group',
  );

  $form['group_select']['select_single'] = array(
    '#type' => 'select',
    '#title' => 'select not multiple',
    '#id' => 'select-single',
    '#options' => array(
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ),
    '#empty_option' => 'None',
    '#empty_value' => '',
    '#default_value' => variable_get('webdriver_test_form_value_select_single', 'foo'),
  );

  $form['group_select']['select_multiple'] = array(
    '#type' => 'select',
    '#title' => 'select multiple',
    '#id' => 'select-multiple',
    '#options' => array(
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ),
    '#multiple' => TRUE,
    '#default_value' => variable_get('webdriver_test_form_value_select_multiple', array('foo', 'baz')),
  );

  $form['group_checkboxes'] = array(
    '#type' => 'fieldset',
    '#title' => 'Checkboxes',
    '#collapsible' => TRUE,
    '#group' => 'group',
  );

  $form['group_checkboxes']['checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => 'checkbox',
    '#id' => 'checkbox-unchecked',
    '#default_value' => variable_get('webdriver_test_form_value_checkbox', FALSE),
  );

  $form['group_checkboxes']['checkbox_checked'] = array(
    '#type' => 'checkbox',
    '#title' => 'checkbox checked',
    '#id' => 'checkbox-checked',
    '#default_value' => variable_get('webdriver_test_form_value_checkbox_checked', TRUE),
  );

  $form['group_radios'] = array(
    '#type' => 'fieldset',
    '#title' => 'Radios',
    '#collapsible' => TRUE,
    '#group' => 'group',
  );

  $form['group_radios']['radios'] = array(
    '#type' => 'radios',
    '#title' => 'radios',
    '#options' => array(
      'foo' => 'Foo',
      'bar' => 'Bar',
      'baz' => 'Baz',
    ),
    '#default_value' => variable_get('webdriver_test_form_value_radios', 'foo'),
  );

  $form['group_files'] = array(
    '#type' => 'fieldset',
    '#title' => 'Files',
    '#collapsible' => TRUE,
    '#group' => 'group',
  );

  $form['group_files']['file'] = array(
    '#type' => 'file',
    '#title' => 'input type file',
    '#name' => 'files[file]',
  );

  $form['group_files']['file_managed'] = array(
    '#type' => 'managed_file',
    '#title' => 'input type file ajax',
    '#upload_location' => 'temporary://',
    '#default_value' => variable_get('webdriver_test_form_value_file_managed', 0),
  );

  // Non-submit elements.
  $form['group_buttons'] = array(
    '#type' => 'fieldset',
    '#title' => 'Buttons',
    '#collapsible' => TRUE,
    '#group' => 'group',
  );

  $form['group_buttons']['input_button'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'button',
      'onclick' => "alert('click on input type button');",
      'name' => 'input_button',
      'value' => 'Input button',
    ),
  );
  $form['group_buttons']['button'] = array(
    '#type' => 'html_tag',
    '#tag' => 'button',
    '#attributes' => array(
      'type' => 'button',
      'onclick' => "alert('click on button');",
      'name' => 'button',
    ),
    '#value' => 'Button',
  );
  $form['group_buttons']['reset'] = array(
    '#type' => 'html_tag',
    '#tag' => 'input',
    '#attributes' => array(
      'type' => 'reset',
      'name' => 'reset',
      'value' => 'Reset',
    ),
  );

  // Submit elements.
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit_hidden'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#attributes' => array('style' => 'visibility:hidden;'),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => 'Cancel',
  );
  // Use an html tag instead of #type button because it is rendered by default
  // as an input type submit.
  $form['actions']['button_submit'] = array(
    '#type' => 'html_tag',
    '#tag' => 'button',
    '#attributes' => array(
      'type' => 'submit',
      'name' => 'op',
      'value' => 'Button submit',
    ),
    '#value' => 'Button submit',
    // Add default properties of a submit button to submit the form.
    // @see system_element_info().
    '#input' => TRUE,
    '#button_type' => 'submit',
    '#name' => 'op',
    '#executes_submit_callback' => TRUE,
    '#limit_validation_errors' => FALSE,
  );

  // Will always be triggered because this HTML tag doesn't return any value on
  // POST submission.
  // @see _form_button_was_clicked().
  if (variable_get('webdriver_test_form_submit_image', FALSE)) {
    $form['actions']['image'] = array(
      '#type' => 'image_button',
      '#name' => 'op',
      '#src' => 'misc/powered-blue-88x31.png',
      '#value' => 'Image',
    );
  }

  return $form;
}

/**
 * Testing autocomplete callback.
 */
function webdriver_test_autocomplete($string) {
  $matches = array(
    'foo (1)' => 'Foo',
    'bar (2)' => 'Bar',
    'baz (3)' => 'Baz',
  );
  drupal_json_output($matches);
}

/**
 * Testing form submit handler.
 */
function webdriver_test_form_submit(&$form, &$form_state) {
  if (!empty($_FILES['files']['name']['file'])) {
    $file = file_save_upload('file');
    $form_state['values']['files[file]'] = $file->fid;
  }

  if ($form_state['triggering_element']['#value'] === 'Submit') {
    foreach ($form_state['values'] as $name => $values) {
      variable_set('webdriver_test_form_value_' . $name, $values);
    }
    drupal_set_message('Data have been saved.');
  }
  elseif ($form_state['triggering_element']['#value'] === 'Cancel') {
    foreach (array_keys($form_state['values']) as $name) {
      variable_del('webdriver_test_form_value_' . $name);
    }
    variable_del('webdriver_test_form_value_files[file]');
    drupal_set_message('Data have been deleted.');
  }
  elseif ($form_state['triggering_element']['#value'] === 'Button submit') {
    drupal_set_message('Button submit submit the form.');
  }
  elseif ($form_state['triggering_element']['#value'] === 'Image') {
    drupal_set_message('Input type image always triggered to submit the form.');
  }
}

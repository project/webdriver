<?php

/**
 * Implements hook_form_FORM_ID_alter();
 */
function webdriver_form_simpletest_settings_form_alter(&$form, &$form_state) {

  $form['webdriver'] = array(
    '#type' => 'fieldset',
    '#title' => t('Webdriver'),
    '#collapsible' => TRUE,
  );

  $form['webdriver']['webdriver_library_path'] = array(
    '#type' => 'textfield',
    '#title' => t('php-webdriver library path'),
    '#description' => t('Path to the library directory where php-webdriver is. For example, !path.', array('!path' => WEBDRIVER_LIBRARY_DEFAULT_PATH)),
    '#default_value' => variable_get('webdriver_library_path', WEBDRIVER_LIBRARY_DEFAULT_PATH),
  );

  $form['webdriver']['webdriver_verbose_directory'] = array(
    '#type' => 'textfield',
    '#title' => t('Verbose directory'),
    '#description' => t('The directory where verbose messages (html and images screenshots) would be stored. Should be reachable by the webserver to be view with the simpletest GUI.'),
    '#default_value' => variable_get('webdriver_verbose_directory', 'public://simpletest/verbose'),
  );

  $form['webdriver']['webdriver_clear_verbose_results_on_success'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear verbose directories on success'),
    '#description' => t('Whether to clear verbose directories results for successfull class test methods executed only. If at least one failed/exception assert exists, verbose directories are keep. Be careful, verbose assertions would still exist but will point to non-existing files. Because most of the time you only care about failed verbose messages, you should check this option in order to save disk spaces with big test suites.'),
    '#default_value' => variable_get('webdriver_clear_verbose_results_on_success', FALSE),
  );

  $form['webdriver']['webdriver_servers']['#tree'] = TRUE;
  $form['webdriver']['webdriver_servers']['#element_validate'][] = 'webdriver_settings_form_clean_no_children_element_validate';

  $server_infos = webdriver_servers_info();
  $servers_settings = variable_get('webdriver_servers', array());

  foreach ($server_infos as $server_type => $server_info) {
    $server = new $server_info['class']();
    $server_settings = !empty($servers_settings[$server_type]) ? $servers_settings[$server_type] : array();

    $form['webdriver']['webdriver_servers'][$server_type] = array(
      '#type' => 'fieldset',
      '#title' => $server->getLabel(),
      '#collapsible' => TRUE,
      '#collapsed' => $servers_settings && empty($server_settings),
      '#element_validate' => array('webdriver_settings_form_server_element_validate'),
    );

    $form['webdriver']['webdriver_servers'][$server_type]['type'] = array(
      '#type' => 'value',
      '#value' => $server_type,
    );

    $form['webdriver']['webdriver_servers'][$server_type]['host'] = array(
      '#type' => 'textfield',
      '#title' => t('Webdriver server host (default or hub)'),
      '#description' => t('Format is HOST:PORT/PATH (example: localhost:4444/wd/hub).'),
      '#default_value' => !empty($server_settings['host']) ? $server_settings['host'] : '',
    );

    $wrapper_id = drupal_html_id('webdriver-browsers-wrapper');
    $form['webdriver']['webdriver_servers'][$server_type]['browsers']['#prefix'] = '<div id="' . $wrapper_id . '">';
    $form['webdriver']['webdriver_servers'][$server_type]['browsers']['#suffix'] = '</div>';
    $form['webdriver']['webdriver_servers'][$server_type]['browsers']['#theme'] = 'webdriver_browsers';
    $form['webdriver']['webdriver_servers'][$server_type]['browsers']['#element_validate'][] = 'webdriver_settings_form_clean_no_children_element_validate';

    $extra_description = $server->getCapabilitiesDescription();
    $form['webdriver']['webdriver_servers'][$server_type]['browsers']['#description'] = 'Each browsers declared should match desired capabilities supported by nodes (or default server). If some capabilities are omitted, the first match founded by the server would win.';
    $form['webdriver']['webdriver_servers'][$server_type]['browsers']['#description'] .= !empty($extra_description) ? ' ' . $extra_description : '';

    $delta = 0;
    $browsers_settings = !empty($form_state['storage']['webdriver_servers'][$server_type]['browsers'])
      ? $form_state['storage']['webdriver_servers'][$server_type]['browsers']
      : (!empty($server_settings['browsers']) ? $server_settings['browsers'] : array());
    $browsers_settings[] = array();

    foreach ($browsers_settings as $capabilities) {

      $form['webdriver']['webdriver_servers'][$server_type]['browsers'][$delta]['enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enabled?'),
        '#default_value' => isset($capabilities['enabled']) ? (bool)$capabilities['enabled'] : FALSE,
      );

      $element = $server->getCapabilitiesElementForm($capabilities);
      $element['#server_class'] = $server_info['class'];
      $element['#element_validate'][] = 'webdriver_settings_form_capabilities_element_validate';

      $header = array(t('Enabled'));
      foreach (element_children($element) as $child_key) {
        if (isset($element[$child_key]['#title'])) {
          $header[] = $element[$child_key]['#title'];
        }
      }
      $form['webdriver']['webdriver_servers'][$server_type]['browsers']['#header'] = $header;
      $form['webdriver']['webdriver_servers'][$server_type]['browsers'][$delta] += $element;
      $delta++;
    }

    $form['webdriver']['webdriver_servers'][$server_type]['browsers']['add_more'] = array(
      '#type' => 'submit',
      '#name' => $server_type . '_add_more',
      '#value' => t('Add more'),
      '#limit_validation_errors' => array(array('webdriver_servers', $server_type, 'browsers')),
      '#submit' => array('webdriver_settings_form_ajax_submit'),
      '#ajax' => array(
        'wrapper' => $wrapper_id,
        'callback' => 'webdriver_settings_form_ajax_callback',
        'effect' => 'fade',
      ),
    );
  }

  return system_settings_form($form);
}

/**
 * Capabilities element validate callback.
 */
function webdriver_settings_form_capabilities_element_validate($element, &$form_state, $form) {
  $values = drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  $server = new $element['#server_class']();

  if ($server->capabilitiesElementIsEmpty($element)) {
    $values = NULL;
  }
  else {
    $server->capabilitiesElementValuesAlter($element, $values);
  }
  form_set_value($element, $values, $form_state);
}

/**
 * Server form element validate callback.
 */
function webdriver_settings_form_server_element_validate($element, &$form_state, $form) {
  $values = drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  if (empty($element['host']['#value'])) {
    if (!empty($values['browsers'])) {
      form_error($element['host'], t('Host field is required to register browsers.'));
    }
    else {
      $values = array();
    }
  }
  form_set_value($element, $values, $form_state);
}

/**
 * Elements validate callback to clear values if children does not exists.
 */
function webdriver_settings_form_clean_no_children_element_validate($element, &$form_state, $form) {
  $values = drupal_array_get_nested_value($form_state['values'], $element['#parents']);
  foreach (element_children($element) as $delta) {
    if ($delta === 'add_more' || empty($values[$delta])) {
      unset($values[$delta]);
    }
  }
  form_set_value($element, $values, $form_state);
}

/**
 * Submit callback for "add more" action.
 */
function webdriver_settings_form_ajax_submit($form, &$form_state) {
  $form_state['storage']['webdriver_servers'] = $form_state['values']['webdriver_servers'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Ajax callback to render the browsers element.
 */
function webdriver_settings_form_ajax_callback($form, &$form_state) {
  $parents = $form_state['triggering_element']['#array_parents'];
  array_pop($parents);
  return drupal_array_get_nested_value($form, $parents);
}

/**
 * Theme callback.
 */
function theme_webdriver_browsers($variables) {
  $element = $variables['element'];
  $output = '';

  $rows = array();
  foreach (element_children($element) as $delta) {
    $row = array();
    foreach (element_children($element[$delta]) as $key) {
      if ($key !== 'add_more' && $element[$delta][$key]['#type'] !== 'value') {
        $element[$delta][$key]['#title_display'] = 'invisible';
        $row[] = drupal_render($element[$delta][$key]);
      }
    }
    $rows[] = $row;
  }

  $output .= theme('table', array(
    'header' => $element['#header'],
    'caption' => t('Browsers availables on nodes or default server with their capabilities'),
    'rows' => $rows,
  ));
  $output .= drupal_render($element['add_more']);
  $output .= '<div><p>' . $element['#description'] . '</p></div>';
  return $output;
}

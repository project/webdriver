<?php

class MobileDriverCommand extends DriverCommand {

  const CONTEXTS = 'getContexts';
  const GET_CURRENT_CONTEXT = 'getCurrentContext';
  const SWITCH_TO_CONTEXT = 'switchToContext';

  // Appium
  const HIDE_KEYBOARD = 'hideKeyboard';
  const KEY_EVENT = 'keyEvent'; // Needed for Selendroid
  const PRESS_KEYCODE = 'pressKeyCode';
}

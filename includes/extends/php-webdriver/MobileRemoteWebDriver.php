<?php

class MobileRemoteWebDriver extends RemoteWebDriver {

  /**
   * Override parent to set a mobile HTTP command executor.
   */
  public static function create(
    $url = 'http://localhost:4444/wd/hub',
    $desired_capabilities = null,
    $connection_timeout_in_ms = 300000
  ) {

    $driver = parent::create($url, $desired_capabilities, $connection_timeout_in_ms);

    $executor = new MobileHttpCommandExecutor($url);
    $executor->setConnectionTimeout($connection_timeout_in_ms);
    $driver->setCommandExecutor($executor);

    return $driver;
  }

  /**
   * Override parent to set a mobile HTTP command executor.
   */
  public static function createBySessionID(
    $session_id,
    $url = 'http://localhost:4444/wd/hub'
  ) {

    $driver = parent::createBySessionID($session_id, $url);
    $driver->setCommandExecutor(new MobileHttpCommandExecutor($url));
    return $driver;
  }

  /**
   * Returns all available contexts.
   *
   * @return array
   *   An array of available context names.
   */
  public function getContexts() {
    return $this->execute(MobileDriverCommand::CONTEXTS);
  }

  /**
   * Returns current context.
   *
   * @return string
   *   The current context name.
   */
  public function getCurrentContext() {
    return $this->execute(MobileDriverCommand::GET_CURRENT_CONTEXT);
  }

  /**
   * Switch to a given context.
   *
   * @param string $context
   *   The context to switch : 'NATIVE_APP', 'WEBVIEW_1', etc.
   */
  public function switchToContext($context) {
    $this->execute(MobileDriverCommand::SWITCH_TO_CONTEXT, array(
      'name' => $context,
    ));
  }

  /**
   * Hides the software keyboard on the device. In iOS, use `key_name` to press
   * a particular key, or `strategy`. In Android, no parameters are used.
   *
   * @param string $keyName
   *   key to press
   * @param string $key
   * @param string $strategy
   *   strategy for closing the keyboard (e.g., `tapOutside`)
   *
   * @return \MobileRemoteWebDriver
   */
  public function hideKeyboard($keyName = null, $key = null, $strategy = null) {
    $params = array();

    if ($keyName) {
      $params['keyName'] = $keyName;
    } elseif ($key) {
      $params['key'] = $key;
    }

    if ($strategy) {
      $params['strategy'] = $strategy;
    }

    $this->execute(MobileDriverCommand::HIDE_KEYBOARD, $params);
    return $this;
  }

  /**
   * Sends a keycode to the device. Android only. Possible keycodes can be found
   * in http://developer.android.com/reference/android/view/KeyEvent.html.
   * Needed for Selendroid.
   *
   * @param type $keyCode
   *   the keycode to be sent to the device
   * @param int $metaState
   *   meta information about the keycode being sent
   *
   * @return \MobileRemoteWebDriver
   */
  public function keyEvent($keyCode, $metaState = null) {
    $params = array(
      'keycode' => $keyCode,
    );
    if ($metaState !== null) {
      $params['metastate'] = $metaState;
    }

    $this->execute(MobileDriverCommand::KEY_EVENT, $params);
    return $this;
  }

  /**
   * Sends a keycode to the device. Android only. Possible keycodes can be found
   * in http://developer.android.com/reference/android/view/KeyEvent.html.
   *
   * @param int $keyCode
   *   the keycode to be sent to the device
   * @param int $metaState
   *   meta information about the keycode being sent
   *
   * @return \MobileRemoteWebDriver
   */
  public function pressKeyCode($keyCode, $metaState = null) {
    $params = array(
      'keycode' => $keyCode,
    );
    if ($metaState !== null) {
      $params['metastate'] = $metaState;
    }

    $this->execute(MobileDriverCommand::PRESS_KEYCODE, $params);
    return $this;
  }
}

<?php

class MobileHttpCommandExecutor extends HttpCommandExecutor {

  protected static $mobileCommands = array(
    MobileDriverCommand::CONTEXTS =>                     array('method' => 'GET', 'url' => '/session/:sessionId/contexts'),
    MobileDriverCommand::GET_CURRENT_CONTEXT =>          array('method' => 'GET', 'url' => '/session/:sessionId/context'),
    MobileDriverCommand::SWITCH_TO_CONTEXT =>            array('method' => 'POST', 'url' => '/session/:sessionId/context'),
    // Appium
    MobileDriverCommand::HIDE_KEYBOARD =>                array('method' => 'POST', 'url' => '/session/:sessionId/appium/device/hide_keyboard'),
    MobileDriverCommand::KEY_EVENT =>                    array('method' => 'POST', 'url' => '/session/:sessionId/appium/device/keyevent'),
    MobileDriverCommand::PRESS_KEYCODE =>                array('method' => 'POST', 'url' => '/session/:sessionId/appium/device/press_keycode'),
  );

  public function __construct($url) {
    self::$commands += self::$mobileCommands;
    parent::__construct($url);
  }
}

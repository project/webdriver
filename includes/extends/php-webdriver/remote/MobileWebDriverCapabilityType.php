<?php

class MobileWebDriverCapabilityType extends WebDriverCapabilityType {

  const AUTOMATION_NAME = "automationName";

  const PLATFORM_NAME = "platformName";
  const PLATFORM_VERSION = "platformVersion";

  const DEVICE_NAME = "deviceName";

  const NEW_COMMAND_TIMEOUT = "newCommandTimeout";
  const DEVICE_READY_TIMEOUT = "deviceReadyTimeout";
  const LAUNCH_TIMEOUT = "launchTimeout";

  const APP = "app";
  const APP_PACKAGE = "appPackage";
  const APP_ACTIVITY = "appActivity";
  const APP_WAIT_ACTIVITY = "appWaitActivity";
  const APP_WAIT_PACKAGE = "appWaitPackage";
  const SELENDROID_PORT  = "selendroidPort";
}

<?php

class MobileWebDriverBrowserType {

  const SAFARI = "Safari";
	const BROWSER = "Browser";
	const CHROMIUM = "Chromium";
	const CHROME = "Chrome";
}

<?php

class MobileWebDriverPlatform {

  const ANDROID = 'Android';
  const IOS = 'iOS';
  const FIREFOX_OS = 'FirefoxOS';
}

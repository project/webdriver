<?php

/**
 * Dangerous class which allow you to execute your test on live site !
 * You may need it if :
 * - you don't use/maintains your profile. Shame on you !
 * - sadly, you can't deploy it because architecture is too much complex
 * - you only do "safe" operations (not writting data, sending mail, etc) and
 *   want to boots your tests by skipping set up.
 * - you want to test external website and can not use the php-webdriver library
 *   on your own.
 *
 * WARNING : Any writting records (including public/temporary/private files,
 * cache backends, search engines), fatal errors, mails send, etc would really
 * occured againsts the live site ! Unless you really know what you are doing,
 * you are strongely encourage to don't use this class. This is on your own
 * risks !
 *
 * Also note that executing parallel tests may not working because you are
 * testing the same backend and conflicts could happens.
 *
 * Because tests executions could not be safe (i.e : fatal errors), we can't
 * switch config during tests. So live site config is not altered.
 *
 * As a consequence, here is a summary list of stuff which are removed :
 * - not swithing on testing mail system, mails would be delivered !
 * - not catching fatal error, logged by live site !
 * - not changing site language (i.e : english not forced)
 * - not changing files directories : public, private and temporary. Any
 *   operations inside these directories would really happens !
 *
 * Most of the time, you would override the self::tearDown() to clear your live
 * environment site pollute by your tests. However, keep in mind that this
 * method may not be necessary called, so you should not rely on it ! For
 * example, a fatal error in the test suite would break execution and thus don't
 * call tear down method.
 */
abstract class DrupalWebDriverLiveTestCase extends DrupalWebDriverTestCase {

  /**
   * Override parent to keep the minimum.
   */
  protected function setUp(array $module_list = array()) {

    // Init test ID + DB prefix for assertions.
    $this->prepareDatabasePrefix();

    // Prepare only verbose file directories.
    $this->prepareVerboseDirectory();

    // Reset all statics so that test is performed with a clean environment.
    drupal_static_reset();

    drupal_set_time_limit($this->timeLimit);
    $this->setup = TRUE;

    // Call parent to execute post operations.
    $this->postSetUp();
  }

  /**
   * Override parent to remove DrupalWebTestCase::tearDown() not needed.
   */
  protected function tearDown() {

    $this->preTearDown();

    // Ensure that internal logged in variable is reset.
    $this->loggedInUser = FALSE;

    $this->postTearDown();
  }

  /**
   * Override parent to remove user agent testing.
   */
  protected function initDriver() {

    switch ($this->browser) {

      case 'firefox' :
        if (!isset($this->desiredCapabilities[FirefoxDriver::PROFILE])) {
          $this->desiredCapabilities[FirefoxDriver::PROFILE] = new FirefoxProfile();
        }
        break;

      case 'chrome' :
        if (!isset($this->desiredCapabilities[ChromeOptions::CAPABILITY])) {
          $this->desiredCapabilities[ChromeOptions::CAPABILITY] = new ChromeOptions();
        }
        break;

      case 'phantomjs' :
        $this->desiredCapabilities['phantomjs.page.customHeaders.User-Agent'] = '';
        break;
    }

    parent::initDriver();

    // No need to inject cookie for user-agent.
    $this->userAgentNeedCookie = FALSE;
  }

  /**
   * Nothing to do because the live log system is used.
   */
  protected function assertTestingSiteErrors() {}

  /**
   * Nothing to do because the live error log file is used.
   */
  protected function assertTestingSiteFatalErrors() {}
}

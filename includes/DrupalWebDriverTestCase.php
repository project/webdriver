<?php

abstract class DrupalWebDriverTestCase extends DrupalWebTestCase {

  /**
   * An array of all available servers config keyed by server type containing an
   * array with keys :
   * - type : the type of the server
   * - host : in the form HOSTNAME:PORT/PATH
   * - browsers : an array of supported browsers. Each entry is an array of
   *   required/desired capabilities, depending on server type.
   */
  protected $servers = array();

  /**
   * The current server type config array running.
   */
  protected $server = array();

  /**
   * A RemoteWebDriver object instance.
   */
  protected $driver = NULL;

  /**
   * Name of the current browser executed.
   */
  protected $browser = NULL;

  /**
   * Connection timeout in ms for the driver session.
   */
  protected $timeoutConnection = 300000;

  /**
   * Desired capabilities to execute tests againsts.
   * Must be override in the initDriver() method.
   *
   * For example, to add proxy settings, you could do :
   * @code
   * protected function initDriver() {
   *   if ($this->browser === 'firefox') {
   *     $this->desiredCapabilities[WebDriverCapabilityType::PROXY] = array(
   *       'proxyType' => 'manual',
   *       'httpProxy' => '127.0.0.1:2043',
   *       'sslProxy' => '127.0.0.1:2043',
   *     );
   *   }
   *   parent::initDriver();
   * }
   * @endcode
   *
   * @see https://code.google.com/p/selenium/wiki/DesiredCapabilities
   */
  protected $desiredCapabilities = array();

  /**
   * Store default desired capabilities for assert messages. It would only store
   * capabilities provided in self::run() method or by config. It don't include
   * capabilities added by classes.
   */
  protected $defaultDesiredCapabilities = array();

  /**
   * Absolute path to the chrome binary. Set it if system could not get it.
   */
  protected $chromeBinaryPath = '';

  /**
   * Properties availables to execute a local Chrome Driver instead of using
   * proxies like Selenium or Appium.
   * Because it would be executed by our web server user (apache, nginx,
   * etc), it must be executable by it (ChromeDriver, Chrome and more).
   * Furthermore, the server type host should be updated to the ChromeDriver.
   * Note that --url-base argument could not be used due to the DriverService
   * class.
   * Also note that proxies provide some work around so its usage without them
   * is not recommanded.
   */
  protected $chromeDriverUse = FALSE;
  protected $chromeDriverBinaryPath = '';
  protected $chromeDriverPort = 9515;
  protected $chromeDriverArgs = array();
  protected $chromeDriverLogFile = '';
  protected $chromeDriverVerbose = FALSE;
  protected $chromeDriverSilent = FALSE;

  /**
   * Additionnal Chrome Driver settings required for mobile.
   * Only Android is supported for the moment so you should set the android
   * device serial manually (i.e, result of : adb devices).
   * Usually, adb port is 5037.
   */
  protected $chromeDriverAdbPort = NULL;
  protected $chromeDriverDeviceSerial = '';

  /**
   * Store cookie domain to use. Be careful, WebDriver API expect it to be
   * exactly the same as the current browser domain requested. In other words,
   * if testing site domain is 'foo.com' and cookie domain is '.foo.com', it
   * won't work because it should be 'foo.com'.
   */
  protected $cookieDomain = '';

  /**
   * Internal flag whether cookie domain have been initialized or not.
   */
  protected $cookieDomainInit = FALSE;

  /**
   * Contains the user agent string generated.
   */
  protected $userAgent = NULL;

  /**
   * Boolean indicating whether the browser doesn't support HTTP user agent
   * header override and thus need a core patch to use cookie instead.
   * For instance, HTTP headers override is supported by :
   * - firefox
   * - chrome
   * - phantomJS
   * Not supported by :
   * - IE (custom profile only on same host)
   * - Opera (only custom profile on same host with Custom User-agent setting)
   * - Safari
   */
  protected $userAgentNeedCookie = FALSE;

  /**
   * Default cookie testing UA for purpose. May be override if needed.
   * Needs core patch and used only for browsers which doesn't support HTTP user
   * agent override.
   */
  protected $userAgentCookie = array(
    'name' => 'test_ua',
    'value' => '',
    'path' => '/',
    'domain' => NULL,
    'secure' => FALSE,
    'expiry' => NULL,
  );

  /**
   * Amount of time in seconds the driver should wait when searching for
   * elements. If you encounter some troubles with unexpected DOM loading, you
   * may increase this value. However, it is strongely advice to let's it to 0
   * and use wait events instead (WebDriverWait). Otherwise, test performance
   * will suffer unnecessary for other use cases.
   */
  protected $implicitWait = 0;

  /**
   * Set the amount of time in seconds to wait for a page load to complete
   * before throwing an error. By default, this is never (wait indefinitely).
   */
  protected $pageLoadTimeout = 0;

  /**
   * Set the amount of time, in seconds, that asynchronous scripts executed
   * are permitted to run before they are aborted and a Timeout error is
   * returned to the client.
   * By default, this value is often not enough (~3ms). So we increase it.
   */
  protected $scriptTimeout = 5;

  /**
   * Whether screenshot support is disabled or not.
   */
  protected $screenshotDisabled = FALSE;

  /**
   * By default, disable vertical tabs in form. Set it to FALSE if you want to
   * test vertical tabs events.
   */
  protected $disableVerticalTabs = TRUE;

  /**
   * Optionally provide an absolute url path to initialize the HTTP
   * authentification.
   */
  protected $httpAuthInitUrl = '';

  /**
   * Verbose variables used to override default simpletest behavior.
   */
  protected $isVerbose = TRUE;
  protected $verboseID = 1;
  protected $verboseRootDirectory = '';
  protected $verboseDirectory = '';
  protected $verboseClearOnSuccess = FALSE;

  /**
   * Whether HTTP status code should be checked with JS or using cURL library.
   * Default to JS (ajax request) to use remote browser with its config
   * (cookies, proxy, ...).
   */
  protected $checkHttpStatusCodeWithJS = TRUE;

  /**
   * Enable XDebug session on remote client browser.
   * Default values are for purpose only and you may alter it to fit your needs
   * in children classes or with Drupal setting "webdriver_xdebug_cookie".
   */
  protected $xDebug = FALSE;
  protected $xDebugCookie = array(
    'name' => 'XDEBUG_SESSION',
    'value' => 'netbeans-xdebug',
    'path' => '/',
    'domain' => NULL,
    'secure' => FALSE,
    'expiry' => NULL,
  );

  /**
   * Ugly internal boolean flag for method self::getLengthCheckingWait() which
   * may reside into an another class instead.
   */
  protected $resetPageLoadCheckLength = TRUE;

  /**
   * Override parent to run all tests in this class againsts further
   * capabilities.
   *
   * @param string $requested_server_type
   *   A server type to execute tests againsts. Prevent conflict in case of same
   *   browsers name exist between further servers.
   * @params array $requested_browsers
   *   An array of browsers name to execute tests againsts.
   */
  public function run(array $methods = array(), $requested_server_type = '', array $requested_browsers = array()) {
    $class = get_class($this);

    $this->preRun();

    // Get and filters class methods to execute.
    $class_methods = get_class_methods($class);
    if ($methods) {
      $class_methods = array_intersect($class_methods, $methods);
    }

    // Reduce the list of server types to execute.
    $servers = $requested_server_type ? array_intersect_key($this->servers, array_flip(array($requested_server_type))) : $this->servers;

    // Iterate over each servers types to get their supported browsers.
    foreach ($servers as $server_settings) {

      $this->server = $server_settings;
      $browsers = $this->getAvailableBrowsers($requested_browsers);

      // Iterate over each browsers to execute test classes.
      foreach ($browsers as $capabilities) {

        // Cleanup desired capabilities.
        $this->browser = $capabilities['browserName'];
        unset($capabilities['browserName']);
        unset($capabilities['enabled']);

        // Get unsupported methods depending on current capabilities.
        $methods_unsupported = $this->unsupportedTestMethods($capabilities);

        // Remove methods not supported for this browser.
        $methods_supported = array_diff($class_methods, $methods_unsupported);
        foreach ($methods_supported as $method) {

          if (strtolower(substr($method, 0, 4)) === 'test') {

            // Reset capabilities for each test setup.
            $this->desiredCapabilities = $this->defaultDesiredCapabilities = $capabilities;

            $method_info = new ReflectionMethod($class, $method);
            $caller = array(
              'file' => $method_info->getFileName(),
              'line' => $method_info->getStartLine(),
              'function' => $class . '->' . $method . '()',
            );
            $completion_check_id = self::insertAssert($this->testId, $class, FALSE, t('The test did not complete due to a fatal error.'), 'Completion check', $caller, $this->browser, $this->defaultDesiredCapabilities, $this->server['type']);

            try {
              $this->setUp();
            }
            catch (Exception $e) {
              $this->exceptionHandler($e);
              $this->setup = FALSE;
            }

            if ($this->setup) {
              try {
                $this->$method();
              }
              catch (Exception $e) {
                $this->exceptionHandler($e);
              }
              $this->tearDown();
            }
            else {
              $this->fail(t("The test cannot be executed because it has not been set up properly."));
              $this->clearSetUp();
            }

            self::deleteAssert($completion_check_id);
          }
        }
      }
    }

    if (!$servers) {
      $this->fail('No server config founded.');
    }
    elseif (!$this->browser) {
      $this->fail('No enabled browsers config founded.');
    }

    $this->postRun();
  }

  /**
   * Method helper to execute previous operations before test suite is running.
   */
  protected function preRun() {

    // Set error handler first.
    set_error_handler(array($this, 'errorHandler'));

    // Verbose behaviour review.
    $this->isVerbose = variable_get('simpletest_verbose', TRUE);
    $writtable = $this->prepareVerboseRootDirectory(variable_get('webdriver_verbose_directory', 'public://simpletest/verbose'));
    if (!$writtable) {
      self::insertAssert($this->testId, get_class($this), FALSE, t('The test did not complete because verbose root directory could not be created.'), 'Completion check');
      return;
    }

    // Keep HTTP authentification credential even if its usage is very limited.
    $this->httpauth_method = variable_get('simpletest_httpauth_method', CURLAUTH_BASIC);
    $username = variable_get('simpletest_httpauth_username', NULL);
    $password = variable_get('simpletest_httpauth_password', NULL);
    if ($username && $password) {
      $this->httpauth_credentials = $username . ':' . $password;
    }

    // Include PHP-webdriver.
    $library_path = variable_get('webdriver_library_path', WEBDRIVER_LIBRARY_DEFAULT_PATH) . '/php-webdriver';
    require_once(drupal_realpath($library_path) . '/lib/__init__.php');

    // Get config before it is cleared by setUp (loosing settings.php config).
    $this->servers = variable_get('webdriver_servers', array());
    $this->verboseClearOnSuccess = variable_get('webdriver_clear_verbose_results_on_success', FALSE);
    $this->xDebug = variable_get('webdriver_xdebug_enabled', $this->xDebug);
    $this->xDebugCookie = variable_get('webdriver_xdebug_cookie', $this->xDebugCookie);
  }

  /**
   * Method helper to execute post operations after test suite has been running.
   */
  protected function postRun() {
    drupal_get_messages();
    restore_error_handler();
  }

  /**
   * Method helper to get available browsers config of the current server type.
   *
   * @param array $requested_browsers
   *   An array of browser config to used instead and check if enabled.
   *
   * @return
   *   A list of browsers config which could be executed for the given server
   *   type.
   *
   * @see self::run().
   */
  protected function getAvailableBrowsers(array $requested_browsers = array()) {
    $browsers = array();

    // Get enabled browsers of the server.
    $server_browsers = array_filter($this->server['browsers'], function($capabilities) {
      return isset($capabilities['enabled']) ? $capabilities['enabled'] : TRUE;
    });
    $browsers = $server_browsers;

    // Exclude provided browsers which are not supported by this server type.
    if ($requested_browsers && !empty($server_browsers)) {
      $server_info = webdriver_servers_info($this->server['type']);
      $server = new $server_info['class']();
      $browsers = $server->getCapabilitiesAvailables($requested_browsers, $server_browsers);
    }

    return $browsers;
  }

  /**
   * Let's children classes provide a list of test methods which are not
   * supported for the requested capabilities. It prevent useless set up for
   * these methods.
   *
   * @param array $capabilities
   *   An array of capabilities to execute tests againsts. 'browserName'
   *   property is accessible with self::$browser and is removed from this
   *   array.
   *
   * @return array
   *   An array of test methods which should not be executed.
   */
  protected function unsupportedTestMethods(array $capabilities) {
    return array();
  }

  /**
   * Prepare verbose root directory for every test classes run in the suite.
   * Must be called before switching on testing environnement. Otherwise, scheme
   * stream config would be reset.
   *
   * @param string $directory
   *   The verbose directory to create.
   *
   * @see simpletest_verbose().
   */
  protected function prepareVerboseRootDirectory($directory) {

    if (!$this->isVerbose) {
      return TRUE;
    }

    // Because testing environnement would change scheme config, we can not
    // use stream wrapper during test. If parent directories don't exist,
    // drupal_realpath() would return FALSE instead of the absolute path it
    // supposed to be.
    $wrapper = file_stream_wrapper_get_instance_by_uri($directory);
    if ($wrapper) {
      $directory = $wrapper->getDirectoryPath() . '/' . file_uri_target($directory);
    }

    $this->verboseRootDirectory = $directory;
    $writable = file_prepare_directory($this->verboseRootDirectory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
    if (!$writable) {
      return FALSE;
    }
    elseif (!file_exists($this->verboseRootDirectory . '/.htaccess')) {
      return (bool) file_put_contents($this->verboseRootDirectory . '/.htaccess', "<IfModule mod_expires.c>\nExpiresActive Off\n</IfModule>\n");
    }
    return TRUE;
  }

  /**
   * Override parent to also prepare verbose directory of current test class
   * method executed.
   */
  protected function prepareEnvironment() {
    parent::prepareEnvironment();
    if ($this->setupEnvironment) {
      $this->setupEnvironment = $this->prepareVerboseDirectory();
    }
  }

  /**
   * Prepare verbose directory for the current test class method executed.
   */
  protected function prepareVerboseDirectory() {

    if (!$this->isVerbose) {
      return TRUE;
    }

    // Unfortunetly, DB prefix which is a unique identifier is not store in DB
    // (only last).
    // Use sha256 hash algo for filename.
    $browser_key = hash('sha256', $this->browser . serialize($this->defaultDesiredCapabilities) . $this->server['type']);
    $class_safe = str_replace('\\', '_', get_class($this));
    $verbose_directory = "$this->verboseRootDirectory/$this->testId/$browser_key/$class_safe";
    $writable = FALSE;

    $directories = array(
      $verbose_directory . '/html',
      $verbose_directory . '/screenshots',
    );
    foreach ($directories as $directory) {
      $writable = file_prepare_directory($directory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);
      if (!$writable) {
        return FALSE;
      }
    }

    $this->verboseDirectory = $verbose_directory;
    return TRUE;
  }

  /**
   * Override parent::setUp().
   *
   * Only support module list as one parameter.
   */
  protected function setUp(array $module_list = array()) {

    parent::setUp($module_list);
    if (!$this->setup) {
      return FALSE;
    }

    $this->postSetUp();
  }

  /**
   * Post set-up to init driver.
   */
  protected function postSetUp() {

    // First of all, reset internal flags.
    $this->resetVariables();

    // Init a new driver session with clean data.
    try {
      $this->initDriver();
    }
    catch (Exception $e) {
      $this->fail(sprintf('Unable to init browser %s with exception : %s', $this->browser, $e->getMessage()));
      $this->setup = FALSE;
      return;
    }

    // If you encounter a lot of wait bug (AJAX, dropdown and so on), increase
    // this value. Not advice.
    if ($this->implicitWait > 0) {
      $this->driver->manage()->timeouts()->implicitlyWait($this->implicitWait);
    }

    // If for particular reason there is pages which take too much time,
    // you could tell server to throw an exception because default config would
    // wait indefinitely.
    if ($this->pageLoadTimeout > 0) {
      // Some browser like Safari doesn't support this setting.
      try {
        $this->driver->manage()->timeouts()->pageLoadTimeout($this->pageLoadTimeout);
      }
      catch (WebDriverException $e) {}
    }

    // By default, we increase this value which is not enough.
    if ($this->scriptTimeout > 0) {
      $this->driver->manage()->timeouts()->setScriptTimeout($this->scriptTimeout);
    }

    // Better visibility.
    try {
      $this->driver->manage()->window()->maximize();
    }
    // However, some browsers don't support it or have some weird bug. No matter.
    catch (WebDriverException $e) {}

    // Init HTTP authentification.
    if ($this->httpauth_credentials) {
      $this->initHttpAuth();
    }

    // Send cookie testing UA for browser which doesn't support HTTP user agent
    // override. Need core patch to work.
    if ($this->userAgentNeedCookie) {
      $this->sendCookieTestUA();
    }

    // Enable debugging.
    if ($this->xDebug) {
      $this->startXdebugSession();
    }
  }

  /**
   * Fix a lack of clean up after a setup has failed.
   */
  protected function clearSetUp() {

    // Restore database connection if it was changed.
    if (Database::getConnectionInfo('simpletest_original_default')) {
      Database::removeConnection('default');
      Database::renameConnection('simpletest_original_default', 'default');
    }
  }

  /**
   * Reset required internal flag for every setup.
   * Instead of overriding self::resetAll() which may be called in various use
   * cases, use a specific method.
   *
   * Not all variables are cleared because it depends on use case. For example,
   * following variables are not required to be cleared :
   * - implicitWait
   * - pageLoadTimeout
   * - scriptTimeout
   * - cookieDomain
   * and children may override this method to reset them.
   */
  protected function resetVariables() {

    // Internal flags.
    $this->assertions = array();
    $this->cookieDomainInit = FALSE;
    $this->url = NULL;
    $this->verboseID = 1;
    // Browser config flag.
    $this->userAgentNeedCookie = FALSE;
    $this->screenshotDisabled = FALSE;
    $this->checkHttpStatusCodeWithJS = TRUE;
  }

  /**
   * Method helper to initialize a driver with desired capabilities.
   */
  protected function initDriver() {

    // Set user-agent to switch on database test at bootstrap.
    if (!preg_match('/simpletest\d+/', $this->databasePrefix, $matches)) {
      throw new Exception('Test ID needed for user agent could not be retrieved.');
    }
    $this->userAgent = drupal_generate_test_ua($matches[0]);

    // Create browser session with desired capabilities.
    $caps = NULL;
    $desired_capabilities = array();
    switch ($this->browser) {

      case 'firefox' :
        $caps = DesiredCapabilities::firefox();
        $profile = new FirefoxProfile();
        $profile->setPreference('general.useragent.override', $this->userAgent);
        $desired_capabilities[FirefoxDriver::PROFILE] = $profile;
        // Force enabling native events for Linux by default to use advanced
        // user actions, even if it could break windows manager with commands
        // like switch window. Already enabled by default for Windows.
        $desired_capabilities['nativeEvents'] = TRUE;
        break;

      case 'chrome' :
        $caps = DesiredCapabilities::chrome();
        $options = new ChromeOptions();
        $options->addArguments(array("--user-agent=$this->userAgent"));
        if ($this->chromeBinaryPath) {
          $options->setBinary($this->chromeBinaryPath);
        }
        $desired_capabilities[ChromeOptions::CAPABILITY] = $options;
        break;

      case 'internet_explorer' :
        $caps = DesiredCapabilities::internetExplorer();
        $desired_capabilities['ie.validateCookieDocumentType'] = FALSE;
        $desired_capabilities['ie.ensureCleanSession'] = TRUE;
        $this->userAgentNeedCookie = TRUE;
        break;

      case 'htmlunit' :
        $caps = DesiredCapabilities::htmlUnitWithJS();
        $this->userAgentNeedCookie = TRUE;
        // Not supported because it have no UI.
        $this->screenshotDisabled = TRUE;
        break;

      case 'phantomjs' :
        $caps = DesiredCapabilities::phantomjs();
        // Using phantomjs.page.settings.userAgent setting breaks some stuff.
        // @see https://github.com/detro/ghostdriver/issues/342
        $desired_capabilities['phantomjs.page.customHeaders.User-Agent'] = $this->userAgent;
        break;

      case 'safari' :
        $caps = DesiredCapabilities::safari();
        $desired_capabilities['safari.options.cleanSession'] = TRUE;
        $this->userAgentNeedCookie = TRUE;
        break;

      case 'opera' :
        $caps = DesiredCapabilities::opera();
        $this->userAgentNeedCookie = TRUE;
        // Driver don't return any JS result so use cURL instead.
        $this->checkHttpStatusCodeWithJS = FALSE;
        break;

      case 'selendroid' :
        $caps = DesiredCapabilities::android();
        $desired_capabilities[WebDriverCapabilityType::ROTATABLE] = TRUE;
        if (isset($this->desiredCapabilities['emulator'])) {
          $this->desiredCapabilities['emulator'] = (bool)$this->desiredCapabilities['emulator'];
        }
        $this->userAgentNeedCookie = TRUE;
        break;

      case 'appium-chrome' :
      case 'appium-chromebeta' :
      case 'appium-chromium' :
        $caps = new DesiredCapabilities(array(
          WebDriverCapabilityType::BROWSER_NAME => str_replace('appium-', '', $this->browser),
          WebDriverCapabilityType::ROTATABLE => TRUE,
        ));

        // Set Android platform for Selenium Grid only to prevent conflicts.
        $is_android = isset($this->desiredCapabilities[MobileWebDriverCapabilityType::PLATFORM_NAME])
          && $this->desiredCapabilities[MobileWebDriverCapabilityType::PLATFORM_NAME] === MobileWebDriverPlatform::ANDROID;
        if ($is_android) {
          $caps->setPlatform(WebDriverPlatform::ANDROID);
        }

        // Use an another chromedriver than default one embed with Appium?
        if (!$this->chromeDriverUse && $this->chromeDriverBinaryPath) {
          $caps->setCapability('chromedriverExecutable', $this->chromeDriverBinaryPath);
        }

        $options = new ChromeOptions();
        $options->addArguments(array("--user-agent=$this->userAgent"));
        $options->addArguments(array('--disable-translate'));

        // Use ChromeDriver without Appium proxy?
        if ($this->chromeDriverUse && $this->chromeDriverDeviceSerial) {
          if (!$is_android) {
            throw new Exception('Using chromedriver without Appium is only supported with Android platform for now.');
          }
          if ($this->browser === 'appium-chromium') {
            $package = 'org.chromium.chrome.shell';
          }
          elseif ($this->browser === 'appium-chromebeta') {
            $package = 'com.chrome.beta';
          }
          else {
            $package = 'com.android.chrome';
          }
          $options->setExperimentalOption('androidPackage', $package);
          $options->setExperimentalOption('androidDeviceSerial', $this->chromeDriverDeviceSerial);
        }

        $desired_capabilities[ChromeOptions::CAPABILITY] = $options;
        break;

      case 'appium-browser' :
        $caps = new DesiredCapabilities(array(
          WebDriverCapabilityType::BROWSER_NAME => 'browser',
          WebDriverCapabilityType::ROTATABLE => TRUE,
          // Set Android platform for Selenium Grid only to prevent conflicts.
          WebDriverCapabilityType::PLATFORM => WebDriverPlatform::ANDROID,
        ));

        if ($this->chromeDriverUse && $this->chromeDriverDeviceSerial) {
          $options = new ChromeOptions();
          $options->setExperimentalOption('androidPackage', 'com.android.browser');
          $options->setExperimentalOption('androidActivity', 'com.android.browser.BrowserActivity');
          $options->setExperimentalOption('androidDeviceSerial', $this->chromeDriverDeviceSerial);
          $desired_capabilities[ChromeOptions::CAPABILITY] = $options;
        }

        $this->userAgentNeedCookie = TRUE;
        break;

      default :
        throw new Exception(t('Unsupported browser @browser.'), array('@browser' => $this->browser));
    }

    // Merge capabilities with defaults.
    $this->desiredCapabilities += $desired_capabilities;
    foreach ($this->desiredCapabilities as $name => $value) {
      // Override version and platform capabilities if provided.
      if ($name === WebDriverCapabilityType::VERSION) {
        $caps->setVersion((float)$value);
      }
      elseif ($name === WebDriverCapabilityType::PLATFORM) {
        $caps->setPlatform($value);
      }
      else {
        $caps->setCapability($name, $value);
      }
    }
    // Gather all capabilities, including defaults to be reachable by children.
    $this->desiredCapabilities = $caps->toArray();

    // Use ChromeDriver directly instead of the proxy like Selenium or Appium.
    if ($this->chromeDriverUse && $this->chromeDriverBinaryPath
        && in_array($this->browser, array('chrome', 'appium-chrome', 'appium-chromebeta', 'appium-chromium', 'appium-browser'), TRUE)) {
      $this->driver = $this->createChromeDriverService($caps);
    }
    // Otherwise, use the proxy server depending on device type.
    elseif (strpos($this->browser, 'appium-') === 0 || $this->browser === 'selendroid') {
      $this->driver = MobileRemoteWebDriver::create($this->server['host'], $caps, $this->timeoutConnection);
    }
    // Otherwise, use the proxy server.
    else {
      $this->driver = RemoteWebDriver::create($this->server['host'], $caps, $this->timeoutConnection);
    }
  }

  /**
   * Method helper to create A RemoteWebDriver instance without proxy.
   *
   * @param DesiredCapabilities $caps
   *   Desired capabilities to forward to Chrome Driver.
   *
   * @return ChromeDriver
   *   A RemoteWebDriver instance.
   */
  protected function createChromeDriverService(DesiredCapabilities $caps) {

    $this->chromeDriverArgs[] = "--port=$this->chromeDriverPort";

    if ($this->chromeDriverLogFile) {
      $this->chromeDriverArgs[] = "--log-path=$this->chromeDriverLogFile";
    }
    if ($this->chromeDriverVerbose) {
      $this->chromeDriverArgs[] = "--verbose";
    }
    if ($this->chromeDriverSilent) {
      $this->chromeDriverArgs[] = "--silent";
    }
    if ($this->chromeDriverAdbPort) {
      $this->chromeDriverArgs[] = "--adb-port=$this->chromeDriverAdbPort";
    }

    $service = new ChromeDriverService($this->chromeDriverBinaryPath, $this->chromeDriverPort, $this->chromeDriverArgs);
    return ChromeDriver::start($caps, $service);
  }

  /**
   * Override parent to split it into further methods.
   */
  protected function tearDown() {

    $this->preTearDown();
    parent::tearDown();
    $this->postTearDown();
  }

  /**
   * Pre tear down method to be easily used by children class.
   */
  protected function preTearDown() {

    // Shutdown the browser instance running.
    try {
      $this->driver->quit();
    }
    catch (WebDriverException $e) {
      $this->exceptionHandler($e);
    }

    // Final check for catchable errors.
    $this->assertTestingSiteErrors();

    // Like parent, read and write fatal errors of testing site at this step
    // only. But do it before parent to log it per browser.
    // Note that doing it only at teardown doesn't really make sense. Indeed,
    // we should instead check it after each requests (GET/POST) to keep the
    // history it happen...
    $this->assertTestingSiteFatalErrors();
  }

  /**
   * Post tear down method to be easily used by children class.
   */
  protected function postTearDown() {

    // Remove successfully verbose directory.
    if ($this->verboseClearOnSuccess) {

      $pass = TRUE;
      foreach ($this->assertions as $assertion) {
        if (in_array($assertion['status'], array('fail', 'exception'), TRUE)) {
          $pass = FALSE;
          break;
        }
      }

      if ($pass) {
        file_unmanaged_delete_recursive($this->verboseDirectory);
      }
    }
  }

  /**
   * Override parent method to split it into further methods for children
   * classes.
   *
   * @param stdClass $account
   *   A user account object as returned by self::drupalCreateUser().
   */
  protected function drupalLogin(stdClass $account) {

    if ($this->loggedInUser) {
      $this->drupalLogout();
    }

    $this->drupalLoginPerform($account);

    $is_logged = $this->isLoggedIn();
    $pass = $this->assertTrue($is_logged, t('User %name successfully logged in.', array('%name' => $account->name)), t('User login'));
    if ($pass) {
      $this->loggedInUser = $account;
    }
  }

  /**
   * Override parent method to split it into further methods for children
   * classes.
   */
  protected function drupalLogout() {

    $this->drupalLogoutPerform();

    $is_logged = $this->isLoggedIn();
    $pass = $this->assertFalse($is_logged, t('User %name successfully logged out.', array('%name' => $this->loggedInUser->name)), t('User logout'));
    if ($pass) {
      $this->loggedInUser = FALSE;
    }
  }

  /**
   * Perform a log in action againsts a given account.
   *
   * @param stdClass $account
   *   A user account object as returned by self::drupalCreateUser().
   */
  protected function drupalLoginPerform(stdClass $account) {
    $edit = array(
      'name' => $account->name,
      'pass' => $account->pass_raw
    );
    $this->drupalPost('user', $edit, t('Log in'));
  }

  /**
   * Perform a log out action againsts the current active session.
   */
  protected function drupalLogoutPerform() {
    $this->drupalGet('user/logout');
  }

  /**
   * Method helper to determine if current browser session is logged in.
   * Note that we can not check cookie session because we could have anonymous
   * session. Furthermore, some driver don't return HTTP only cookie.
   *
   * @return bool
   *   Whether user is logged in or not.
   */
  protected function isLoggedIn() {
    // Use body tag with class 'not-logged-in' (not link text 'Log out').
    // We don't catch exception because we MUST found body tag. Otherwise, we
    // may return a wrong result (false but browser session is authenticated).
    $body = $this->driver->wait(5)->until(WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::tagName('body')));
    return strpos($body->getAttribute('class'), 'not-logged-in') === FALSE;
  }

  /**
   * Override parent to fetch an url given.
   */
  protected function drupalGet($path, array $options = array(), array $headers = array()) {
    $options['absolute'] = TRUE;

    $url = url($path, $options);
    $this->driver->get($url);

    $output = $this->getPageSource();
    $current_url = $this->getUrl();
    $this->drupalSetContent($output, $current_url);
    $this->assertTestingSiteErrors();
    $this->verbose('GET request to: ' . $url .
                   '<hr />Ending URL: ' . $current_url .
                   '<hr />' . $this->getScreenshotLink() .
                   '<hr />' . $output);

    $message = t('@url returned response (!length).', array(
      '@url' => $url,
      '!length' => format_size(strlen($output)),
    ));
    $this->assertTrue($output, $message, t('Browser'));
    return $output;
  }

  /**
   * Override parent to deals with form API in POST.
   */
  protected function drupalPost($path, $edit, $submit, array $options = array(), array $headers = array(), $form_html_id = NULL, $extra_post = NULL) {
    $ajax = is_array($submit);
    $post_array = array();

    // Go to the page if needed.
    if (isset($path)) {
      $this->drupalGet($path, $options);
    }

    // Get RemoteWebElement form.
    $form = $this->getForm($form_html_id);
    if (!$form) {
      $this->fail('Failed to found the form.');
      return;
    }

    // Display element inside vertical tabs only if needed.
    $this->disableVerticalTabs();

    // Populate values of form.
    $this->processForm($post_array, $edit, $submit, $form);

    // Emulate form submission with key ENTER.
    if ($submit === NULL) {
      $form->submit();
    }
    else {

      // Get trigerring element.
      $triggering_element = $this->getTriggeringElement($submit, $form);
      if (!$triggering_element) {
        $this->fail('Failed to found the triggering element.');
        return;
      }

      // Submit the form with the submit trigerring element.
      $triggering_element->click();

      // Wait until throbber or progress bar is visible.
      if ($ajax) {
        $this->driver->wait(5)->until(
          WebDriverExpectedCondition::invisibilityOfElementLocated(
            WebDriverBy::className('ajax-progress')
          )
        );
      }
    }

    $content = $this->getPageSource(TRUE);
    $current_url = $this->getUrl();
    $this->drupalSetContent($content, $current_url);
    $this->assertTestingSiteErrors();
    $this->verbose('POST request to: ' . $path .
                         '<hr />Ending URL: ' . $current_url .
                         '<hr />Fields: ' . highlight_string('<?php ' . var_export($post_array, TRUE), TRUE) .
                         '<hr />' . $this->getScreenshotLink() .
                         '<hr />' . $content);

    return $this->content;
  }

  /**
   * Override parent method to POST a form through AJAX.
   */
  protected function drupalPostAJAX($path, $edit, $selector, $ajax_path = NULL, array $options = array(), array $headers = array(), $form_html_id = NULL, $ajax_settings = NULL) {
    $post_array = array();

    if (isset($path)) {
      $this->drupalGet($path, $options);
    }

    $form = $this->getForm($form_html_id);
    if (!$form) {
      $this->fail('Failed to found the form.');
      return;
    }

    $this->disableVerticalTabs();

    $triggering_element = $this->getTriggeringElement($selector, $form, FALSE, $edit);
    if (!$triggering_element) {
      $this->fail('Failed to found the triggering element.');
      return;
    }
    // Get triggering element info before it may be detach from the DOM.
    // i.e : form completly replace through AJAX with self::processForm().
    $tag_name = $triggering_element->getTagName();
    $type = $triggering_element->getAttribute('type');
    $is_submit = $this->elementIsSubmit($triggering_element);

    // Populate values of form, maybe with triggering element itself.
    $this->processForm($post_array, $edit, $selector, $form);

    // If triggered element is a button (still attached), submit it.
    if ($is_submit) {
      $triggering_element->click();
    }
    // For blur event, try to loose focus.
    elseif ($tag_name === 'textarea' || ($tag_name === 'input' && in_array($type, array('text', 'password'), TRUE))) {

      // Key press TAB not working. Android only.
      if (in_array($this->browser, array('appium-chrome', 'appium-chromium'), TRUE)) {
        $previous_context = $this->driver->getCurrentContext();
        $this->driver->switchToContext('NATIVE_APP');
        $this->driver->keyEvent(61); // tab
        sleep(1);
        try {
          // Needed with Chrome only, useless for Chromium.
          $this->driver->hideKeyboard();
        }
        catch (WebDriverException $e) {}
        $this->driver->switchToContext($previous_context);
      }
      // Browsers which don't support TAB key press need an another ugly tricks.
      elseif (in_array($this->browser, array('htmlunit', 'safari', 'selendroid'), TRUE)) {
        $this->driver->findElement(WebDriverBy::tagName('body'))->click();
      }
      // Otherwise, use key press TAB which is the best solution.
      else {
        $this->driver->getKeyboard()->pressKey(WebDriverKeys::TAB);
        $this->driver->getKeyboard()->releaseKey(WebDriverKeys::TAB);
      }
    }

    // Wait until throbber or progress bar is visible.
    $this->driver->wait(5)->until(
      WebDriverExpectedCondition::invisibilityOfElementLocated(
        WebDriverBy::className('ajax-progress')
      )
    );

    // Add verbose message too.
    $content = $this->getPageSource(TRUE);
    $current_url = $this->getUrl();
    $this->drupalSetContent($content, $current_url);
    $this->assertTestingSiteErrors();
    $this->verbose('POST AJAX request ' .
               '<hr />Trigerring element: ' . highlight_string('<?php ' . var_export($selector, TRUE), TRUE) .
               '<hr />Fields: ' . highlight_string('<?php ' . var_export($post_array, TRUE), TRUE) .
               '<hr />' . $this->getScreenshotLink() .
               '<hr />' . $content);

    return $this->content;
  }

  /**
   * Rewrite of parent::handleForm() to deals with non submit form elements.
   *
   * @param array $post
   *   Reference to array of post values.
   * @param array $edit
   *   An array of edit values to be checked against the form.
   * @param string|array $submit
   *   Form submit button value.
   * @param RemoteWebElement $form
   *   A RemoteWebElement object of the form to handle.
   *
   * @see parent::handleForm().
   */
  protected function processForm(&$post, $edit, $submit, RemoteWebElement $form) {
    $ajax = is_array($submit);

    foreach ($edit as $name => $value) {

      $inputs = $form->findElements(WebDriverBy::name($name));
      if (!$inputs) {
        $this->fail(t('Failed to found field @name.', array('@name' => $name)));
        continue;
      }

      // Try to find enabled and displayed elements with the same name and
      // execute the first one founded.
      try {
        $attempt = 0;
        foreach ($inputs as $input) {
          $attempt++;

          if (!$input->isEnabled()) {
            if ($attempt >= count($inputs)) {
              $this->fail(t('Field @name is disabled.', array('@name' => $name)));
            }
          }
          // Be careful, some elements could not be visible (CSS or JS like tabs).
          elseif (!$input->isDisplayed()) {
            if ($attempt >= count($inputs)) {
              $this->fail('Field @name is not displayed/visible (via JS or CSS).', array('@name' => $name));
            }
          }
          else {

            $tag_name = $input->getTagName();
            $type = $input->getAttribute('type');

            if ($tag_name === 'select') {

              $select = new WebDriverSelect($input);
              if ($select->isMultiple()) {
                $select->deselectAll();
              }
              $values = is_array($value) ? $value : array($value);
              foreach ($values as $value) {
                // Fix an AJAX bug with selendroid ("element no longer attach to
                // the DOM" exception throw). But everything works fine.
                try {
                  $select->selectByValue($value);
                }
                catch (UnknownServerException $e) {
                  if ($this->browser !== 'selendroid' || !$ajax) {
                    throw $e;
                  }
                }
              }
            }
            elseif ($tag_name === 'input' && $type === 'checkbox') {
              // Click depending on current value and value provided.
              $ori_value = $input->getAttribute('checked') === 'true';
              if ((!$ori_value && $value) || ($ori_value && !$value)) {
                $input->click();
              }
            }
            elseif ($tag_name === 'input' && $type === 'radio') {
              // Search for the next radio with provided value.
              if ((string)$input->getAttribute('value') !== (string)$value) {
                continue;
              }
              // Radio could not be clicked twice or reset manually.
              if ($input->getAttribute('checked') !== 'true') {
                $input->click();
              }
            }
            // Autocomplete.
            elseif ($tag_name === 'input' && $type === 'text' && strpos($input->getAttribute('class'), 'form-autocomplete') !== FALSE && $value !== "") {

              // Type just the search, not the ID.
              $search = $value;
              if (preg_match('/\([0-9]*\)$/', $value)) {
                $search = trim(substr($value, 0, strrpos($value, '(')));
              }
              $input->clear();
              $input->sendKeys($search);

              // Wait suggestion through AJAX.
              $by_suggestion = WebDriverBy::xpath('//div[@id="autocomplete"]/ul/li/div');
              try {
                $this->driver->wait(5)->until(
                  WebDriverExpectedCondition::presenceOfAllElementsLocatedBy(
                    $by_suggestion
                  )
                );
              }
              catch (NoSuchElementException $e) {}
              catch (TimeOutException $e) {}

              // Try to find suggestion in the list even if timeout is exceed.
              $found = FALSE;
              $suggestions = $this->driver->findElements($by_suggestion);
              foreach ($suggestions as $suggestion) {

                if (stripos($suggestion->getText(), $search) !== FALSE) {
                  $suggestion->click();
                  $found = TRUE;
                  break;
                }
              }
              if (!$found) {
                $this->fail('Failed to select option in autocomplete list.');
              }
            }
            elseif ($tag_name === 'input' && $type === 'file') {
              if (in_array($this->browser, array('safari', 'opera', 'selendroid', 'appium-chrome', 'appium-chromium'), TRUE)) {
                $this->fail(sprintf('%s driver does not support upload file.', $this->browser));
                continue;
              }
              $input->setFileDetector(new LocalFileDetector());
              $input->sendKeys($value);
            }
            // Color and range tags doesn't support RemoteWebElement::sendKeys()
            // method and have no specific implementation yet.
            // With Chrome at least, user dates input is formatted by the
            // browser's widgets and formats are not the same as W3C (but
            // default values or POST data do).
            // To achieve it easily on any browsers, we used an ugly tricks to
            // set values with JS.
            elseif ($tag_name === 'input' && in_array($type, array('color', 'range', 'date', 'datetime', 'datetime-local', 'month', 'time', 'week'), TRUE)) {
              try {
                $this->driver->executeScript("jQuery('input[name=$name]').val('$value');");
              }
              catch (UnknownServerException $e) {
                if ($this->browser !== 'opera') {
                  throw $e;
                }
              }
            }
            // Other HTML inputs : textarea, input type : text, password, email,
            // number, search, tel, url.
            else {
              $input->clear();
              $input->sendKeys($value);
            }

            $post[$name] = $value;
            break;
          }
        }
      }
      catch (WebDriverException $e) {
        $this->fail(t('Failed to set field @name with value @value and error : @error', array(
          '@name' => $name,
          '@value' => $value,
          '@error' => $e->getMessage(),
        )));
      }
    }
  }

  /**
   * Override parent method to click on a link for a text and optionally an
   * index given.
   */
  protected function clickLink($label, $index = 0) {
    $url_before = $this->getUrl();
    $url_target = '';

    $links = $this->driver->findElements(WebDriverBy::linkText($label));
    if (isset($links[$index])) {
      $url_target = $links[$index]->getAttribute('href');
    }

    $this->assertTrue($url_target, t('Clicked link %label (@url_target) from @url_before', array('%label' => $label, '@url_target' => $url_target, '@url_before' => $url_before)), t('Browser'));

    if ($url_target) {
      // Use click event instead of self::drupalGet().
      $links[$index]->click();

      // Mimic verbose message like self::drupalGet().
      $output = $this->getPageSource();
      $this->drupalSetContent($output, $url_target);
      $this->assertTestingSiteErrors();
      $this->verbose('GET request to: ' . $url_target .
                     '<hr />Ending URL: ' . $this->getUrl() .
                     '<hr />' . $this->getScreenshotLink() .
                     '<hr />' . $output);

      $message = t('!method @url returned @status (!length).', array(
        '@url' => $url_target,
        '!length' => format_size(strlen($output)),
      ));
      $this->assertTrue($output, $message, t('Browser'));
      return $output;
    }

    return FALSE;
  }

  /**
   * Override parent to retrieve current absolute URL.
   */
  protected function getUrl() {
    return $this->driver->getCurrentURL();
  }

  /**
   * Override parent method to write HTML verbose page per test methods instead.
   */
  protected function verbose($message) {

    if (!$this->isVerbose) {
      return;
    }
    $filepath = $this->verboseDirectory . "/html/$this->verboseID.html";

    $message = "<hr />ID #$this->verboseID ( " .
               ($this->verboseID > 1 ? '<a href="' . ($this->verboseID - 1) . '.html">Previous</a> |' : '') .
               // We always set a next link because we can't know if there would
               // be new verbose messages later.
               '<a href="' . ($this->verboseID + 1) . '.html">Next</a>' .
               ')<hr />' . $message;
    file_put_contents($filepath, $message, FILE_APPEND);

    $message = l(t('Verbose message'), file_create_url($filepath), array('attributes' => array('target' => '_blank')));
    $this->assert('debug', $message, 'Debug');
    $this->verboseID++;
  }

  /**
   * Because WebDriver don't support HTTP headers, we cannot check
   * X-Drupal-Assertion-NUMBER headers.
   * With a core patch, we could alternatively log errors into a file. It is a
   * better and much simple approach than using cookies for example.
   *
   * @see parent::curlHeaderCallback()
   * @see _drupal_log_error()
   */
  protected function assertTestingSiteErrors() {

    $logfile = "$this->public_files_directory/error_catchable.log";
    if (file_exists($logfile)) {
      foreach (file($logfile) as $line) {
        list($message, $group, $caller) = unserialize($line);
        $this->error($message, $group, $caller);
      }
      file_unmanaged_delete($logfile);
    }
  }

  /**
   * Rewrite of simpletest_log_read() called by parent::tearDown() in order to
   * log fatal errors of testing website.
   * It's allow us to store fatal errors by browsers and capabilities.
   *
   * @see self::preTearDown()
   * @see parent::tearDown()
   * @see simpletest_log_read()
   * @see _drupal_bootstrap_full()
   */
  protected function assertTestingSiteFatalErrors() {

    $logfile = "$this->public_files_directory/error.log";
    if (file_exists($logfile)) {
      foreach (file($logfile) as $line) {

        // Parse PHP fatal errors
        if (preg_match('/\[.*?\] (.*?): (.*?) in (.*) on line (\d+)/', $line, $match)) {
          $caller = array(
            'line' => $match[4],
            'file' => $match[3],
            'function' => 'Unknown',
          );
          $this->error(trim($match[2]), $match[1], $caller);
        }
        // Unknown format, place the entire message in the log.
        else {
          $this->error($line, 'PHP Fatal error');
        }
      }

      // Delete file to prevent parent asserting same errors.
      file_unmanaged_delete($logfile);
    }
  }

  /**
   * Override parent to insert additionnal arguments.
   *
   * @param string $browser
   *   The current browser name executed.
   * @param array capabilities
   *   An array of requested capabilities, not the final capabilities set by
   *   classes.
   * @param string $server_type
   *   The current server type used.
   */
  public static function insertAssert($test_id, $test_class, $status, $message = '', $group = 'Other', array $caller = array(), $browser = '', array $capabilities = array(), $server_type = '') {

    if (is_bool($status)) {
      $status = $status ? 'pass' : 'fail';
    }

    $caller += array(
      'function' => t('Unknown'),
      'line' => 0,
      'file' => t('Unknown'),
    );

    $assertion = array(
      'test_id' => $test_id,
      'test_class' => $test_class,
      'status' => $status,
      'message' => $message,
      'message_group' => $group,
      'function' => $caller['function'],
      'line' => $caller['line'],
      'file' => $caller['file'],
      'browser' => $browser,
      'capabilities' => $capabilities ? serialize($capabilities) : NULL,
      'server_type' => $server_type,
    );

    return db_insert('simpletest')
      ->fields($assertion)
      ->execute();
  }

  /**
   * Override parent method to insert additionnal columns.
   */
  protected function assert($status, $message = '', $group = 'Other', array $caller = NULL) {

    // Convert boolean status to string status.
    if (is_bool($status)) {
      $status = $status ? 'pass' : 'fail';
    }

    // Increment summary result counter.
    $this->results['#' . $status]++;

    // Get the function information about the call to the assertion method.
    if (!$caller) {
      $caller = $this->getAssertionCall();
    }

    // Creation assertion array that can be displayed while tests are running.
    $this->assertions[] = $assertion = array(
      'test_id' => $this->testId,
      'test_class' => get_class($this),
      'status' => $status,
      'message' => $message,
      'message_group' => $group,
      'function' => $caller['function'],
      'line' => $caller['line'],
      'file' => $caller['file'],
      'browser' => $this->browser ? $this->browser : '',
      'capabilities' => $this->defaultDesiredCapabilities ? serialize($this->defaultDesiredCapabilities) : NULL,
      'server_type' => $this->server ? $this->server['type'] : '',
    );

    // Store assertion for display after the test has completed.
    try {
      $connection = Database::getConnection('default', 'simpletest_original_default');
    }
    catch (DatabaseConnectionNotDefinedException $e) {
      // If the test was not set up, the simpletest_original_default
      // connection does not exist.
      $connection = Database::getConnection('default', 'default');
    }
    $connection
      ->insert('simpletest')
      ->fields($assertion)
      ->execute();

    // We do not use a ternary operator here to allow a breakpoint on
    // test failure.
    if ($status == 'pass') {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Override parent to check that title is the same as provided.
   */
  protected function assertTitle($title, $message = '', $group = 'Other') {
    $actual = $this->driver->getTitle();
    if (!$message) {
      $message = t('Page title @actual is equal to @expected.', array(
        '@actual' => var_export($actual, TRUE),
        '@expected' => var_export($title, TRUE),
      ));
    }
    return $this->assertEqual($actual, $title, $message, $group);
  }

  /**
   * Override parent to check that title is not the same as provided.
   */
  protected function assertNoTitle($title, $message = '', $group = 'Other') {
    $actual = $this->driver->getTitle();
    if (!$message) {
      $message = t('Page title @actual is not equal to @unexpected.', array(
        '@actual' => var_export($actual, TRUE),
        '@unexpected' => var_export($title, TRUE),
      ));
    }
    return $this->assertNotEqual($actual, $title, $message, $group);
  }

  /**
   * Override parent to include other HTML tag.
   */
  protected function constructFieldXpath($attribute, $value) {
    $xpath = '//textarea[@' . $attribute . '=:value]|//input[@' . $attribute . '=:value]|//select[@' . $attribute . '=:value]|//button[@' . $attribute . '=:value]';
    return $this->buildXPathQuery($xpath, array(':value' => $value));
  }

  /**
   * Override parent to check if an element exists for the given value.
   */
  protected function assertFieldByXPath($xpath, $value = NULL, $message = '', $group = 'Other') {
    $match = FALSE;

    $elements = $this->driver->findElements(WebDriverBy::xpath($xpath));
    if ($elements && !isset($value)) {
      $match = TRUE;
    }
    else {
      foreach ($elements as $element) {

        $tag_name = $element->getTagName();
        $type = $element->getAttribute('type');

        if ($tag_name === 'select') {
          // Internet explorer 8 does not has attribute "selected".
          if ($this->browser === 'internet_explorer' && isset($this->desiredCapabilities['version']) && version_compare($this->desiredCapabilities['version'], 9, '<')) {
            $this->fail('Deprecated method with IE8 and lower.');
            return;
          }
          try {
            $match = (bool)$element->findElement(WebDriverBy::xpath($this->buildXPathQuery('.//option[@selected=:selected and @value=:value]', array(
              ':selected' => 'selected',
              ':value' => $value,
            ))));
          }
          catch (NoSuchElementException $e) {}
          // Needed for opera driver if element doesn't exists...
          catch (UnknownServerException $e) {
            if ($this->browser !== 'opera') {
              throw $e;
            }
          }

        }
        elseif ($tag_name === 'input' && $type === 'checkbox') {
          $checked = $element->getAttribute('checked') === 'true';
          $match = ($checked && $value) || (!$checked && !$value);
        }
        elseif ($tag_name === 'input' && $type === 'radio') {
          $match = (string)$value === (string)$element->getAttribute('value') && $element->getAttribute('checked') === 'true';
        }
        else {
          $match = (string)$element->getAttribute('value') === (string)$value;
        }

        if ($match) {
          break;
        }
      }
    }

    return $this->assertTrue($match, $message, $group);
  }

  /**
   * Override parent to check if an element doesn't exists or doesn't has the
   * given value.
   */
  protected function assertNoFieldByXPath($xpath, $value = NULL, $message = '', $group = 'Other') {
    $match = FALSE;

    $elements = $this->driver->findElements(WebDriverBy::xpath($xpath));
    if ($elements && !isset($value)) {
      $match = TRUE;
    }
    else {
      foreach ($elements as $element) {

        $tag_name = $element->getTagName();
        $type = $element->getAttribute('type');

        if ($tag_name === 'select') {
          // Internet explorer 8 does not add attribute "selected".
          if ($this->browser === 'internet_explorer' && isset($this->desiredCapabilities['version']) && version_compare($this->desiredCapabilities['version'], 9, '<')) {
            $this->fail('Deprecated method with IE8 and lower.');
            return;
          }
          try {
            $match = (bool)$element->findElement(WebDriverBy::xpath($this->buildXPathQuery('.//option[@selected=:selected and @value=:value]', array(
              ':selected' => 'selected',
              ':value' => $value,
            ))));
          }
          catch (NoSuchElementException $e) {}
          // Needed for opera driver if element doesn't exists...
          catch (UnknownServerException $e) {
            if ($this->browser !== 'opera') {
              throw $e;
            }
          }
        }
        elseif ($tag_name === 'input' && $type === 'checkbox') {
          $checked = $element->getAttribute('checked') === 'true';
          $match = ($checked && $value) || (!$checked && !$value);
        }
        elseif ($tag_name === 'input' && $type === 'radio') {
          $match = (string)$value === (string)$element->getAttribute('value') && $element->getAttribute('checked') === 'true';
        }
        else {
          $match = (string)$element->getAttribute('value') === (string)$value;
        }

        if ($match) {
          break;
        }
      }
    }

    return $this->assertFalse($match, $message, $group);
  }

  /**
   * Override parent method to check if a checkbox is checked.
   */
  protected function assertFieldChecked($id, $message = '') {
    try {
      $element = $this->driver->findElement(WebDriverBy::xpath($this->buildXPathQuery('//input[@type="checkbox" and @id=:id and @checked="checked"]', array(':id' => $id))));
    }
    catch (NoSuchElementException $e) {}
    return $this->assertTrue(isset($element), $message ? $message : t('Checkbox field @id is checked.', array('@id' => $id)), t('Browser'));
  }

  /**
   * Override parent method to check if a checkbox is not checked.
   */
  protected function assertNoFieldChecked($id, $message = '') {
    try {
      $element = $this->driver->findElement(WebDriverBy::xpath($this->buildXPathQuery('//input[@type="checkbox" and @id=:id and @checked="checked"]', array(':id' => $id))));
    }
    catch (NoSuchElementException $e) {}
    return $this->assertFalse(isset($element), $message ? $message : t('Checkbox field @id is not checked.', array('@id' => $id)), t('Browser'));
  }

  /**
   * Override parent to check if a select element exists and the specified
   * option is selected.
   * We cannot use Xpath for performance because IE8 does not has attribute
   * "selected".
   */
  protected function assertOptionSelected($id, $option, $message = '') {
    $match = FALSE;
    try {
      $element = $this->driver->findElement(WebDriverBy::id($id));
      $select = new WebDriverSelect($element);
      $options = $select->getAllSelectedOptions();
      foreach ($options as $select_option) {
        if ((string)$select_option->getAttribute('value') === $option) {
          $match = TRUE;
          break;
        }
      }
    }
    catch (NoSuchElementException $e) {}
    return $this->assertTrue($match, $message ? $message : t('Option @option for field @id is selected.', array('@option' => $option, '@id' => $id)), t('Browser'));
  }

  /**
   * Override parent to check if a select element doesn't exists or the
   * specified option is not selected.
   * We cannot use Xpath for performance because IE8 does not add attribute
   * "selected".
   */
  protected function assertNoOptionSelected($id, $option, $message = '') {
    $match = FALSE;
    try {
      $element = $this->driver->findElement(WebDriverBy::id($id));
      $select = new WebDriverSelect($element);
      $options = $select->getAllSelectedOptions();
      foreach ($options as $select_option) {
        if ((string)$select_option->getAttribute('value') === $option) {
          $match = TRUE;
          break;
        }
      }
    }
    catch (NoSuchElementException $e) {}
    return $this->assertFalse($match, $message ? $message : t('Option @option for field @id is not selected.', array('@option' => $option, '@id' => $id)), t('Browser'));
  }

  /**
   * Override parent method to don't rely on content fetched because link may be
   * added on client side by JS.
   */
  protected function assertLink($label, $index = 0, $message = '', $group = 'Other') {
    $links = $this->driver->findElements(WebDriverBy::linkText($label));
    $message = ($message ?  $message : t('Link with label %label found.', array('%label' => $label)));
    return $this->assert(isset($links[$index]), $message, $group);
  }

  /**
   * Override parent to check that no link exists, including dynamic links.
   */
  protected function assertNoLink($label, $message = '', $group = 'Other') {
    $links = $this->driver->findElements(WebDriverBy::linkText($label));
    $message = ($message ?  $message : t('Link with label %label not found.', array('%label' => $label)));
    return $this->assert(empty($links), $message, $group);
  }

  /**
   * Override parent to check also links added on client side.
   */
  protected function assertLinkByHref($href, $index = 0, $message = '', $group = 'Other') {
    $links = $this->driver->findElements(WebDriverBy::xpath($this->buildXPathQuery('//a[contains(@href, :href)]', array(':href' => $href))));
    $message = ($message ?  $message : t('Link containing href %href found.', array('%href' => $href)));
    return $this->assert(isset($links[$index]), $message, $group);
  }

  /**
   * Override parent to check also links not added on client side.
   */
  protected function assertNoLinkByHref($href, $message = '', $group = 'Other') {
    $links = $this->driver->findElements(WebDriverBy::xpath($this->buildXPathQuery('//a[contains(@href, :href)]', array(':href' => $href))));
    $message = ($message ?  $message : t('No link containing href %href found.', array('%href' => $href)));
    return $this->assert(empty($links), $message, $group);
  }

  /**
   * Override parent to refreshed current page source, including JS changes.
   */
  protected function drupalGetContent() {
    $content = $this->driver->getPageSource();

    // IE8 returns uppercase html tags. Convert it to lower.
    if ($this->browser === 'internet_explorer' && isset($this->desiredCapabilities['version']) && version_compare($this->desiredCapabilities['version'], 9, '<')) {
      $content = preg_replace_callback("#<(/?[A-Z].*?)>#s", function ($matches) {
        return strtolower($matches[0]);
      }, $content);
    }

    return $content;
  }

  /**
   * Override parent method to force plain text content to be refreshed.
   */
  protected function assertTextHelper($text, $message = '', $group, $not_exists) {
    $this->plainTextContent = FALSE;
    return parent::assertTextHelper($text, $message, $group, $not_exists);
  }

  /**
   * Override parent method to force plain text content to be refreshed.
   */
  protected function assertUniqueTextHelper($text, $message = '', $group, $be_unique) {
    $this->plainTextContent = FALSE;
    return parent::assertUniqueTextHelper($text, $message, $group, $be_unique);
  }

  /**
   * Override parent to check that HTTP status code for the current url is equal
   * to the provided status code.
   */
  protected function assertResponse($code, $message = '') {
    $status = $this->getCurrentHttpStatusCode();
    return $this->assertTrue($status !== NULL && $status == $code, $message ? $message : t('HTTP response expected !code, actual !curl_code', array('!code' => $code, '!curl_code' => $status)), t('Browser'));
  }

  /**
   * Override parent to check HTTP status code for the current url is not equal
   * to provided status code.
   */
  protected function assertNoResponse($code, $message = '') {
    $status = $this->getCurrentHttpStatusCode();
    return $this->assertFalse($status !== NULL && $status == $code, $message ? $message : t('HTTP response not expected !code, actual !curl_code', array('!code' => $code, '!curl_code' => $status)), t('Browser'));
  }

  /**
   * Initialize an HTTP authentification.
   *
   * Needs work because only HTTP basic auth is tested and at least NTLM and IE
   * would not working.
   * Also note that it hope that browser would cache credential for all test
   * execution.
   */
  protected function initHttpAuth() {

    if ($this->httpauth_method === CURLAUTH_NTLM) {
      throw new Exception('NTLM authentification method is not supported yet.');
    }
    elseif ($this->browser === 'internet_explorer') {
      throw new Exception('No HTTP authentification is implemented for internet explorer.');
    }

    // Skip HTTP prompt by initializing credential with front page.
    list($scheme, $path) = explode('://', $this->httpAuthInitUrl ? $this->httpAuthInitUrl : url('<front>', array('absolute' => TRUE)));
    $this->drupalGet("$scheme://$this->httpauth_credentials@$path");
  }

  /**
   * Method helper to send a cookie testing UA to switch on simpletest sandbox
   * for browser which doesn't support HTTP user agent setting override.
   */
  protected function sendCookieTestUA() {
    $this->userAgentCookie['value'] = base64_encode($this->userAgent);
    $this->addCookies(array($this->userAgentCookie));
  }

  /**
   * Start an Xdebug session on the remote web browser with cookie.
   */
  protected function startXdebugSession() {
    $this->addCookies(array($this->xDebugCookie));
  }

  /**
   * Method wrapper to return page source.
   *
   * @param bool $redirect_post
   *   Whether source are getting after a redirect POST.
   *
   * @return string
   *   The source of the current page.
   */
  protected function getPageSource($redirect_post = FALSE) {

    if (in_array($this->browser, array('internet_explorer', 'safari', 'opera'), TRUE)
      || ($redirect_post && $this->browser === 'selendroid')) {

      $this->waitForPageToLoad();
    }
    return $this->driver->getPageSource();
  }

  /**
   * Method helper to wait for a page to be loaded.
   * Rewrite of the waitForPageToLoad Java class.
   *
   * Note that this helper should be never called because drivers should already
   * implements it. But it seems to be buggy for some browsers...So in fact, the
   * only thing we are really doing is sleep for 1 second.
   * Use WebDriverWait class as soon as possible instead.
   *
   * @param bool $forceCheckLength
   *   Whether to force check length methods to be used. TRUE by default because
   *   most of the time, driver implementation already check document status but
   *   seems to don't be enough.
   * @param int $timeout
   *   Timeout in seconds.
   * @param int $interval
   *   Interval check in milliseconds.
   *
   * @see https://code.google.com/p/selenium/source/browse/java/client/src/com/thoughtworks/selenium/webdriven/commands/WaitForPageToLoad.java
   */
  protected function waitForPageToLoad($forceCheckLength = TRUE, $timeout = 30, $interval = 250) {

    // Micro sleep before we continue in case an async click needs processing.
    usleep(250);

    $state = NULL;
    if (!$forceCheckLength) {

      try {
        $state = $this->driver->executeScript("return !!document['readyState'];");
      }
      catch (WebDriverException $e) {
        usleep(500);
        try {
          $state = $this->driver->executeScript("return !!document['readyState'];");
        }
        catch (WebDriverException $e) {
          $this->fail('Failed to get state of document with exception : ' . $e->getMessage());
          return;
        }
      }
    }

    // Browser support ready state check ?
    $method = isset($state) ? 'getReadyStateUsingWait' : 'getLengthCheckingWait';
    // Ugly workaround for getLengthCheckingWait() method.
    $this->resetPageLoadCheckLength = TRUE;
    try {
      $this->driver->wait($timeout, $interval)->until(array($this, $method));
    }
    catch (TimeOutException $e) {
      $this->fail(sprintf('Failed to load page within %d seconds.', $timeout));
    }

    usleep(250);
  }

  /**
   * Internal method helper to check the state of the current document.
   * Copied from Java sources.
   *
   * @see self::waitForPageToLoad().
   */
  public function getReadyStateUsingWait(RemoteWebDriver $driver) {
    try {
      return (bool)$driver->executeScript("return 'complete' == document.readyState;");
    }
    // Possible page reload. Fine
    catch (Exception $e) {}
    return FALSE;
  }

  /**
   * Internal method helper alternative to presume that the page is fully loaded
   * if document state doesn't work.
   * Note that this method is also copied from Java sources.
   *
   * @see self::waitForPageToLoad().
   */
  public function getLengthCheckingWait(RemoteWebDriver $driver) {
    static $length, $seenAt;

    // Ugly workaround because it must be a separate class. Each caller must set
    // this flag to TRUE.
    if ($this->resetPageLoadCheckLength) {
      $length = $seenAt = NULL;
      $this->resetPageLoadCheckLength = FALSE;
    }

    // Page length needs to be stable for a second
    try {
      $currentLength = strlen($driver->findElement(WebDriverBy::tagName("body"))->getText());
      // First time we check.
      if ($seenAt === NULL) {
        $seenAt = round(microtime(TRUE) * 1000);
        $length = $currentLength;
        return FALSE;
      }

      // Page changed ?
      if ($currentLength !== $length) {
        $seenAt = round(microtime(TRUE) * 1000);
        $length = $currentLength;
        return FALSE;
      }

      // Page is the same but wait at least one second.
      return (round(microtime(TRUE) * 1000) - $seenAt) > 1000;
    }
    catch (NoSuchElementException $e) {}
    // Not in java class but body may be no longer attached to the DOM between
    // getting it and retrieving its content.
    catch (StaleElementReferenceException $e) {}
  }

  /**
   * Method helper to create a screenshot link.
   *
   * @return string
   *   A string containing HTML link of a screenhot.
   */
  protected function getScreenshotLink() {

    if (!$this->isVerbose) {
      return;
    }

    $filepath = $this->takeScreenshot();
    if ($filepath) {
      return l(t('Screenshot'), file_create_url($filepath), array('attributes' => array('target' => '_blank'), 'absolute' => TRUE));
    }
    return 'Unable to generate a screenshot image.';
  }

  /**
   * Method helper to take a screenshot and store it into a PNG file.
   *
   * @return string | bool
   *   A string with the path of the resulting file.
   *   FALSE on error or not supported.
   */
  protected function takeScreenshot() {

    if ($this->screenshotDisabled) {
      return FALSE;
    }

    try {
      $data = $this->driver->takeScreenshot();
    }
    catch (WebDriverException $e) {
      return FALSE;
    }

    return file_unmanaged_save_data($data, "$this->verboseDirectory/screenshots/screenshot.png", FILE_EXISTS_RENAME);
  }

  /**
   * Assert that a radio is checked for a given name and value.
   */
  protected function assertRadioChecked($name, $value, $message = '') {
    try {
      $element = $this->driver->findElement(WebDriverBy::xpath($this->buildXPathQuery('//input[@type="radio" and @name=:name and @value=:value and @checked="checked"]', array(
        ':name' => $name,
        ':value' => $value,
      ))));
    }
    catch (NoSuchElementException $e) {}

    return $this->assertTrue(isset($element), $message ? $message : t('Radio @name with value @value is checked.', array('@name' => $name, '@value' => $value)), t('Browser'));
  }

  /**
   * Assert that a radio is not checked for a given name and value.
   */
  protected function assertNoRadioChecked($name, $value, $message = '') {
    try {
      $element = $this->driver->findElement(WebDriverBy::xpath($this->buildXPathQuery('//input[@type="radio" and @name=:name and @value=:value and @checked="checked"]', array(
        ':name' => $name,
        ':value' => $value,
      ))));
    }
    catch (NoSuchElementException $e) {}

    return $this->assertFalse(isset($element), $message ? $message : t('Radio @name with value @value is not checked.', array('@name' => $name, '@value' => $value)), t('Browser'));
  }

  /**
   * Method helper to return a RemoteWebElement form.
   *
   * @param $form_html_id
   *   Optionally the ID of the form to retrieve. Otherwise, first form founded
   *   would be returned.
   *
   * @return RemoteWebElement | bool
   *   The RemoteWebElement form.
   *   FALSE otherwise.
   */
  protected function getForm($form_html_id = NULL) {

    try {
      return $form_html_id
        ? $this->driver->findElement(WebDriverBy::id($form_html_id))
        : $this->driver->findElement(WebDriverBy::tagName('form'));
    }
    catch (NoSuchElementException $e) {}
    return FALSE;
  }

  /**
   * Method helper to disable vertical tabs.
   */
  protected function disableVerticalTabs() {

    if (!$this->disableVerticalTabs) {
      return;
    }

    try {
      // Check that class exists may means that we have vertical script added.
      // In that way, we won't throw a JS error if script is not included.
      $this->driver->findElement(WebDriverBy::className('vertical-tabs-list'));
      $this->driver->executeScript('jQuery(".vertical-tabs-pane").show();');
    }
    catch (NoSuchElementException $e) {}
    catch (UnknownServerException $e) {
      // Strange exception throw by Opera driver but job is done...
      if ($this->browser !== 'opera') {
        throw $e;
      }
    }
  }

  /**
   * Method helper to get a triggering element provided into a form provided.
   *
   * @param string | array $triggering_element
   *   May be the value of the element or an array where key is the name and
   *   value is its value.
   *   If only value is provided, it should be a submit form element.
   * @param RemoteWebElement $form
   *   The RemoteWebElement form where the triggering element reside in.
   * @param bool $is_submit
   *   Whether triggering element should be a submit form element or not.
   * @param array $edit
   *   Optionnally an array of values to submit with the triggering element.
   *   If triggering element is not a submit button and $is_submit is FALSE,
   *   triggering element would be append to it.
   *
   * @return RemoteWebElement | null
   *   The triggering RemoteWebElement.
   *   NULL otherwise.
   */
  protected function getTriggeringElement($triggering_element, RemoteWebElement $form, $is_submit = TRUE, array &$edit = array()) {
    $element = $name = $value = NULL;

    // Have we name and value element ?
    if (is_array($triggering_element)) {

      reset($triggering_element);
      $name = key($triggering_element);
      $value = current($triggering_element);

      $xpath = './/input[@type="submit" and @name=:name and @value=:value]' .
               '|.//input[@type="image" and @name=:name and @value=:value]' .
               '|.//button[@type="submit" and @name=:name and @value=:value]';
      $xpath = $this->buildXPathQuery($xpath, array(
        ':name' => $name,
        ':value' => $value,
      ));
    }
    // We just have name. Must be a submit form element.
    else {
      $value = $triggering_element;
      $xpath = './/input[@type="submit" and @value=:value]' .
               '|.//input[@type="image" and @value=:value]' .
               '|.//button[@type="submit" and @value=:value]';
      $xpath = $this->buildXPathQuery($xpath, array(
        ':value' => $value,
      ));
    }

    // Then get triggering element. Looks for submit button first.
    try {
      // Retrieve each trigerring element founded, as we are not always using
      // unique identifier. Some of them could not be enabled or not visible, so
      // use the first one which isn't. With these checks, element is supposed
      // to be clickable,
      // @see WebDriverExpectedCondition::elementToBeClickable().
      $children = $form->findElements(WebDriverBy::xpath($xpath));
      foreach ($children as $child) {
        if ($child->isEnabled() && $child->isDisplayed()) {
          $element = $child;
          break;
        }
      }
    }
    catch (NoSuchElementException $e) {}

    // If not found, this is maybe not a submit form element. Check if element
    // exists just with its name, isn't a submit button and populate edit form
    // elements with.
    if (!$element && !$is_submit && $name && isset($value)) {

      $children = $form->findElements(WebDriverBy::name($name));
      foreach ($children as $child) {
        if ($child->isEnabled() && $child->isDisplayed() && !$this->elementIsSubmit($child)) {
          $element = $child;
          $edit[$name] = $value;
          break;
        }
      }
    }

    return $element;
  }

  /**
   * Method helper to determine if a provided RemoteWebElement is a form submit
   * element or not.
   *
   * @param RemoteWebElement $element
   *   The element to check.
   *
   * @return boolean
   *   Whether element is a form submit or not.
   */
  protected function elementIsSubmit(RemoteWebElement $element) {
    $tag_name = $element->getTagName();
    $type = $element->getAttribute('type');
    if (($tag_name === 'button' && $type === 'submit') || ($tag_name === 'input' && in_array($type, array('submit', 'image'), TRUE))) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Method helper to check partial link.
   */
  protected function assertPartialLink($partial_text, $index = 0, $message = '', $group = 'Other') {
    $links = $this->driver->findElements(WebDriverBy::partialLinkText(trim($partial_text)));
    $message = ($message ?  $message : t('Link with partial label %label found.', array('%label' => $partial_text)));
    return $this->assert(isset($links[$index]), $message, $group);
  }

  /**
   * Method helper to check partial link.
   */
  protected function assertNoPartialLink($partial_text, $message = '', $group = 'Other') {
    $links = $this->driver->findElements(WebDriverBy::partialLinkText(trim($partial_text)));
    $message = ($message ?  $message : t('Link with partial label %label not found.', array('%label' => $partial_text)));
    return $this->assert(empty($links), $message, $group);
  }

  /**
   * Method helper to retrieve the HTTP status code for the current url.
   * Indeed, WebDriver API doesn't support it.
   * So we have two work-around :
   * - first and default is to send an AJAX request to the current page with
   *   remote browser.
   * - use a curl client with cookies. Indeed, some driver (Opera presto) cannot
   *   return result from executed asynchronous scripts.
   *   Be careful, not all drivers (IE and Safari) return HTTP only cookie like
   *   session cookies. So this alternative could not be used for them.
   *
   * If you only want to check 200, 403 (access denied) or 404 (not found) codes,
   * the better work-around is to override this method and returns code
   * associated to the page markup.
   *
   * @return int
   *   A HTTP status code.
   *   0 or NULL if none retrieved.
   *
   * @see https://code.google.com/p/selenium/issues/detail?id=141.
   */
  protected function getCurrentHttpStatusCode() {
    $code = NULL;

    // Default work-around.
    if ($this->checkHttpStatusCodeWithJS) {

      // This method always pass a last global argument (named "arguments") in
      // addition to other which is the callback to called when asynchronous
      // script is finished. The argument pass to this callback would be then
      // return by the command.
      try {
        // Strangely, using jQuery.ajax() didn't work.
        $code = $this->driver->executeAsyncScript(
          "var callback = arguments[arguments.length - 1];" .
          "var xhr = new XMLHttpRequest();" .
          "xhr.open('GET', window.location.href, true);" .
          "xhr.onreadystatechange = function() {" .
          "  if (xhr.readyState == 4) {" .
          "    callback(xhr.status);" .
          "  }" .
          "};" .
          "xhr.send();"
        );
      }
      catch (ScriptTimeoutException $e) {}
    }
    // Otherwise, use cURL which may not work in some cases (cookie HTTP only
    // not returned, open_basedir).
    else {

      // Prepare cookies string.
      $cookies_string = array();
      $cookies = $this->getCookies();
      foreach ($cookies as $cookie) {
        $cookies_string[] = $cookie['name'] . '=' . $cookie['value'];
      }
      $cookies_string = implode('; ', $cookies_string);

      // Init curl with options.
      $ch = curl_init($this->getUrl());
      $curl_options = array(
        // Cookies needed, at least cookie session.
        CURLOPT_COOKIE => $cookies_string,
        // Not working with open_basedir.
        CURLOPT_FOLLOWLOCATION => TRUE,
        // Don't display data
        CURLOPT_RETURNTRANSFER => TRUE,
        // Required for HTTPS (no need to have certificate for this)
        CURLOPT_SSL_VERIFYPEER => FALSE,
        CURLOPT_SSL_VERIFYHOST => FALSE,
        // Spoof user agent to switch on dabatase test
        CURLOPT_USERAGENT => $this->userAgent,
      );
      if (isset($this->httpauth_credentials)) {
        $curl_options[CURLOPT_HTTPAUTH] = $this->httpauth_method;
        $curl_options[CURLOPT_USERPWD] = $this->httpauth_credentials;
      }

      $result = curl_setopt_array($ch, $curl_options);
      if (!$result) {
        throw new Exception('One or more cURL options could not be set.');
      }

      try {
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      }
      catch (Exception $e) {}
      curl_close($ch);
    }

    return (int)$code;
  }

  /**
   * Init cookie domain to match exactly the current domain site.
   */
  protected function initCookieDomain() {

    //  First, we need to go to the domain to match it later.
    if (!$this->url) {
      $this->drupalGet('<front>');
    }

    // Set cookie domain if not already done by children classes or by previous
    // test method.
    if (!$this->cookieDomain) {
      // Use default one provided in settings.php or detect by Drupal.
      $this->cookieDomain = $GLOBALS['cookie_domain'];
      // Cookie domain must be strickly egals to the current domain i.e : a
      // cookie domain like ".foo.com" could not be set for a current domain
      // like "foo.com".
      // For non-multisite, remove dot prefix to match current domain.
      if ($this->cookieDomain[0] === '.' && !file_exists(DRUPAL_ROOT . '/sites/sites.php')) {
        $this->cookieDomain = substr($this->cookieDomain, 1);
      }
    }

    $this->cookieDomainInit = TRUE;
  }

  /**
   * Method wrapper to fetch cookies.
   *
   * @param string $name
   *   The cookie name to retrieve. If omitted, retrieve all cookies visible.
   *
   * @return
   *   If $name is provided, an array for the cookie.
   *   Otherwise, an array of array cookies.
   */
  protected function getCookies($name = NULL) {
    if ($name) {
      return $this->driver->manage()->getCookieNamed($name);
    }
    return $this->driver->manage()->getCookies();
  }

  /**
   * Method wrapper to inject cookies for the current domain.
   *
   * @param array $cookies
   *   An array of array cookie with keys :
   *   - name
   *   - value
   *   - path
   *   - domain
   *   - secure : bool
   *   - expiry : int. Be careful, if set to 0, not all browsers understand it
   *     as the end of the session but on contrary won't add it.
   *   - httpOnly : bool
   *
   * @see WebDriverOptions::addCookie().
   */
  protected function addCookies(array $cookies = array()) {

    if (!$this->cookieDomainInit) {
      $this->initCookieDomain();
    }

    foreach ($cookies as $cookie) {

      // Cookie domain must exists and match the exact current domain.
      if (!isset($cookie['domain'])) {
        $cookie['domain'] = $this->cookieDomain;
      }
      if (!isset($cookie['expiry'])) {
        $cookie['expiry'] = REQUEST_TIME + (3600 * 24);
      }

      // Deals with cookie management issues.
      if ($this->browser === 'opera' && preg_match('/^[0-9.]+$/', $cookie['domain'])) {
        $this->fail('Opera driver could not add cookie with IP domain. Use host name instead.');
        continue;
      }

      try {
        $this->driver->manage()->addCookie($cookie);
      }
      catch (WebDriverException $e) {
        $this->fail("Failed to add cookie with error :" . $e->getMessage());
      }
    }
  }

  /**
   * Method wrapper to delete cookies.
   *
   * @param string $name
   *   The cookie name to delete. If omitted, it will delete all cookies visible.
   */
  protected function deleteCookies($name = NULL) {

    if ($this->browser === 'safari') {
      $this->fail('Safari driver could not delete cookies.');
      return;
    }
    elseif ($this->browser === 'selendroid' && $name) {
      $this->fail('Android driver could not delete cookies by name.');
      return;
    }

    if ($name) {
      return $this->driver->manage()->deleteCookieNamed($name);
    }
    return $this->driver->manage()->deleteAllCookies();
  }

  /**
   * Method helper to change the orientation for mobile.
   *
   * @param string $orientation
   *   The orientation to change. May be LANDSCAPE or PORTRAIT.
   */
  protected function setOrientation($orientation = 'LANDSCAPE') {

    // Note that selendroid don't require this capability.
    if (empty($this->desiredCapabilities[WebDriverCapabilityType::ROTATABLE])) {
      $this->fail(sprintf('%s driver does not support screen orientation', $this->browser));
      return;
    }

    // If driver didn't support orientation, we can use Appium native instead.
    $use_appium = in_array($this->browser, array('appium-chrome', 'appium-chromium', 'appium-browser'), TRUE);
    if ($use_appium) {
      $previous_context = $this->driver->getCurrentContext();
      $this->driver->switchToContext('NATIVE_APP');
    }

    $this->driver->manage()->window()->setScreenOrientation($orientation);

    // Restore previous context.
    if ($use_appium) {
      $this->driver->switchToContext($previous_context);
    }

    // Wait until it is done on client side, instead of using WebDriver API.
    $this->driver->wait(5)->until(function($driver) use ($orientation) {
      $code = (int)$driver->executeScript('return window.orientation');
      return $orientation === 'PORTRAIT' ? $code === 0 : $code === -90 || $code === 90;
    });
  }

  /**
   * Method helper to get the current mobile orientation.
   */
  protected function getOrientation() {

    $use_appium = in_array($this->browser, array('appium-chrome', 'appium-chromium', 'appium-browser'), TRUE);
    if ($use_appium) {
      $previous_context = $this->driver->getCurrentContext();
      $this->driver->switchToContext('NATIVE_APP');
    }

    $orientation = $this->driver->manage()->window()->getScreenOrientation();

    if ($use_appium) {
      $this->driver->switchToContext($previous_context);
    }

    return $orientation;
  }
}

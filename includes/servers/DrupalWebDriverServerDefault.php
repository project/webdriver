<?php

/**
 * @file
 * Provide a default implementation of the DrupalWebDriverServer interface.
 */

abstract class DrupalWebDriverServerDefault implements DrupalWebDriverServer {

  /**
   * Implements getCapabilitiesAvailables().
   */
  public function getCapabilitiesAvailables(array $requested_capabilities, array $capabilities) {
    $capabilities_available = array();

    foreach ($requested_capabilities as $requested_browser) {
      foreach ($capabilities as $capability) {

        // Only check name, not other capabilities.
        if ($requested_browser['browserName'] === $capability['browserName']) {
          $capabilities_available[] = $requested_browser;
          break;
        }
      }
    }

    return $capabilities_available;
  }

  /**
   * Implements getStatus().
   */
  public function getStatus($host) {
    $infos = array();
    $response = $this->executeCommandStatus($host);

    if ($response['code'] === 200) {
      if (!empty($response['response']['value']) && !empty($response['response']['value']['build']['version'])) {
        $infos['version'] = $response['response']['value']['build']['version'];
      }
      else {
        $infos['version'] = t('Unknown');
      }
    }
    // Grid hub server don't returns any status (for Selenium and thus,
    // Selendroid too which use it). Instead, you should request nodes which
    // return their status.
    elseif ($response['code'] === 500) {
      $infos['version'] = t('Grid hub server');
    }

    return $infos;
  }

  /**
   * Internal method to execute command status againsts the server and return
   * the JSON decoded response.
   *
   * @return array | null
   */
  protected function executeCommandStatus($host) {

    $curl = curl_init("$host/status");
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json'));
    $raw_results = trim(curl_exec($curl));
    $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    return array(
      'code' => $code,
      'response' => drupal_json_decode($raw_results),
    );
  }
}

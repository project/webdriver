<?php

/**
 * @file
 * Implements the DrupalWebDriverServer for the Appium server.
 */

class DrupalWebDriverServerAppium extends DrupalWebDriverServerDefault {

  /**
   * Implements getLabel().
   */
  public function getLabel() {
    return t('Appium');
  }

  /**
   * Implements getBrowsers().
   */
  public function getBrowsers() {
    return array(
      'appium-chrome',
      'appium-chromebeta',
      'appium-chromium',
      'appium-browser',
    );
  }

  /**
   * Implements getCapabilitiesDescription().
   */
  public function getCapabilitiesDescription() {
    return t('Required parameters : browser name, platform name and device name.');
  }

  /**
   * Implements getCapabilitiesElementForm().
   */
  public function getCapabilitiesElementForm(array $default_capabilities) {
    $element = array();

    $element['browserName'] = array(
      '#type' => 'select',
      '#title' => t('Browser name'),
      '#options' => array(
        'appium-chrome' => 'Chrome',
        'appium-chromebeta' => 'Chrome beta',
        'appium-chromium' => 'Chromium',
        'appium-browser' => 'Browser',
      ),
      '#empty_option' => '--' . t('None') . '--',
      '#empty_value' => '',
      '#default_value' => isset($default_capabilities['browserName']) ? $default_capabilities['browserName'] : '',
    );

    $element['deviceName'] = array(
      '#type' => 'textfield',
      '#title' => t('Device name'),
      '#description' => t('The kind of mobile device or emulator to use. For example: Android Emulator, Galaxy S4, iPad Simulator, iPhone Retina 4-inch, etc.'),
      '#size' => 32,
      '#default_value' => isset($default_capabilities['deviceName']) ? $default_capabilities['deviceName'] : '',
    );

    $element['platformName'] = array(
      '#type' => 'select',
      '#title' => t('Platform'),
      '#options' => array(
        'Android' => t('Android'),
      ),
      '#empty_option' => '--' . t('None') . '--',
      '#empty_value' => '',
      '#default_value' => isset($default_capabilities['platformName']) ? $default_capabilities['platformName'] : '',
    );

    $element['platformVersion'] = array(
      '#type' => 'textfield',
      '#title' => t('Platform version'),
      '#description' => t('API version to use. Example : 5.0, 4.4, 4.3, etc.'),
      '#element_validate' => array('element_validate_number'),
      '#maxlength' => 10,
      '#size' => 10,
      '#default_value' => isset($default_capabilities['platformVersion']) ? $default_capabilities['platformVersion'] : '',
    );

    return $element;
  }

  /**
   * Implements capabilitiesElementIsEmpty().
   */
  public function capabilitiesElementIsEmpty(array $capabilities) {
    return empty($capabilities['browserName']['#value'])
      || empty($capabilities['platformName']['#value'])
      || empty($capabilities['deviceName']['#value']);
  }

  /**
   * Implements capabilitiesElementValuesAlter().
   */
  public function capabilitiesElementValuesAlter(array $capabilities, array &$values) {

    if (empty($capabilities['platformVersion']['#value'])) {
      unset($values['platformVersion']);
    }

    $msg = $this->validateAndroid($capabilities['browserName']['#value'], $capabilities['platformName']['#value']);
    if ($msg) {
      form_error($capabilities['browserName'], t($msg));
    }
  }

  /**
   * Implements capabilitiesValidate().
   */
  public function capabilitiesValidate(array $capabilities) {
    $msgs = array();

    if (empty($capabilities['deviceName'])) {
      $msgs[] = t('deviceName capability is required.');
    }

    if (empty($capabilities['platformName'])) {
      $msgs[] = t('platformName capability is required.');
    }
    else {
      $msg = $this->validateAndroid($capabilities['browserName'], $capabilities['platformName']);
      if ($msg) {
        $msgs[] = $msg;
      }
    }

    return $msgs;
  }

  /**
   * Methods helper to validate Android capabilities.
   */
  protected function validateAndroid($browser, $platform) {
    if (in_array($browser, array('appium-chrome', 'appium-chromebeta', 'appium-chromium', 'appium-browser'), TRUE) && $platform !== 'Android') {
      return 'Android browsers could be used only on Android platform.';
    }
  }
}

<?php

/**
 * @file
 * WebDriver server interface mainly used to deals with capabilities.
 */

interface DrupalWebDriverServer {

  /**
   * @return string
   *   The human server name.
   */
  public function getLabel();

  /**
   * @return array
   *   An array of browser names supported.
   */
  public function getBrowsers();

  /**
   * @return string | null
   *   An additionnal capabilities description in the settings form.
   */
  public function getCapabilitiesDescription();

  /**
   * Returns a form element for capabilities supported by this server.
   *
   * @param array $default_capabilities
   *   An array of default capabilities set in config, keyed by capability name.
   *
   * @return array
   *   A form element to aggregate with the settings form.
   */
  public function getCapabilitiesElementForm(array $default_capabilities);

  /**
   * Determine whether capabilities are empty and thus could be deleted.
   *
   * @param array $capabilities
   *   A form element.
   *
   * @return bool
   */
  public function capabilitiesElementIsEmpty(array $capabilities);

  /**
   * Alter capabilities values. For e.g, to make some cleanup.
   *
   * @param array $capabilities
   *   A form element.
   *
   * @param array $values
   *   An array of values passed by reference. Must be altered directly.
   */
  public function capabilitiesElementValuesAlter(array $capabilities, array &$values);

  /**
   * Validate capabilities.
   *
   * @param array $capabilities
   *   An array of capabilities
   *
   * @return array
   *   Return an array of error messages.
   */
  public function capabilitiesValidate(array $capabilities);

  /**
   * From a list of requested capabilities, returns only ones availables for
   * this server. Typically, it check at least the browser name.
   *
   * @param array $requested_capabilities
   *   An array of requested capabilities. Not all may be supported.
   * @param array $capabilities
   *   An array of capabilities availables for this server.
   *
   * @return array
   *   An array of available capabilities from the requested capabilities.
   *   So finally its returned the requested capabilities filtered.
   */
  public function getCapabilitiesAvailables(array $requested_capabilities, array $capabilities);

  /**
   * @param string $host
   *   The host url of the webdriver server. For example, localhost:4444/wd/hub.
   *
   * @return array | NULL
   *   A json decode response from the server. May contains the following keys :
   *   - version
   *   If omitted but not empty, it assumes that server is reachable but cannot
   *   provide more informations.
   *   If empty, server is not responding.
   */
  public function getStatus($host);
}

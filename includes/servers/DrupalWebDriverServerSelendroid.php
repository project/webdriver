<?php

/**
 * @file
 * Implements the DrupalWebDriverServer for the Selendroid server.
 */

class DrupalWebDriverServerSelendroid extends DrupalWebDriverServerDefault {

  /**
   * Implements getLabel().
   */
  public function getLabel() {
    return t('Selendroid');
  }

  /**
   * Implements getBrowsers().
   */
  public function getBrowsers() {
    return array(
      'selendroid',
    );
  }

  /**
   * Implements getCapabilitiesDescription().
   */
  public function getCapabilitiesDescription() {}

  /**
   * Implements getCapabilitiesElementForm().
   */
  public function getCapabilitiesElementForm(array $default_capabilities) {
    $element = array();

    $element['browserName'] = array(
      '#type' => 'value',
      '#value' => 'selendroid',
    );

    $element['platform'] = array(
      '#type' => 'value',
      '#value' => 'ANDROID',
    );

    $element['emulator'] = array(
      '#type' => 'select',
      '#title' => t('Emulator'),
      '#options' => array(
        1 => t('Yes'),
        0 => t('No'),
      ),
      '#empty_option' => '-- ' . t('Select') . ' --',
      '#empty_value' => '',
      '#default_value' => isset($default_capabilities['emulator']) ? $default_capabilities['emulator'] : '',
    );

    $element['platformVersion'] = array(
      '#type' => 'textfield',
      '#title' => t('Platform version'),
      '#description' => t('API version to use. Example : 19 (= 4.4), 18 (=4.3).'),
      '#element_validate' => array('element_validate_number'),
      '#maxlength' => 10,
      '#size' => 10,
      '#default_value' => isset($default_capabilities['platformVersion']) ? $default_capabilities['platformVersion'] : '',
    );

    $element['screenSize'] = array(
      '#type' => 'textfield',
      '#title' => t('Screen size'),
      '#description' => t('Enter the screen size to match. Example value: 1024x600, 720x1280, WQVGA432'),
      '#maxlength' => 15,
      '#size' => 15,
      '#default_value' => isset($default_capabilities['screenSize']) ? $default_capabilities['screenSize'] : '',
    );

    return $element;
  }

  /**
   * Implements capabilitiesElementIsEmpty().
   */
  public function capabilitiesElementIsEmpty(array $capabilities) {
    return
      !$capabilities['enabled']['#value']
      && $capabilities['emulator']['#value'] === ''
      && empty($capabilities['platformVersion']['#value'])
      && empty($capabilities['screenSize']['#value']);
  }

  /**
   * Implements capabilitiesElementValuesAlter().
   */
  public function capabilitiesElementValuesAlter(array $capabilities, array &$values) {

    if ($capabilities['emulator']['#value'] === '') {
      unset($values['emulator']);
    }
    if (empty($capabilities['platformVersion']['#value'])) {
      unset($values['platformVersion']);
    }
    if (empty($capabilities['screenSize']['#value'])) {
      unset($values['screenSize']);
    }
  }

  /**
   * Implements capabilitiesValidate().
   */
  public function capabilitiesValidate(array $capabilities) {
    $msgs = array();

    if (!empty($capabilities['platform'])
      && $capabilities['browserName'] === 'selendroid'
      && $capabilities['platform'] !== 'ANDROID') {

      $msgs[] = 'Selendroid must be on an Android OS.';
    }

    return $msgs;
  }
}

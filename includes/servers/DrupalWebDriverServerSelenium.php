<?php

/**
 * @file
 * Implements the DrupalWebDriverServer for the Selenium server.
 */

class DrupalWebDriverServerSelenium extends DrupalWebDriverServerDefault {

  /**
   * Implements getLabel().
   */
  public function getLabel() {
    return t('Selenium');
  }

  /**
   * Implements getBrowsers().
   */
  public function getBrowsers() {
    return array(
      'firefox',
      'chrome',
      'internet_explorer',
      'htmlunit',
      'phantomjs',
      'opera',
      'safari',
    );
  }

  /**
   * Implements getCapabilitiesDescription().
   */
  public function getCapabilitiesDescription() {
    return t('For HTMLunit browser, you can set Javascript engine to use with version capability. Enter "firefox" or "internet explorer-7". It could be followed by "-VERSION". For example, "firefox-32".');
  }

  /**
   * Implements getCapabilitiesElementForm().
   */
  public function getCapabilitiesElementForm(array $default_capabilities) {
    $element = array();

    $element['browserName'] = array(
      '#type' => 'select',
      '#title' => t('Browser name'),
      '#options' => array(
        'firefox'=> 'Firefox',
        'chrome' => 'Chrome',
        'internet_explorer' => 'Internet Explorer',
        'htmlunit' => 'HTML unit',
        'phantomjs' => 'Phantom JS',
        'safari' => 'Safari',
        'opera' => 'Opera',
      ),
      '#empty_option' => '--' . t('None') . '--',
      '#empty_value' => '',
      '#default_value' => isset($default_capabilities['browserName']) ? $default_capabilities['browserName'] : '',
    );

    $element['version'] = array(
      '#type' => 'textfield',
      '#title' => t('Version'),
      '#element_validate' => $default_capabilities && $default_capabilities['browserName'] === 'htmlunit' ? array() : array('element_validate_number'),
      '#maxlength' => 10,
      '#size' => 10,
      '#default_value' => isset($default_capabilities['version']) ? $default_capabilities['version'] : '',
    );

    // Need to be hardcoded because php-webdriver library may not be loaded.
    $element['platform'] = array(
      '#type' => 'select',
      '#title' => t('Platform'),
      '#options' => array(
        'LINUX' => 'Linux',
        'UNIX' => 'Unix',
        'MAC' => 'Mac',
        'WINDOWS' => 'Windows',
        'VISTA' => 'Vista',
        'XP' => 'XP',
      ),
      '#empty_option' => t('Any'),
      '#empty_value' => '',
      '#default_value' => isset($default_capabilities['platform']) ? $default_capabilities['platform'] : '',
    );

    return $element;
  }

  /**
   * Implements capabilitiesElementIsEmpty().
   */
  public function capabilitiesElementIsEmpty(array $capabilities) {
    return empty($capabilities['browserName']['#value']);
  }

  /**
   * Implements capabilitiesElementValuesAlter().
   */
  public function capabilitiesElementValuesAlter(array $capabilities, array &$values) {

    // Version check.
    if (empty($capabilities['version']['#value'])) {
      unset($values['version']);
    }
    elseif ($capabilities['browserName']['#value'] === 'htmlunit') {
      $msg = $this->validateHTMLUnitVersion($capabilities['version']['#value']);
      if ($msg) {
        form_error($capabilities['version'], t($msg));
      }
    }

    // Platform check.
    if (empty($capabilities['platform']['#value'])) {
      unset($values['platform']);
    }
    else {
      if ($capabilities['browserName']['#value'] === 'internet_explorer') {
        $msg = $this->validateInternetExplorer($capabilities['platform']['#value']);
        if ($msg) {
          form_error($capabilities['platform'], t($msg));
        }
      }
    }
  }

  /**
   * Implements capabilitiesValidate().
   */
  public function capabilitiesValidate(array $capabilities) {
    $msgs = array();

    if (!empty($capabilities['platform']) && $capabilities['browserName'] === 'internet_explorer') {
      $msg = $this->validateInternetExplorer($capabilities['platform']);
      if ($msg) {
        $msgs[] = $msg;
      }
    }

    if ($capabilities['browserName'] === 'htmlunit' && !empty($capabilities['version'])) {
      $msg = $this->validateHTMLUnitVersion($capabilities['version']);
      if ($msg) {
        $msgs[] = $msg;
      }
    }

    return $msgs;
  }

  /**
   * Internal method helper to validate IE platform.
   *
   * @param string $platform
   *   The requested platform.
   *
   * @return string | null
   *   An error message not translated.
   *   NULL if no error has been detected.
   */
  protected function validateInternetExplorer($platform) {
    if (!in_array($platform, array('WINDOWS', 'VISTA', 'XP'), TRUE)) {
      return 'Internet explorer must be on a windows OS.';
    }
  }

  /**
   * Internal method helper to validate HTMLunit version.
   *
   * @param string $version
   *   A HTMLunit version which may be :
   *   - a numeric value
   *   - "firefox" or "internet explorer-7", optionally concatenate with
   *     '-VERSION'.
   *
   * @return string | null
   *   An error message not translated.
   *   NULL if no error has been detected.
   */
  protected function validateHTMLUnitVersion($version) {

    if (!is_numeric($version)) {
      $parts = explode('-', $version);

      if (!in_array($parts[0], array('firefox', 'internet explorer-7'))) {
        return 'Javascript engine name must be "firefox" or "internet explorer-7".';
      }
      if (isset($parts[1]) && !is_numeric($parts[1])) {
        return 'Javascript engine version must be a numeric value.';
      }
    }
  }
}
